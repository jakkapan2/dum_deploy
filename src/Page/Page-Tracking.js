import React, { Component } from 'react';
import TextHead from '../Components/TextHead';
import Main from '../Components/Main';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import Images from '../Components/Images'
import color from '../Asset/color';
import { get } from '../api';
import { Search } from '../Components/Search/Search';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class PageTracking extends Component {
	constructor(props) {
		super(props);

		this.state = {
			products: [],
			oldProducts: [],
			loadingSave: false
		};
	}
	// -----------------------------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		this.fetchProduct();
	};
	// -----------------------------------------------------------------------------------------------------------------------
	fetchProduct = async () => {
		this.setState({ loadingSave: true });
		const products = await get('/complaint/me');
		this.setState({ products, oldProducts: products, loadingSave: false });
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_onChangeText = (e) => this.setState({ [e.target.name]: e.target.value });
	// -----------------------------------------------------------------------------------------------------------------------
	lookDetail = (cell, row) => {
		// console.log('row', row)
		return (
			<div className="tb-report-icon" onClick={() => this.props.history.push('/lookcomplaint/' + row.form_id)}>
				รายละเอียด
			</div>
		);
	};
	// -----------------------------------------------------------------------------------------------------------------------
	search = (e) => {
		const value = e.target.value;
		if (value !== '') {
			let newsolution = this.state.oldProducts
				.map((e) => ({ ...e, sub: !e.subject ? '-' : e.subject }))
				.filter((el) => {
					return (
						el.id.toLowerCase().includes(value) || el.sub.includes(value) || el.status_name.includes(value)
					);
				});
			this.setState({ products: newsolution });
		} else {
			this.setState((prev) => ({ products: prev.oldProducts }));
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	render() {
		const { products } = this.state;
		const optionsTable = {
			paginationSize: 3,
			sizePerPageList: [ 10 ],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			noDataText: 'ไม่พบข้อมูล'
		};
		return (
			<Main>
				<TextHead title="ติดตามเรื่องร้องเรียน" />
				<br />
				<Search
					// title="ค้นหา"
					placeholder="ค้นหา รหัสเรื่อง, เรื่องร้องเรียน, สถานะเรื่อง"
					w="400px"
					classNameDiv={'d-flex justify-content-center mb-4'}
					onChange={this.search}
				/>
				{this.state.loadingSave ? <LoaderSaving /> : ''}
				<BootstrapTable
					data={products.map((e) => ({ ...e, sub: !e.subject ? '-' : e.subject }))}
					pagination={true}
					options={optionsTable}
					containerClass="my-container-class tb-setMinH"
					tableContainerClass="my-tableContainer-class"
					tableHeaderClass="my-tableHeader-class"
				>
					<TableHeaderColumn dataField="id" columnClassName={'td-style'} isKey width={'15%'}>
						รหัสเรื่องร้องเรียน
					</TableHeaderColumn>
					<TableHeaderColumn dataField="sub" columnClassName={'td-style'}>
						เรื่องร้องเรียน
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="status_name"
						tdStyle={{ fontWeight: '400', color: color.bluetheme }}
						columnClassName={'td-style'}
						width={'20%'}
					>
						สถานะเรื่อง
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="detail"
						dataFormat={this.lookDetail}
						columnClassName={'td-style'}
						width={'15%'}
					>
						รายละเอียด
					</TableHeaderColumn>
				</BootstrapTable>
			</Main>
		);
	}
}
