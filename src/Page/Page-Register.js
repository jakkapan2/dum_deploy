import React, { Component } from 'react'
import CenterGd from '../Components/CenterGd'
import styled from 'styled-components'
import { register } from '../Components/Static/static'
import { InputForm, InputRow, SelectName, InputID, InputIDregis } from '../Components/Form/Input'
import swal from 'sweetalert'
import { Row, Col } from 'reactstrap'
import { get, post } from '../api'
import lodash from 'lodash'
// import Register from '../Components/Register'
import { Line, Label, ContainerForm } from '../Asset/styled'
import { Button } from '../Asset/styleAdmin'
import LoaderSaving from '../Components/Load/LoaderSaving';

const MyHeader = styled.p`
    font-size: 1.3rem;
    font-weight: 500;
    text-align: center;
    margin-bottom: 30px;
`
const TextCenter = styled.div`
    text-align: center;
    margin-top: 50px;
`
const Content = styled.div` 
    display: table;
    flex-direction: column;
    align-items: center;    
    @media (min-width: 768px){
        width: 800px;
      }    
`

export default class PageRegister extends Component {
    constructor(props) {
        super(props)

        this.state = {
            prefix: null,
            fname: null,
            lname: null,
            id_card: null,
            rear_id_card: null,
            phone: null,
            house_number: null,
            moo: null,
            alleyway: '-',
            road: null,
            username: null,
            password: null,
            old_password: null,
            allDistrict: [],
            allProvince: [],
            allTombon: [],
            // จังหวัด
            province: '',
            district: '',
            sub_district: '',
            zipcode: '',
            loadingSave: false,
            errors: {},
            formIsValid: {}

        }
    }
    // ------------------------------------------------------------------------------------
    componentDidMount() {
        this.get_province(this.state.selProvince)
    }
    swalCheck = (message) => {
        swal('คำเตือน!', message, 'warning');
    };
    // ------------------------------------------------------------------------------------
    _onRegister = async () => {
        let { id_card, old_password, password, formIsValid, errors } = this.state
        let valids = false
        const registerData = lodash.omit(this.state, ['allDistrict', 'allProvince', 'allTombon', 'old_password'])

        if (registerData.prefix === null || registerData.prefix === '') {
            errors['prefix'] = true
            this.setState({ errors });
        }
        if (registerData.fname === null || registerData.fname === '') {
            errors['fname'] = true
            this.setState({ errors });
        }
        if (registerData.lname === null || registerData.lname === '') {
            errors['lname'] = true
            this.setState({ errors });
        }
        if (registerData.id_card === null || registerData.id_card === '') {
            errors['id_card'] = true
            this.setState({ errors });
        }
        if (registerData.rear_id_card === null || registerData.rear_id_card === '') {
            errors['rear_id_card'] = true
            this.setState({ errors });
        }
        // if (registerData.id_card.length !== 17) {
        //     this.swalCheck('กรุณากรอกหมายเลขประจำตัวประชาชนให้ครบ');
        // }
        if (registerData.phone === null || registerData.phone === '') {
            errors['phone'] = true
            this.setState({ errors });
        }
        if (registerData.house_number === null || registerData.house_number === '') {
            errors['house_number'] = true
            this.setState({ errors });
        }

        if (registerData.province === null || registerData.province === '') {
            errors['province'] = true
            this.setState({ errors });
        }
        if (registerData.district === null || registerData.district === '') {
            errors['district'] = true
            this.setState({ errors });
        }
        if (registerData.sub_district === null || registerData.sub_district === '') {
            errors['sub_district'] = true
            this.setState({ errors });
        }
        if (registerData.username === null || registerData.username === '') {
            errors['username'] = true
            this.setState({ errors });
        }
        if (registerData.password === null || registerData.password === '') {
            errors['password'] = true
            this.setState({ errors });
        }
        if (registerData.password !== old_password) {
            errors['old_password'] = true
            this.setState({ errors });
            this.swalCheck("รหัสผ่านไม่ตรงกัน")
        }else{
            errors['old_password'] = false
            this.setState({ errors });
        }
        if (JSON.stringify(formIsValid) === JSON.stringify(errors)) {
            registerData.id_card = String(
                id_card.split('-')[0] +
                id_card.split('-')[1] +
                id_card.split('-')[2] +
                id_card.split('-')[3] +
                id_card.split('-')[4]
            )
            try {
                this.setState({ loadingSave: true });
                await post('/auth/user_register', registerData)
                this.setState({ loadingSave: false });
                return swal("", "ลงทะเบียนสำเร็จ", "success").then(() => {
                    this.props.history.push('/login');
                })
            } catch (error) {
                this.setState({ loadingSave: false });
                swal("", error, "warning");
            }
        } else {
            console.log('errors', this.state);
            this.swalCheck("กรุณากรอกข้อมูลให้ครบ")

        }

    }
    // ------------------------------------------------------------------------------------
    updatetypeInput = (e, reg) => {
        let { errors, formIsValid } = this.state
        formIsValid[e.target.name] = false
        this.setState({ [e.target.name]: e.target.value, formIsValid })

        if (!e.target.value.match(reg)) {
            errors[e.target.name] = true
            this.setState({ errors, formIsValid });
        } else {
            errors[e.target.name] = false
            this.setState({ errors })
        }
    }
    // ------------------------------------------------------------------------------------
    _updateProvince = (event) => {  this.get_district(event.target.value) };
    _updateDistrict = (event) => { this.get_tombon(event.target.value) };
    _updateTombon = (event) => {
        const [sub_district, zipcode] = event.target.value.split(',')
        this.setState({ sub_district, zipcode })

        let { errors, formIsValid } = this.state
        formIsValid['sub_district'] = false
        this.setState({ formIsValid })
        errors['sub_district'] = false
        this.setState({ errors })
    };
    // ------------------------------------------------------------------------------------
    get_province = async () => {
        try {
            const allProvince = await get('/homeland/provinces')
            this.setState({ allProvince, })
        } catch (err) {
            console.log(err)
        }
    }
    get_district = async (province_id) => {
        try {
            const province = this.state.allProvince.find(el => el.value == province_id).label
            const allDistrict = await post(`/homeland/districts`, { province_id })
            this.setState({ allDistrict, province })

            let { errors, formIsValid } = this.state
            formIsValid['province'] = false
            this.setState({ formIsValid })
            errors['province'] = false
            this.setState({ errors })
        } catch (err) {
            console.log(err)
        }
    }
    get_tombon = async (district_id) => {
        try {
            const district = this.state.allDistrict.find(el => el.value == district_id).label
            const allTombon = await post(`/homeland/sub_districts`, { district_id })
            this.setState({ allTombon, district })

            let { errors, formIsValid } = this.state
            formIsValid['district'] = false
            this.setState({ formIsValid })
            errors['district'] = false
            this.setState({ errors })
        } catch (err) {
            console.log(err)
        }
    }


    render() {
        return (
            <React.Fragment>
                <div className='m-3'>
                    <a href='/login' > <Button w={'100px'}>กลับ</Button></a>
                </div>
                <CenterGd>
                    <Content>
                        {/* <div className='login-img' >
                            <img src={Images.Asset1} alt='' />
                        </div> */}
                        <MyHeader>ลงทะเบียน</MyHeader>
                        {this.renderForm()}
                        <TextCenter>
                            <button className='btn-blue' onClick={this._onRegister} >ลงทะเบียน</button>
                        </TextCenter>
                        {/* <pre>{JSON.stringify(this.state, null, '\t')}</pre> */}
                    </Content>
                </CenterGd>
                {this.state.loadingSave ? <div style={{ position: 'relative', height: '100vh', marginTop: 'calc(0px - 100vh)' }}><LoaderSaving /></div> : ''}
            </React.Fragment>
        )
    }

    renderSta(s) {
        alert(s)
        this.setState({ s })
    }

    renderForm() {
        return register.map(el => {
            switch (el.type) {
                case 'selectName': return <SelectName item={el} onChangeText={(e, i) => this.updatetypeInput(e, i)} active={true} errors={this.state.errors} />;
                case 'input': return <InputForm item={el} onChangeText={(e, i) => this.updatetypeInput(e, i)} errors={this.state.errors} />;
                case 'row': return (this.idcard(el))
                case 'rowAddr': return <InputRow item={el} onChangeText={(e, i) => this.updatetypeInput(e, i)} errors={this.state.errors} />;
                case 'rowProv': return (this.renderProv());
                case 'inputmask': return <InputID item={el} onChangeText={(e, i) => this.updatetypeInput(e, i)} errors={this.state.errors} />
                default: return <div />
            }
        })
    }
    renderProv() {
        let { errors } = this.state
        return (
            <Row style={{ paddingTop: 10 }} >
                <div className='col-sm-4 LabelInput-panel'>
                    <Label>จังหวัด</Label>
                    <select onChange={this._updateProvince} style={{ borderColor: errors && errors['province'] ? '#ff3300' : '' }}>
                        <option value='all' >-โปรดเลือกจังหวัด-</option>
                        {
                            this.state.allProvince.map(el => <option key={el.label} value={el.value} >{el.label}</option>)
                        }
                    </select>
                </div>
                <div className='col-sm-4 LabelInput-panel'>
                    <Label>อำเภอ</Label>
                    <select onChange={this.updatetypeInput} onChange={this._updateDistrict}  style={{ borderColor: errors && errors['district'] ? '#ff3300' : '' }}>
                        <option value='all' >-โปรดเลือกอำเภอ-</option>
                        {
                            this.state.allDistrict.map(el => <option key={el.label} value={el.value} >{el.label}</option>)
                        }
                    </select>
                </div>
                <div className='col-sm-4 LabelInput-panel'>
                    <Label>ตำบล</Label>
                    <select onChange={this._updateTombon}  style={{ borderColor: errors && errors['sub_district'] ? '#ff3300' : '' }}>
                        <option value='all' >-โปรดเลือกตำบล-</option>
                        {
                            this.state.allTombon.map(el => <option key={el.label} value={[el.label, el.zipcode]} >{el.label}</option>)
                        }
                    </select>
                </div>
                <div className='col-sm-4 LabelInput-panel zipcodeStyle'>
                    <Label>รหัสไปรษณีย์</Label>
                    <input type='text' name='inputZipcode' value={this.state.zipcode} className="" disabled onChange={this.updateInputValue} placeholder='รหัสไปรษณีย์' />
                </div>
                <Line />
            </Row >
        )
    }

    idcard(item) {
        let { data, xs, sm, md, ref, length } = item;
        return (
            // <InputRow item={el} onChangeText={this.updatetypeInput} />;
            <ContainerForm>
                <Row>
                    {data.map((el, i) => {
                        return (
                            <Col key={i} sm={sm} md={md} xs={xs}>
                                {el.type === 'input' ?
                                    <InputForm
                                        item={el}
                                        name={ref}
                                        onChangeText={(e, i) => this.updatetypeInput(e, i)}
                                        errors={this.state.errors}
                                        maxLength={length}
                                    />
                                    : <InputIDregis item={el} onChangeText={(e, i) => this.updatetypeInput(e, i)} errors={this.state.errors} />}
                            </Col>
                        );
                    })}
                </Row>
            </ContainerForm>
        )
    }
}
