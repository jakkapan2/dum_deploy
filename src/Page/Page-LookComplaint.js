import React, { Component } from 'react';
import Main from '../Components/Main';
import TextHead from '../Components/TextHead';
import { SumTrack } from '../Components/Div/SetDiv';
import { lookcomp } from '../Components/Static/static';
import { History, DetailComp } from '../Components/Form/FormComp';
import Images from '../Components/Images';
import { post, get, path } from '../api';
import { observer } from 'mobx-react';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import LoaderSaving from '../Components/Load/LoaderSaving';
import Modal from 'react-responsive-modal';
import Chat from '../Components/Chat/Chat';
import Message from '../mobx/message';
import { messaging } from '../init-fcm';
import 'moment/locale/th';
import moment from 'moment';
import USER from '../mobx/user';
moment.locale('th');

@observer
class PageLookComplaint extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form_id: '',
			result: null,
			showPopup: true,
			files: [],
			openPic: false,
			showImage: null,
			formstaff_join: [],
			chat_customer: [],
			filechat_customer: [],
			filenamechat_customer: [],
			imgchat_customer: [],
			imgnamechat_customer: [],
			message: '',
			user_profile: [],
			loadingsend: false,
			loadingpic: false,
			readChatCustomer: false,
			loadingSave: false,
			rightInsentive: false
		};
	}
	// ------------------------------------------------------------------------------------------------------------------
	componentWillMount = async () => {
		let accordion = [];
		lookcomp.forEach((i) => {
			accordion.push({
				title: i.title,
				content: i.content,
				ref: i.ref,
				open: false
			});
		});
		this.setState({
			accordionItems: accordion
		});
		const form_id = this.props.match.params.id;
		this.setState({ loadingSave: true });
		this.formstaff_join(form_id);
		this.chat_customer(form_id);
		this.readChatCustomer(form_id);
		try {
			this.setState({ loadingSave: true });
			const result = await post('/complaint/form', { form_id });
			// console.log('complaint/form', result);
			const get_profile = await get('/auth/profile');
			this.setState({
				result: result,
				user_profile: get_profile,
				loadingSave: false,
				rightInsentive: result.disclose == 0 ? true : false
			});
		} catch (error) {
			console.log(error);
			this.setState({ loadingSave: false });
		}
		this.setState({ loadingSave: false });
	};
	// ------------------------------------------------------------------------------------------------------------------

	// componentDidMount = async () => {
	// 	const form_id = this.props.match.params.id;
	// 	this.interval = setInterval(() => {
	// 		this.chat_customer(form_id);
	// 		this.readChatCustomer(form_id);
	// 	}, 2000);
	// };
	// componentWillUnmount() {
	// 	clearInterval(this.interval);
	// }
	//-----------------------------------------------------------------------------
	readChatCustomer = async (form_id) => {
		let read = await post('/chat/get_user_read_chat', { form_id });
		let r = read.filter(
			(e) => parseInt(e.user_id) === parseInt(USER.user_id) && parseInt(e.form_id) === parseInt(form_id)
		);
		this.setState({
			readChatCustomer: r.length > 0 ? r.some((e) => e.read === 1) : true
		});
	};
	updatereadChatCustomer = async () => {
		let form_id = this.props.match.params.id;
		let user_id = USER.user_id;
		await post('/chat/user_read_chat', { form_id, user_id });
		this.readChatCustomer();
	};
	// ------------------------------------------------------------------------------------------------------------------
	chat_customer = async (form_id) => {
		let res = await get('/chat/customer/' + form_id);
		// console.log('res', res);
		this.setState({ chat_customer: res });
	};
	// ------------------------------------------------------------------------------------------------------------------
	formstaff_join = async (form_id) => {
		let res = await get('/form/staff_join/' + form_id);
		this.setState({ formstaff_join: res });
		// console.log('res', res);
	};
	// ------------------------------------------------------------------------------------------------------------------
	click = (i) => {
		const newAccordion = this.state.accordionItems.slice();
		const index = newAccordion.indexOf(i);

		newAccordion[index].open = !newAccordion[index].open;
		this.setState({ accordionItems: newAccordion });
	};

	// ------------------------------------------------------------------------------------------------------------------
	popupChat = () => {
		// this.setState({ showPopup: !this.state.showPopup })
	};
	onOpenPic = (e) => {
		this.setState({ openPic: true, showImage: e });
	};
	onCloseModal = () => {
		this.setState({ openPic: false });
	};
	//---------------------------------------------------------------------------------------
	insert_customer_chat = async () => {
		let { message, imgchat_customer, filechat_customer } = this.state;
		// alert(message);
		let files = imgchat_customer.concat(filechat_customer);
		let formdata = new FormData();
		formdata.append('form_id', this.props.match.params.id);
		formdata.append('message', message);
		files.forEach((e) => {
			formdata.append('file', e);
		});

		if (message === '' && files.length === 0) {
		} else {
			this.setState({ loadingsend: true });
			// console.log('obj', obj);
			let res = await post('/chat/insert_customer_chat', formdata, true);
			if (res === 'insert chat success') {
				this.setState(
					{
						message: '',
						imgnamechat_customer: [],
						filenamechat_customer: [],
						imgchat_customer: [],
						filechat_customer: [],
						loadingsend: false
					},
					() => this.chat_customer()
				);
			}
		}
	};
	onEnter_customer = async (e) => {
		if (e.key === 'Enter') {
			let { message, imgchat_customer, filechat_customer } = this.state;
			let files = imgchat_customer.concat(filechat_customer);
			let formdata = new FormData();
			formdata.append('form_id', this.props.match.params.id);
			formdata.append('message', message);
			files.forEach((e) => {
				formdata.append('file', e);
			});
			// alert(message);
			if (message === '' && files.length === 0) {
			} else {
				this.setState({ loadingsend: true });
				// console.log('obj', obj);
				let res = await post('/chat/insert_customer_chat', formdata, true);
				if (res === 'insert chat success') {
					this.setState(
						{
							message: '',
							imgnamechat_customer: [],
							filenamechat_customer: [],
							imgchat_customer: [],
							filechat_customer: [],
							loadingsend: false
						},
						() => this.chat_customer()
					);
				}
			}
		}
	};
	//---------------------------------------------------------------------------------------
	uploadImg_chat_customer = (event) => {
		// console.log('event.target.files', event.target.files);

		Object.keys(event.target.files).forEach((key) => {
			let { imgchat_customer } = this.state;
			imgchat_customer.push(event.target.files[key]);
			this.setState({ imgchat_customer });
			// console.log('event.target.files[key]', event.target.files[key]);
			if (event.target.files && event.target.files[key]) {
				let reader = new FileReader();
				reader.onload = (e) => {
					let { imgnamechat_customer } = this.state;
					imgnamechat_customer.push(e.target.result);
					this.setState({ imgnamechat_customer });
				};
				reader.readAsDataURL(event.target.files[key]);
			}
		});
	};
	//--------------------------------------------------------------------------------------
	uploadFile_chat_customer = (event) => {
		// console.log('event.target.files', event.target.files);
		Object.keys(event.target.files).forEach((key) => {
			let { filenamechat_customer, filechat_customer } = this.state;
			filechat_customer.push(event.target.files[key]);
			filenamechat_customer.push(event.target.files[key].name);
			this.setState({ filechat_customer, filenamechat_customer });
			// console.log('event.target.files[key]', event.target.files[key]);
		});
	};
	//---------------------------------------------------------------------------------------
	DelImg_chat_customer = (i) => {
		let { imgchat_customer, imgnamechat_customer } = this.state;
		imgchat_customer.splice(i, 1);
		imgnamechat_customer.splice(i, 1);
		this.setState({ imgchat_customer, imgnamechat_customer });
	};
	DelFile_chat_customer = (i) => {
		let { filechat_customer, filenamechat_customer } = this.state;
		filechat_customer.splice(i, 1);
		filenamechat_customer.splice(i, 1);
		this.setState({ filenamechat_customer, filechat_customer });
	};
	//---------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------

	render() {
		// const { open, result, showPopup } = this.state
		const { result, formstaff_join, readChatCustomer, rightInsentive } = this.state;
		// console.log('result852>>', result);

		return (
			<Main>
				<TextHead title="ติดตามเรื่องร้องเรียน" />
				<br />
				{result ? (
					<div>
						<div className="row">
							<div className="-comp-look-content col-md-9">
								<div className="col-md-12">
									<SumTrack
										title="หัวข้อเรื่องร้องเรียน"
										detail={!result.subject ? '-' : result.subject}
									/>
								</div>
								<div className="col-md-7">
									<SumTrack title="รหัสเรื่องร้องเรียน" detail={result.form_id} />
								</div>
								<div className="col-md-5">
									<SumTrack title="รหัส MOI" detail={result.moi ? result.moi : '-'} />
								</div>
								<div className="col-md-12">
									<SumTrack title="สถานะเรื่องร้องเรียน" detail={result.status_name} />
								</div>
								<div className="col-md-7">
									<SumTrack
										title="แจ้งเรื่องวันที่"
										detail={
											moment(result.timestamp).add(543, 'y').format('DD MMMM YYYY HH:MM') + ' น.'
										}
									/>
								</div>
								<div className="col-md-5">
									<SumTrack
										title="วันที่รับเรื่อง"
										detail={
											moment(result.history.datatime).add(543, 'y').format('DD MMMM YYYY HH:MM') +
											' น.'
										}
									/>
								</div>
								<div className="col-md-7">
									<SumTrack
										title="รับเรื่องโดย"
										detail={result.insert_by === 'CENTRAL' ? 'หน่วยงาน' : 'ศูนย์ดำรงธรรม'}
									/>
								</div>
								<div className="col-md-5">
									<SumTrack title="เจ้าหน้าที่ศูนย์ดำรงธรรมที่รับผิดชอบ" detail={result.name_admin} />
								</div>
								<div className="col-md-7">
									<SumTrack
										title="เจ้าหน้าที่หน่วยงานที่รับผิดชอบ"
										detail={result.name_user_central}
									/>
								</div>
								<div className="col-md-5">
									<SumTrack title="หน่วยงานที่รับผิดชอบ" detail={result.name_central} />
								</div>
							</div>
							{rightInsentive == false && (
								<div className="col-md-3 text-align-right">
									<img src={Images.Asset23} alt="" />
									<span onClick={this.updatereadChatCustomer}>
										<span id="callBack" className="h4 ml-2" onClick={Message.onShowChat}>
											การตอบกลับเจ้าหน้าที่
											<i
												className={
													readChatCustomer === false ? 'fas fa-exclamation-circle' : ''
												}
												style={{ color: '#f00' }}
											/>
										</span>
									</span>
								</div>
							)}
						</div>
						<div style={{ marginTop: '20px', marginLeft: '5px' }}>{this.renderLookcomp()}</div>
						{/* --------------------------------------------------------------------------------------------- 
                    {
                        showPopup ? <React.Fragment> {this.renderPopup()}</React.Fragment>
                            : null
					}*/}
						<Modal
							open={this.state.openPic}
							onClose={this.onCloseModal}
							little
							classNames={{ modal: 'Look-modal ChangeStatus ' }}
						>
							<img
								src={this.state.showImage}
								style={{ width: '100%', maxWidth: '900px', display: 'flex' }}
							/>
						</Modal>
						<Chat
							className={Message.showChat ? 'slide' : 'slide-close'}
							chat_customer={this.state.chat_customer}
							user_profile={this.state.user_profile}
							openPic={this.onOpenPic}
							message={this.state.message}
							set_message={(e) => this.setState({ message: e })}
							messagesEnd={(e) => e && e.scrollIntoView({ behavior: 'smooth' })}
							uploadImg_chat_customer={(e) => this.uploadImg_chat_customer(e)}
							uploadFile_chat_customer={(e) => this.uploadFile_chat_customer(e)}
							imgnamechat_customer={this.state.imgnamechat_customer}
							filenamechat_customer={this.state.filenamechat_customer}
							DelImg_chat_customer={(i) => this.DelImg_chat_customer(i)}
							DelFile_chat_customer={(i) => this.DelFile_chat_customer(i)}
							insert_customer_chat={() => this.insert_customer_chat()}
							onEnter_customer={this.onEnter_customer}
							loadingsend={this.state.loadingsend}
							loadingpic={this.state.loadingpic}
						/>
					</div>
				) : null}
				{this.state.loadingSave ? (
					<div style={{ position: 'relative', height: '350px' }}>
						<LoaderSaving />
					</div>
				) : (
					''
				)}
			</Main>
		);
	}
	renderLookcomp() {
		const { files } = this.state;
		return this.state.accordionItems.map((el, i) => {
			// alert(el.ref)
			return (
				<div key={el.id}>
					<div className="-comp-look-q-title" onClick={this.click.bind(null, el)}>
						<div className="arrow-wrapper">
							<i className={el.open ? 'fa fa-angle-down fa-rotate-180' : 'fa fa-angle-down'} />
						</div>
						<span className="-q-title-text">{el.title}</span>
					</div>
					<div className={el.open ? '-comp-look-q-content -q-content-open' : '-comp-look-q-content'}>
						<div
							className={el.open ? '-q-content-text -q-content-text-open colorLabel' : '-q-content-text'}
						>
							{el.ref === 'comp' ? (
								<DetailComp data={this.state.result} role={'user'} openPic={(e) => this.onOpenPic(e)} />
							) : (
								<History data={this.state.result} openPic={(e) => this.onOpenPic(e)} role={'user'} />
							)}
						</div>
					</div>
				</div>
			);
		});
	}
	// renderPopup() {
	//     return (
	//         <PanelPopup>
	//             <p id='close' onClick={this.popupChat}>X</p>
	//             <p style={{ textAlign: 'center' }} >การตอบกลับเจ้าหน้าที่</p>
	//             <BlockPopup>
	//                 <NamePopup>
	//                     <div className='nav-head-left-img' ><img src={Images.Asset30} alt='' /></div>
	//                     <div className='detail-name' >
	//                         <p>ชื่อ</p>
	//                         <span>ตำแหน่ง<label>date</label></span>
	//                     </div>
	//                 </NamePopup>
	//                 <DetailPopup>Text</DetailPopup>
	//             </BlockPopup>
	//         </PanelPopup>
	//     )
	// }
}
export default PageLookComplaint;
