import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';
import TextHead, { TextLine } from '../Components/TextHead';
import Main from '../Components/Main';
import Block from '../Components/Block';
import Images from '../Components/Images';
import {
	sub_complaint,
	headComplaint,
	complaint,
	complaintToCenter,
	complaintToRight,
	detailComplaint,
	selectComplaint
} from '../Components/Static/static';
import {
	InputForm,
	RadioForm,
	InputRow,
	Textarea,
	SelectName,
	RadioeDePart,
	SelectSubtype
} from '../Components/Form/Input';
import { FormCompUpload, FormCompUpList, FormCompAgree } from '../Components/Form/Complaint';
import { RadioCol } from '../Components/Form/InputAdmin';
import InputMask from 'react-input-mask';
import swal from 'sweetalert';
import Modal from 'react-responsive-modal';
import { Row, Col } from 'reactstrap';
import { Label } from '../Asset/styled';
import { get, post } from '../api';
import { Selecter, SelecterPerson, SelecterCompany } from '../Components/Form/SelectProvince';
import MapComponent from '../Components/Maps/MapComponent';
import { Sumtext, SumDetail } from '../Components/Div/SetDiv';
import _ from 'lodash';
import { fullname, address } from '../functions';
import UploadFile from '../Components/Form/UploadFile';
import { uploadState } from '../Components/Form/UploadFile';
import { Button } from '../Asset/styleAdmin';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class PageReportComplaint extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sayyes: false,
			subject: '',
			allDistrict: [],
			allProvince: [],
			allTombon: [],
			allZipcode: {},
			prefix: '',
			fname: '',
			lname: '',
			house_number: '',
			phone: '',
			id_line: '',
			email: '',
			moo: '',
			alley: '',
			road: '',
			detail: '',
			objective: '',

			complaint: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			complainant: '',
			fname_person: '',
			lname_person: '',

			house_number_person: '',
			moo_person: '',
			alleyway_person: '',
			road_person: '',
			right: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			center: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			corporation_name: '',
			corporation_phone: '',
			activecomplaint: false,
			activeleft: true,
			activeright: false,
			openModal: false,
			// select_info: 8, ==> disclose: 8,
			disclose: 8,
			select_complaintTo: 10,
			select_headcomplaintTo: 1,
			type: 0,
			id_card: '',
			province: '',
			district: '',
			sub_district: '',
			zipcode: '',
			sub_type: 'เลือก',

			// department complaint
			departmentComplaint: [ { data: [] } ],
			companyType: [],
			type_company: 'บริษัท',

			// state for api department
			select_depart: '', // department_id
			department_name: '', // find from departmentComplaint
			department_type: '', //
			// company type
			type_id: 0,
			// location
			lat: 0,
			lng: 0,
			location_name: '',
			// ----------------
			fileImgList: [],
			filePdfList: [],
			loadingSave: false,
			valid: false
		};
	}
	// ------------------------------------------------------------------------------------
	componentDidMount() {
		// this._fetchUserData();
		this._fetchDepartmentAndCompanyType();
	}
	// ------------------------------------------------------------------------------------
	_fetchUserData = async () => {
		try {
			let { complaint } = this.state;
			const res = await get('/auth/profile');
			complaint.province = res.province;
			complaint.district = res.district;
			complaint.tumbon = res.sub_district;
			complaint.zipcode = res.zipcode;
			// console.log('res', res);
			this.setState({ ...res, complaint });
		} catch (error) {
			console.log(error);
		}
	};
	delUserData = () => {
		this.setState({
			alleyway: '',
			complaint: {
				district: '',
				province: '',
				tumbon: '',
				zipcode: ''
			},
			email: '',
			fname: '',
			house_number: '',
			lname: '',
			moo: '',
			phone: '',
			prefix: '0',
			road: ''
		});
	};
	// ------------------------------------------------------------------------------------
	_fetchDepartmentAndCompanyType = async () => {
		try {
			const companyType = await get('/form/company_type');
			this.setState({
				departmentComplaint: [
					{
						ref: 'department_name',
						title: 'หน่วยงานที่ถูกร้อง',
						type: 'input',
						placeholder: 'กรุณากรอกหน่วยงานที่ถูกร้อง'
					}
				],
				companyType
			});
		} catch (error) {
			console.log(error);
		}
	};
	// ------------------------------------------------------------------------------------
	componentWillUpdate = (prevProps, prevState) => {
		if (prevState.select_complaintTo !== this.state.select_complaintTo) {
			this.setState((prev) => ({ activeleft: !prev.activeleft, activeright: !prev.activeright }));
		} else if (prevState.disclose !== this.state.disclose) {
			this.setState((prev) => ({ activecomplaint: !prev.activecomplaint }));
		}
	};
	// ------------------------------------------------------------------------------------
	updatetypeInput = (e) => {
		// alert(e.target.name);
		if (e.target.name === 'select_headcomplaintTo') {
			// alert("select")
			this.setState({ sub_type: 'เลือก' });
		}
		if (e.target.name === 'disclose') {
			if (Number(e.target.value) === 8) {
				this.delUserData();
			} else {
				this._fetchUserData();
			}
		}
		this.setState({ [e.target.name]: e.target.value });
	};

	//
	updateRadioDep = (e) => {
		const { value } = e.target;
		// console.log(value)
		const [ select_depart, department_type ] = value.split(',');
		this.setState({ select_depart, department_type });
	};
	// ------------------------------------------------------------------------------------
	onChange = (ref, value) => {
		// console.log('ref', ref, 'value', value);
		this.setState({ [ref]: value });
	};
	onChangeCompany = (ref, value) => {
		if (ref !== 'complaint') {
			// console.log('ref', ref, 'value', value);
			this.setState({ [ref]: value });
		}
	};
	onChangePerson = (ref, value) => {
		if (ref !== 'complaint') {
			// console.log('ref', ref, 'value', value);
			this.setState({ [ref]: value });
		}
	};
	// ------------------------------------------------------------------------------------
	onOpenModal = () => {
		this.setState({ openModal: true });
		this.setState({ fileImgList: uploadState.fileImgList, filePdfList: uploadState.filePdfList });
		// console.log('this.state', this.state);
	};
	onCloseModal = () => {
		this.setState({ openModal: false });
	};
	onMapChange = (e) => {
		// console.log('e',e);
		this.setState({ location_name: e.location_name, lat: e.lat, lng: e.lng });
	};
	// ------------------------------------------------------------------------------------
	swalCheck = (message) => {
		this.setState({ valid: false });
		swal('คำเตือน!', message, 'warning');
	};
	// ------------------------------------------------------------------------------------
	_onSentComplaint = async () => {
		// alert(this.state.disclose)
		// console.log('this.state', this.state);
		let detailJson = JSON.stringify(this.state);
		let detail = JSON.parse(detailJson);
		detail.type = headComplaint[1].data.filter(
			(e) => Number(e.value) === Number(this.state.select_headcomplaintTo)
		)[0].label;
		detail.disclose = Number(this.state.disclose) === 8 ? 0 : 1;
		//-----------------------------------------------------------------
		detail.sub_district_person = detail.center.tumbon;
		detail.district_person = detail.center.district;
		detail.province_person = detail.center.province;
		detail.zipcode_person = detail.center.zipcode;
		//-------------------------------------------------
		detail.name_company = detail.corporation_name;
		detail.phone_company = detail.corporation_phone;
		detail.sub_district_company = detail.right.tumbon;
		detail.district_company = detail.right.district;
		detail.province_company = detail.right.province;
		detail.zipcode_company = detail.right.zipcode;
		detail.complainant =
			Number(detail.select_complaintTo) === 10
				? 'DEPARTMENT'
				: Number(detail.select_complaintTo) === 11 ? 'PERSON' : 'COMPANY';

		detail.id_card = detail.id_card.split('-')[0];
		// console.log('detail', detail);

		let check = this.checkInput(detail);
		if (check === true) {
			if (detail.sayyes === true) {
				// alert('ok');
				try {
					this.setState({ loadingSave: true });
					const insert = await post('/form/insert', detail);
					// console.log('insert>>>', insert)
					const formData = new FormData();
					formData.append('form_id', insert.form_id);
					uploadState.fileImgList.forEach(async (e) => {
						await formData.append('files', e);
					});
					uploadState.filePdfList.forEach(async (e) => {
						await formData.append('files', e);
					});

					await post('/form/insert/evidence', formData, true);
					this.setState({ loadingSave: false });
					swal({
						title: 'ส่งเรื่องร้องเรียนเรียบร้อยแล้ว',
						text: 'ท่านสามารถตรวจสอบเรื่องร้องเรียนได้ที่ เมนู"ติดตามเรื่องและสถานะเรื่อง" ',
						icon: 'success'
					}).then(() => {
						// if (willDelete) {
						this.props.history.push('/tracking');
						// }
					});
				} catch (error) {
					swal('ผิดพลาด', error, 'error');
					this.setState({ loadingSave: false });
				}
			} else {
				this.swalCheck('กรุณากดยอมรับข้อตกลง');
			}
		}
		// console.log('check', check);
	};
	checkInput(detail) {
		if (detail.subject === null || detail.subject === '') {
			this.swalCheck('กรุณากรอกหัวข้อร้องทุกข์');
			return false;
		} else {
			return true;
		}
		// if (detail.sub_type === null || detail.sub_type === 'เลือก') {
		// 	this.swalCheck('กรุณากรอกประเภทเรื่องย่อย');
		// 	return false;
		// }
		// if (Number(detail.disclose) === 1) {
		// 	if (detail.id_card === null || detail.id_card === '') {
		// 		this.swalCheck('กรุณากรอกเลขที่บัตรประจำตัวประชาชนผู้ร้องทุกข์');
		// 		return false;
		// 	}
		// 	if (detail.fname === null || detail.fname === '') {
		// 		this.swalCheck('กรุณากรอกชื่อผู้ร้องทุกข์');
		// 		return false;
		// 	}
		// 	if (detail.lname === null || detail.lname === '') {
		// 		this.swalCheck('กรุณากรอกนามสกุลผู้ร้องทุกข์');
		// 		return false;
		// 	}
		// 	// if (detail.phone === null || detail.phone === '') {
		// 	// 	this.swalCheck('กรุณากรอกเบอร์โทรศัพท์ผู้ร้องทุกข์');
		// 	// 	return false;
		// 	// }
		// 	if (detail.house_number === null || detail.house_number === '') {
		// 		this.swalCheck('กรุณากรอกบ้านเลขที่');
		// 		return false;
		// 	}
		// 	// if (detail.moo === null || detail.moo === '') {
		// 	// 	this.swalCheck('กรุณากรอกหมู่ที่');
		// 	// 	return false;
		// 	// }
		// 	if (detail.province === null || detail.province === '') {
		// 		this.swalCheck('กรุณากรอกจังหวัด');
		// 		return false;
		// 	}
		// 	if (detail.district === null || detail.district === '') {
		// 		this.swalCheck('กรุณากรอกอำเภอ');
		// 		return false;
		// 	}
		// 	if (detail.sub_district === null || detail.sub_district === '') {
		// 		this.swalCheck('กรุณากรอกตำบล');
		// 		return false;
		// 	}
		// }
		// // if (detail.complainant === 'DEPARTMENT') {
		// // 	if (detail.department_name === null || detail.department_name === '') {
		// // 		this.swalCheck('กรุณากรอกหน่วยงานที่ถูกร้อง');
		// // 		return false;
		// // 	}
		// // }
		// if (detail.complainant === 'COMPANY') {
		// 	// if (detail.name_company === null || detail.name_company === '') {
		// 	// 	this.swalCheck('กรุณากรอกชื่อนิติบุคคลหรือบริษัท');
		// 	// 	return false;
		// 	// }
		// 	if (detail.province_company === null || detail.province_company === '') {
		// 		this.swalCheck('กรุณากรอกจังหวัด');
		// 		return false;
		// 	}
		// 	if (detail.district_company === null || detail.district_company === '') {
		// 		this.swalCheck('กรุณากรอกอำเภอ');
		// 		return false;
		// 	}
		// 	if (detail.sub_district_company === null || detail.sub_district_company === '') {
		// 		this.swalCheck('กรุณากรอกตำบล');
		// 		return false;
		// 	}
		// }
		// if (detail.complainant === 'PERSON') {
		// 	// if (detail.fname_person === null || detail.fname_person === '') {
		// 	// 	this.swalCheck('กรุณากรอกชื่อผู้ถูกร้องทุกข์');
		// 	// 	return false;
		// 	// }
		// 	if (detail.lname_person === null || detail.lname_person === '') {
		// 		this.swalCheck('กรุณากรอกนามสกุลผู้ถูกร้องทุกข์');
		// 		return false;
		// 	}
		// 	if (detail.house_number_person === null || detail.house_number_person === '') {
		// 		this.swalCheck('กรุณากรอกบ้านเลขที่');
		// 		return false;
		// 	}
		// 	// if (detail.moo_person === null || detail.moo_person === '') {
		// 	// 	this.swalCheck('กรุณากรอกหมู่ที่');
		// 	// 	return false;
		// 	// }
		// 	if (detail.province_person === null || detail.province_person === '') {
		// 		this.swalCheck('กรุณากรอกจังหวัด');
		// 		return false;
		// 	}
		// 	if (detail.district_person === null || detail.district_person === '') {
		// 		this.swalCheck('กรุณากรอกอำเภอ');
		// 		return false;
		// 	}
		// 	if (detail.sub_district_person === null || detail.sub_district_person === '') {
		// 		this.swalCheck('กรุณากรอกตำบล');
		// 		return false;
		// 	}
		// } else {
		// 	// if (Number(detail.lat) === 0 && Number(detail.lng) === 0) {
		// 	// 	this.swalCheck('กรุณาปักมุดสถานที่เกิดเหตุ');
		// 	// 	return false;
		// 	// }
		// 	// if (detail.location_name === null || detail.location_name === '') {
		// 	// 	this.swalCheck('กรุณากรอกสถานที่เกิดเหตุ');
		// 	// 	return false;
		// 	// }
		// 	// if (detail.detail === null || detail.detail === '') {
		// 	// 	this.swalCheck('กรุณากรอกรายละเอียด');
		// 	// 	return false;
		// 	// }
		// 	// if (detail.objective === null || detail.objective === '') {
		// 	// 	this.swalCheck('กรุณากรอกวัตถุประสงค์');
		// 	// 	return false;
		// 	// }
		// 	return true;
		// }
	}
	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//
	//                                                                                                               //
	//                                                   render                                                      //
	//                                                                                                               //
	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//
	render() {
		let { openModal } = this.state;
		return (
			<Main>
				<TextHead title="แจ้งเรื่องร้องเรียน" />
				<Block>
					<TextLine title="เรื่องร้องทุกข์" />
					{this.renderHeadComplaint()}
					<div className="selectSubtype">{this.renderHeadComplaintTo()}</div>
					<TextLine title="ผู้ร้องทุกข์" />
					{this.renderComplaint()}
					<TextLine title="ผู้ถูกร้องทุกข์" />
					{this.renderComplaintTo()}
					<TextLine title="รายละเอียดคำร้อง" />
					{this.renderDetailComplaint()}
					<TextLine title="ไฟล์เอกสาร" />
				</Block>
				{/* ------------------------------------------------------------- */}
				<UploadFile />
				{/* ------------------------------------------------------------- */}
				<Button w={'140px'} mx={'auto'} my={'3rem'} onClick={this.onOpenModal}>
					ดำเนินการต่อ
				</Button>
				<Modal
					open={openModal}
					onClose={this.onCloseModal}
					little
					classNames={{ modal: 'Look-modal SummaryComplaint ' }}
				>
					<div className="layout" style={{ position: 'relative' }}>
						<h2 style={{ marginBottom: 30 }}>ตรวจสอบเรื่องร้องเรียน</h2>
						{this.modalCheckComplaint()}
						<Button w={'120px'} mx={'auto'} my={'1rem'} onClick={this._onSentComplaint}>
							ส่งคำร้อง
						</Button>
						{this.state.loadingSave ? (
							<div
								style={{
									position: 'absolute',
									height: '100%',
									width: '100%',
									top: '-1.2rem',
									left: '-1.2rem'
								}}
							>
								<LoaderSaving />
							</div>
						) : (
							''
						)}
					</div>
				</Modal>
				{/* <pre style={{ display: 'none' }}>
					{JSON.stringify(
						_.omit(this.state, ['departmentComplaint', 'allDistrict', 'allProvince', 'allTombon']),
						null,
						'\t'
					)}
				</pre> */}
			</Main>
		);
	}
	// --------------------------------------------------------------------------------------------------------------//
	renderHeadComplaint() {
		// console.log('headComplaint',headComplaint.slice(0,2));
		return headComplaint.slice(0, 2).map((el) => {
			return this.onSwitch(el, 'head');
		});
	}

	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//
	//                                                                                                               //
	//                                                   renderHeadComplaintTo                                                      //
	//                                                                                                               //
	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//

	renderHeadComplaintTo() {
		return (
			<Row>
				{
					<Col sm={6}>
						{sub_complaint.map((el, i) => {
							return this.onSwitch(el);
						})}
					</Col>
				}
			</Row>
		);
	}
	// --------------------------------------------------------------------------------------------------------------//
	renderComplaint() {
		return complaint.filter((e) => (Number(this.state.disclose) === 8 ? e.type === 'radio' : e)).map((el) => {
			return this.onSwitch(el, 'complaint');
		});
	}
	// --------------------------------------------------------------------------------------------------------------//
	renderComplaintTo() {
		// console.log('this.state.select_complaintTo', this.state.select_complaintTo);
		return (
			<Row>
				<Col sm={12}>
					{selectComplaint.map((el, i) => {
						return this.onSwitch(el, 'selectTo');
					})}
				</Col>
				{parseInt(this.state.select_complaintTo) === 10 ? (
					<Col sm={12}>
						{this.state.departmentComplaint.map((el, i) => {
							return this.onSwitch(el, 'left');
						})}
					</Col>
				) : parseInt(this.state.select_complaintTo) === 11 ? (
					<Col sm={12}>
						{complaintToCenter.map((el, i) => {
							return this.onSwitch(el, 'center');
						})}
					</Col>
				) : (
					<Col sm={12}>
						{complaintToRight.map((el, i) => {
							return this.onSwitch(el, 'right');
						})}
					</Col>
				)}
			</Row>
		);
	}
	// --------------------------------------------------------------------------------------------------------------//
	renderDetailComplaint() {
		return (
			<Row>
				<Col sm={6}>
					<MapComponent
						onMarkerChange={this.onMapChange}
						markerCoords={_.pick(this.state, [ 'lat', 'lng' ])}
					/>
				</Col>
				<Col sm={6}>
					{detailComplaint.map((el, i) => {
						return this.onSwitch(el, 'detailComplaint');
					})}
				</Col>
			</Row>
		);
	}

	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//
	//                                                                                                               //
	//                                                   onSwitch()                                                      //
	//                                                                                                               //
	// --------------------------------------------------------------------------------------------------------------//
	// --------------------------------------------------------------------------------------------------------------//
	onSwitch(el, ref) {
		// console.log(el.type)
		switch (el.type) {
			case 'selectName':
				return (
					<SelectName
						active={this.state['active' + ref]}
						item={el}
						onChangeText={this.updatetypeInput}
						value={this.state}
					/>
				);
			case 'input':
				return <InputForm item={el} onChangeText={this.updatetypeInput} value={this.state} />;
			case 'radio':
				return <RadioForm checked={this.state[el.ref]} item={el} onChangeRadio={this.updatetypeInput} />;
			// test by khawkriab
			case 'selectedSubType':
				return (
					<SelectSubtype
						value={this.state.sub_type}
						item={el}
						head={this.state.select_headcomplaintTo}
						onChangeSelect={this.updatetypeInput}
					/>
				);
			case 'radioDepart':
				return <RadioeDePart item={el} onChangeRadio={this.updateRadioDep} />;
			case 'radioDep':
				return <RadioForm item={el} onChangeRadio={this.updatetypeInput} />;
			case 'row':
				return (
					<InputRow
						active={ref === 'right' ? true : this.state['active' + ref]}
						item={el}
						onChangeText={this.updatetypeInput}
						value={this.state}
					/>
				);
			case 'cardno':
				return (
					<div key={el.ref} className="-comp-form-cardno-ad">
						<Label>{el.title}</Label>
						<InputMask
							disabled={!this.state.activecomplaint}
							maskChar={null}
							mask={'9-9999-99999-99-9'}
							onChange={this.updatetypeInput}
							name={'id_card'}
							inputMode="numeric"
							type="text"
							className="form-control form-group"
							value={this.state.id_card}
							placeholder="หมายเลขประจำตัวประชาชน"
						/>
					</div>
				);
			case 'textarea':
				return <Textarea item={el} onChangeText={this.updatetypeInput} />;
			case 'rowProv':
				return (this.state.select_complaintTo === 12 || this.state.select_complaintTo === '12') &&
				ref === 'right' ? (
					<SelecterCompany
						active={true}
						name={ref}
						onChange={(ref, value) => this.onChangeCompany(ref, value)}
						value={this.state}
					/>
				) : (this.state.select_complaintTo === 11 || this.state.select_complaintTo === '11') &&
				ref === 'center' ? (
					<SelecterPerson
						active={true}
						name={ref}
						onChange={(ref, value) => this.onChangePerson(ref, value)}
						value={this.state}
					/>
				) : (
					<Selecter
						active={ref === 'right' ? true : this.state['active' + ref]}
						name={ref}
						onChange={(ref, value) => this.onChange(ref, value)}
						value={this.state}
					/>
				);
			case 'corporationType':
				return (
					<RadioCol
						others={this.state.others}
						checked={this.state[el.ref]}
						item={el}
						onChangeRadio={this.updatetypeInput}
					/>
				);
			default:
				return <div />;
		}
	}
	// --------------------------------------------------------------------------------------------------------------//
	modalCheckComplaint = () => {
		// console.log(JSON.stringify( sub_complaint[0][`${this.state.select_headcomplaintTo}`].data))
		// console.log("headComplaint",headComplaint)
		// console.log("headComplaint[1].data[this.state.select_headcomplaintTo].label",headComplaint[1].data[this.state.select_headcomplaintTo===8.1 ? 7 :this.state.select_headcomplaintTo-1].label)
		let {
			sayyes,
			select_complaintTo,
			disclose,
			fileImgList,
			filePdfList,
			select_headcomplaintTo,
			id_line,
			email,
			department_name
		} = this.state;
		return (
			<div>
				<TextLine title="เรื่องร้องทุกข์" />
				<Sumtext title="หัวข้อร้องทุกข์" detail={!this.state.subject ? '' : this.state.subject} />
				<Sumtext
					title="ประเภทเรื่อง"
					detail={
						headComplaint[1].data[
							Number(select_headcomplaintTo) === 8.1 ? 7 : Number(select_headcomplaintTo) - 1
						].label
					}
				/>
				<Sumtext title="ประเภทเรื่องย่อย" detail={this.state.sub_type === 'เลือก' ? '' : this.state.sub_type} />
				{/* -------------------------------------------------------------- */}
				<TextLine title="ผู้ร้องทุกข์" />
				{Number(disclose) === 8 ? (
					<div className="-comp-upload">ปกปิดข้อมูล</div>
				) : (
					<div>
						<Sumtext title="ชื่อ-นามสกุลผู้ร้อง" detail={fullname(this.state.fname, this.state.lname)} />
						<Sumtext title="หมายเลขประจำตัวประชาชน" detail={this.state.id_card} />
						{/* <Sumtext title="ID LINE ผู้ร้องทุกข์" detail={id_line ? id_line : ''} /> */}
						<Sumtext title="E-mail ผู้ร้องทุกข์" detail={email ? email : ''} />
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(this.state, [ 'house_number', 'moo', 'alleyway', 'road' ]),
								..._.pick(this.state.complaint, [ 'tumbon', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				)}
				{/* -------------------------------------------------------------- */}
				<TextLine title="ผู้ถูกร้องทุกข์" />
				<Sumtext
					title="ผู้ถูกร้อง"
					detail={
						Number(select_complaintTo) === 10 ? (
							'หน่วยงาน'
						) : Number(select_complaintTo) === 11 ? (
							'บุคคล'
						) : (
							'นิติบุคคล'
						)
					}
				/>
				{Number(select_complaintTo) === 10 ? (
					<Sumtext
						title="ชื่อหน่วยงาน"
						detail={department_name === '' || department_name === null ? '' : department_name}
					/>
				) : Number(select_complaintTo) === 11 ? (
					<div>
						<Sumtext
							title="ชื่อผู้ถูกร้อง"
							detail={this.state.fname_person + ' ' + this.state.lname_person}
						/>
						<Sumtext
							title="หมายเลขโทรศัพท์(ถ้ามี)"
							detail={this.state.phone_person ? this.state.phone_person : ''}
						/>
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(this.state, [
									'house_number_person',
									'moo_person',
									'alleyway_person',
									'road_person'
								]),
								..._.pick(this.state.center, [ 'tumbon', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				) : (
					<div>
						<Sumtext
							title="ชื่อผู้ถูกร้อง"
							detail={!this.state.corporation_name ? '' : this.state.corporation_name}
							style={{ display: 'grid' }}
						/>
						<Sumtext
							title="หมายเลขโทรศัพท์(ถ้ามี)"
							detail={this.state.phone_company ? this.state.phone_company : ''}
						/>
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(this.state.right, [ 'tumbon', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				)}
				{/* -------------------------------------------------------------- */}
				<TextLine title="รายละเอียดคำร้อง" />
				<Row>
					<Col xs={7}>
						<MapComponent
							h={'250px'}
							coords={{ latitude: this.state.lat, longitude: this.state.lat }}
							markerCoords={{ lat: this.state.lat, lng: this.state.lat }}
							onMarkerChange={(e) => console.log('e', e)}
						/>
					</Col>
					<Col xs={5} className="pl-3">
						<div className="d-grid text-left" style={{ gridTemplateColumns: '100px auto' }}>
							<label>สถานที่เกิดเหตุ</label>
							<label className="text-left pl-3" style={{ fontSize: '12px' }}>
								{!this.state.location_name ? '' : this.state.location_name}
							</label>
						</div>
						{/* <Sumtext title="สถานที่เกิดเหตุ" detail={this.state.location_name} wl={'90px'} /> */}
						<SumDetail title="รายละเอียด" detail={!this.state.detail ? '' : this.state.detail} />
						<SumDetail title="วัตถุประสงค์" detail={!this.state.objective ? '' : this.state.objective} />
					</Col>
				</Row>
				{/* -------------------------------------------------------------- */}
				<TextLine title="ไฟล์เอกสาร" />
				<FormCompUpload>
					{fileImgList.length === 0 && filePdfList.length === 0 && <div>ไม่มีไฟล์เอกสาร</div>}
					{fileImgList.map((fileImg, index) => (
						<FormCompUpList key={fileImg.name}>
							<div className="-namefile" style={{ overflowX: 'hidden' }}>
								<img src={Images.Asset14} alt="" />
								<label style={{ whiteSpace: 'pre', textOverflow: 'ellipsis', overflow: 'hidden' }}>
									{fileImg.name + ' ' + bytesToSize(fileImg.size)}
								</label>
							</div>
						</FormCompUpList>
					))}
					{filePdfList.map((filePdf, index) => (
						<FormCompUpList key={filePdf.name}>
							<div className="-namefile" style={{ overflowX: 'hidden' }}>
								<img src={Images.Asset15} alt="" />
								<label style={{ whiteSpace: 'pre', textOverflow: 'ellipsis', overflow: 'hidden' }}>
									{filePdf.name + ' ' + bytesToSize(filePdf.size)}
								</label>
							</div>
						</FormCompUpList>
					))}
				</FormCompUpload>
				<FormCompAgree>
					<input
						checked={this.state.sayyes === true}
						type="checkbox"
						id="agree"
						name="feature"
						value={sayyes}
						onChange={(e) => this.setState((prev) => ({ sayyes: !prev.sayyes }))}
					/>
					<label for="agree">
						ข้าพเจ้าขอรับรองว่าเป็นความจริงทุกประการ หากปรากฏว่าไม่เป็นความจริง ข้าพเจ้ายอกรับผิด
						และให้ดำเนินการได้ตามกฎหมาย
					</label>
				</FormCompAgree>
			</div>
		);
	};
	removeFile = (index) => {
		let { fileImgList } = this.state;
		fileImgList.splice(index, 1);
		this.setState({ fileImgList });
	};
	removePdfFile = (index) => {
		let { filePdfList } = this.state;
		filePdfList.splice(index, 1);
		this.setState({ filePdfList });
	};
}
// --------------------------------------------------------------------------------------------------------------//
function bytesToSize(bytes) {
	var sizes = [ 'Bytes', 'KB', 'MB', 'GB', 'TB' ];
	if (bytes === 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
