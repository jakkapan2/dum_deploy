import React, { Component } from 'react'
import TextHead from '../Components/TextHead'
import Main from '../Components/Main'
import swal from 'sweetalert'
import { post } from '../api';
import { Button } from '../Asset/styleAdmin';

const rate = [
    { label: 'น้อยที่สุด', value: 1 },
    { label: 'น้อย', value: 2 },
    { label: 'ปานกลาง', value: 3 },
    { label: 'มาก', value: 4 },
    { label: 'มากที่สุด', value: 5 },
]

export default class PageRate extends Component {
    constructor(props) {
        super(props)

        this.state = {
            app_rate: null,
            solving_rate: null,
            message: null,
        }
    }

    updatetypeInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    selectAppRate = (e) => {
        this.setState({ app_rate: e })
    }

    selectSolvingRate = (e) => {
        this.setState({ solving_rate: e })
    }

    sentComment = async () => {
        const { app_rate, solving_rate, message } = this.state
        if (app_rate !== null && solving_rate !== null || message) {
            await post('/rate/insert', { app_rate, solving_rate, message, type: "WEB" })
            swal({
                title: "ประเมินความพึงพอใจเรียนร้อยแล้ว",
                // title: "ขอขอบคุณสำหรับการประเมิน?",
                text: "ผลประเมินความพึงพอใจ และข้อเสนอแนะที่ได้รับจากท่านเป็นประโยชน์อย่างยิ่งในการพัฒนา และปรับปรุงระบบการให้บริการของศูนย์ดำรงธรรมจังหวัดขอนแก่น",
                icon: "success",
            })
                .then(() => {
                    // if (willDelete) {
                        // setTimeout(() => {
                            window.location.reload();
                        // }, 250)
                    // }
                });
        } else {
            swal("", "โปรดประเมินความพึงพอใจให้ครบ", "warning");
        }

    }

    render() {
        return (
            <Main>
                <TextHead title='ประเมินความพึงพอใจ' />
                <div className='-rate' >
                    <div className='-rate-b'>
                        <strong>ท่านพึงพอใจกับการให้บริการผ่านเว็บไซต์ศูนย์ดำรงธรรมจังหวัดขอนแก่นมากน้อยเพียงใด</strong>
                        <div className='-rate-select' >
                            <div class="radio-tile-group">
                                {
                                    rate.map((el) => {
                                        return (
                                            <div class="input-container" onClick={this.selectAppRate.bind(null, el.value)} key={el.value}>
                                                <input id={'rate-service-' + el.value} class="radio-button" type="radio" name="service" />
                                                <div class="radio-tile">
                                                    <label for={'rate-service-' + el.value.value} class="radio-tile-label">{el.value}</label>
                                                </div>
                                                <span>{el.label}</span>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className='-rate-b'>
                        <strong>ท่านพึงพอใจกับการแก้ไขปัญหาเรื่องร้องเรียน ร้องทุกข์ ผ่านเว็บไซต์ศูนย์ดำรงธรรมจังหวัดขอนแก่นมากน้อยเพียงใด</strong>
                        <div className='-rate-select' >
                            <div class="radio-tile-group">
                                {
                                    rate.map(el => {
                                        return (
                                            <div class="input-container" onClick={this.selectSolvingRate.bind(null, el.value)} key={el.value}>
                                                <input id={'rate-solution-' + el.value} class="radio-button" type="radio" name="solution" />
                                                <div class="radio-tile" >
                                                    <label for={'rate-solution-' + el.value} class="radio-tile-label">{el.value}</label>
                                                </div>
                                                <span>{el.label}</span>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className='center-grid' >
                        <div className='-rate-textarea'>
                            <label>ข้อเสนอแนะเพิ่มเติม</label>
                            <textarea name='message' onChange={this.updatetypeInput} placeholder="กรอกข้อเสนอแนะ" className='styleScroll' />
                        </div>
                        <Button onClick={this.sentComment} className='btn-send-blue jus-s-center' > ส่งการประเมิน </Button>
                    </div>
                    {/* <pre>{JSON.stringify(this.state, null, '\t')}</pre> */}
                </div>
            </Main>
        )
    }
}