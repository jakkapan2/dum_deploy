import React, { Component } from 'react'
import TextHead from '../Components/TextHead'
import Main from '../Components/Main'
import Images from '../Components/Images';
import { get } from '../api';
import Pagination from '../Components/Pagination/Pagination';
import { Search } from '../Components/Search/Search'
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class PageHotline extends Component {
    constructor(props) {
        super(props)

        this.state = {
            oldHotline: [],
            hotline: [],
            currentHotline: [],
            currentPage: 1,
        }
    }

    componentDidMount = () => {
        this._fetchHotline()
    }

    _fetchHotline = async () => {
        this.setState({ loadingSave: true });
        try {
            const hotline = await get('/hotline')
            this.setState({ hotline, oldHotline: hotline, currentHotline: hotline })
            this.setState({ loadingSave: false });
        } catch (error) {
            console.error(error.message)
            this.setState({ loadingSave: false });
        }
    }
    // #################################################################################################
    // ######################################## pagination #############################################
    // #################################################################################################
    onPageChanged = data => {
        let { currentPage } = data;
        this.setState({ currentPage })
    }
    // ################################### ########### ##################################################

    searchHotline = (e) => {
        const value = e.target.value
        if (value !== '') {
            let newsolution = this.state.oldHotline.filter(el => {
                return (
                    el.title.includes(value) || el.phone.includes(value) || el.location.includes(value)
                )
            })
            this.setState({ hotline: newsolution, currentHotline: newsolution.slice(0, 10), totalHotline: newsolution.length / 10 })
        } else {
            this.setState(prev => ({ hotline: prev.oldHotline, currentHotline: prev.oldHotline.slice(0, 10) }))
        }
    }

    render() {
        return (
            <Main>
                <div className="d-grid justify-content-center">
                    <TextHead title='สายด่วน' />
                    <Search placeholder="ค้นหา" onChange={this.searchHotline} />
                </div>
                <div className='admin-hotline-center setminHeight' >
                    <div className='row' >
                        {this.renderHotline()}
                        {this.state.loadingSave ? <div style={{ position: 'relative', height: '300px', width:'100%' }}><LoaderSaving /></div> : ''}
                    </div>
                </div>
            </Main>
        )
    }
    renderHotline = () => {
        const { currentHotline, currentPage, hotline } = this.state;
        const totalHotline = currentHotline.length;
        const pageLimit = 8;
        let data = hotline.slice((currentPage - 1) * pageLimit, currentPage * pageLimit)
        let dataLength = hotline.length;
        if (totalHotline === 0) return null;
        return (
            <div className="container mb-5">
                <div className="admin-hotline-list">
                    {hotline && data.map((el, i) => {
                        return <div className='admin-hotline-fx' key={el.hotline_id} >
                            <div className='admin-hotline-rad' >
                                <img src={Images.logo} alt='' />
                            </div>
                            <div className='admin-hotline-text'>
                                <h1>{el.title}</h1>
                                <p><img src={Images.Asset17} alt='' />{el.location}</p>
                                <p><img src={Images.Asset18} alt='' />{el.phone}</p>
                                {el.link && <a href={el.link} target='_blank' ><img src={Images.Asset19} alt='' />{el.title}</a>}
                            </div>
                        </div>
                    })}
                </div>
                <div className="w-100 d-flex align-items-center justify-content-center">
                    <Pagination totalRecords={dataLength} pageLimit={pageLimit} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                </div>
            </div>
        );
    }
}
