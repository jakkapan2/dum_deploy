import React, { Component } from 'react';
import Images from '../Components/Images';
import CenterGd from '../Components/CenterGd';
import swal from 'sweetalert';
import User from '../mobx/user';
import { post } from '../api';
import { observer } from 'mobx-react';
import Modal from 'react-responsive-modal';
import InputMask from 'react-input-mask';
import { withRouter } from 'react-router-dom';
// import parser from 'postcss-selector-parser';
import { messaging } from '../init-fcm';
import { InputFormGroup } from '../Components/Form/Input'

@observer
@withRouter
class PageLogin extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: '',
			oldpassword: '',
			id: '',
			openModal: false,
			openModalFGP: false,
			openModalFGU: false,
			openModalResetP: false,
			id_number: '',
			id_card: null,
			back_id_card: null,
			user_forgot: null
		};
	}

	componentDidMount = async () => {
		try {
			await messaging.requestPermission();
			const token = await messaging.getToken();
			console.log('token', token);
			User.saveToken(token);
		} catch (e) {
			console.log(e);
		}

		const { push } = this.props.history;
		switch (User.role) {
			case 'ADMIN':
				return push('/adminreport');
			case 'USER':
				return push('/');
			default:
				return;
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_handleKeyPress = async (e) => {
		// console.log(e.key);
		if (e.key === 'Enter') {
			let { username, password } = this.state;
			try {
				if (username && password) {
					const result = await post('/auth/login', {
						username,
						password,
						noti_token: User.token
					});
					// console.log('result', result);
					User.saveUser(result);
					switch (User.role.toUpperCase()) {
						case 'ADMIN':
							this.props.history.push('/adminreport');
							break;
						case 'USER':
							this.props.history.push('/');
							break;
						case 'CENTRAL':
							this.props.history.push('/subadminreport');
							break;
						default:
							break;
					}
					// if (result.success) {
					//     User.saveUser(result)
					//     switch (User.role.toUpperCase()) {
					//         case "ADMIN": this.props.history.push('/adminreport'); break;
					//         case "USER": this.props.history.push("/"); break;
					//         default: break;
					//     }
					// }
				}
			} catch (error) {
				console.log(error);
				swal('ล็อคอินผิดพลาด', 'โปรดตรวจสอบ username หรือ password', 'error');
			}
		} else if (e.key === 'Backspace' || e.key === 'Delete') {
			this.setState({});
		}
	};

	// -----------------------------------------------------------------------------------------------------------------------
	onOpenModal = () => {
		this.setState({ openModal: true });
	};
	onCloseModal = () => {
		this.setState({ openModal: false, openModalFGP: false, openModalFGU: false, openModalResetP: false });
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_lookReport = () => {
		let { id_number } = this.state;
		this.props.history.push({
			pathname: '/looktracking',
			state: { id_number }
		});
	};
	_handleKeyPresslookReport = async (e) => {
		// console.log(e.key);
		if (e.key === 'Enter') {
			let { id_number } = this.state;
			this.props.history.push({
				pathname: '/looktracking',
				state: { id_number }
			});
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_onLoginGOAHEAD = () => this.props.history.push('/');
	// -----------------------------------------------------------------------------------------------------------------------
	_onLogin = async () => {
		try {
			const { username, password } = this.state;
			const result = await post('/auth/login', { username, password, noti_token: User.token });
			// console.log(result);
			// alert(JSON.stringify(result))

			User.saveUser(result);
			switch (User.role.toUpperCase()) {
				case 'ADMIN':
					this.props.history.push('/adminreport');
					break;
				case 'USER':
					this.props.history.push('/');
					break;
				case 'CENTRAL':
					this.props.history.push('/subadminreport');
					break;
				default:
					break;
			}
		} catch (error) {
			console.log(error);
			swal('login failed', 'login error', 'error');
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_goRegister = () => this.props.history.push('/register');
	// -----------------------------------------------------------------------------------------------------------------------
	swalCheck = (message) => {
		swal('คำเตือน!', message, 'warning');
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_onForgotUsername = async () => {
		let { id_card, back_id_card } = this.state
		console.log('this.state', this.state);
		if (id_card === null || back_id_card === null) {
			this.swalCheck('กรุณากรอกหมายเลขบัตรประชาชน หรือหมายเลขหลังบัตรประชาชนให้ถูกต้อง')
		} else if (id_card.length !== 17 || back_id_card.length !== 14) {
			this.swalCheck('กรุณากรอกหมายเลขบัตรประชาชน หรือหมายเลขหลังบัตรประชาชนให้ถูกต้อง')
		} else {
			let id_cards = id_card.replace(/(-)/g, '')
			let back_id_cards = back_id_card.replace(/(-)/g, '')
			try {
				const result = await post('/auth/forgot_user', { id_card: id_cards, rear_id_card: back_id_cards.toLowerCase() })
				if (result) {
					this.setState({ id_card: null, back_id_card: null, user_forgot: null })
					swal("", "ชื่อผู้ใช้ (Username) ของคุณคือ \n " + result, "success").then(() => { window.location.reload() })
				}
			} catch (error) {
				swal("", "ไม่พบชื่อผู้ใช้ (Username) \n ในหมายเลขบัตรประชาชนนี้! ", "warning")
			}
		}
	}
	_onForgotPassword = async () => {
		let { id_card, back_id_card, user_forgot } = this.state
		if (user_forgot === '' || user_forgot === null) {
			this.swalCheck('กรุณากรอกชื่อผู้ใช้ (Username)')
		} else if (id_card === null || back_id_card === null) {
			this.swalCheck('กรุณากรอกหมายเลขบัตรประชาชน หรือหมายเลขหลังบัตรประชาชนให้ถูกต้อง')
		} else if (id_card.length !== 17 || back_id_card.length !== 14) {
			this.swalCheck('กรุณากรอกหมายเลขบัตรประชาชน หรือหมายเลขหลังบัตรประชาชนให้ถูกต้อง')
		} else {
			let id_cards = id_card.replace(/(-)/g, '')
			let back_id_cards = back_id_card.replace(/(-)/g, '')
			try {
				const result = await post('/auth/forgot_password', { id_card: id_cards, rear_id_card: back_id_cards, username: user_forgot })
				if (result) {
					this.setState({ id_card: null, back_id_card: null, password:'', oldpassword:'' })
					this.setState({ openModalFGP: false, openModalResetP: true })
				}
			} catch (error) {
				swal("", "ไม่พบข้อมูลผู้ใช้นี้! ", "warning")
			}
		}
	}
	_onResetPassword = async () => {
		let { user_forgot, password, oldpassword } = this.state

		if (password === '' || password === null) {
			this.swalCheck('กรุณากรอกรหัสผ่าน')
		} else if (password !== oldpassword) {
			this.swalCheck('รหัสผ่านไม่ตรงกัน')
		} else {
			try {
				const result = await post('/auth/reset_password', { username: user_forgot, password })
				if (result) {
					this.setState({ openModalResetP: false, id_card: null, back_id_card: null, user_forgot: null })
					swal("เปลี่ยนรหัสผ่านสำเร็จ", "", "success").then(() => { window.location.reload() })
				}
			} catch (error) {
				console.log('error', error);
			}
		}
	}
	// -----------------------------------------------------------------------------------------------------------------------
	_onChangeText = (e) => this.setState({ [e.target.name]: e.target.value });
	// -----------------------------------------------------------------------------------------------------------------------
	render() {
		const { username, password, openModal, openModalFGP, openModalFGU, id_card, back_id_card, user_forgot, openModalResetP, oldpassword } = this.state;
		return (
			<CenterGd cn="height100vh">
				<div style={{ display: 'flex', flexDirection: 'column' }}>
					<div className="login-img">
						<img src={Images.logo} alt="" />
						<h2 id="logoh2">ศูนย์ดำรงธรรม</h2>
						<h5 id="logoh5">จังหวัดขอนแก่น</h5>
					</div>
					<div className="row">
						<div className="col-md-6 label-button">
							<button className="btn-blue" onClick={this._goRegister}>
								ลงทะเบียน
							</button>
							<button className="btn-orange" onClick={this.onOpenModal}>
								ดูสถานะเรื่อง
							</button>
						</div>
						<div className="col-md-6 label-login">
							<h3>เข้าสู่ระบบ</h3>
							<div className="login-User">
								<img src={Images.username} />
								<input
									type="text"
									placeholder="ชื่อผู้ใช้"
									className="form-control form-group"
									name="username"
									value={username}
									onChange={this._onChangeText}
									onKeyDown={this._handleKeyPress}
								/>
							</div>
							<div className="login-password">
								<img src={Images.password} />
								<input
									type="password"
									placeholder="รหัสผ่าน"
									className="form-control form-group"
									name="password"
									value={password}
									onChange={this._onChangeText}
									onKeyDown={this._handleKeyPress}
								/>
							</div>
							<div className="d-flex justify-content-around">
								<span style={{cursor:'pointer', color:'#555', textDecoration:'underline'}} onClick={() => this.setState({ openModalFGP: true })}>ลืมรหัสผ่าน</span>
								<span style={{cursor:'pointer', color:'#555', textDecoration:'underline'}} onClick={() => this.setState({ openModalFGU: true })}>ลืมชื่อผู้ใช้งาน</span>
							</div>
							{/* <button onClick={this._onLoginGOAHEAD} className='btn-blue'>เข้าสู่ระบบ</button> */}
							<button onClick={this._onLogin} className="btn-blue">
								เข้าสู่ระบบ
							</button>
						</div>
					</div>
				</div>
				{/* <pre>{JSON.stringify(this.state, null, '\t')}</pre> */}
				<img src={Images.bglogin} alt="bglogin" id="bglogin" />
				<Modal
					open={openModal}
					onClose={this.onCloseModal}
					little
					classNames={{ modal: 'Look-modal trackingSmall ' }}
				>
					<div className="layout">
						<h1 className="mt-0">ติดตามเรื่องและสถานะเรื่อง</h1>
						<p>ค้นหาโดยหมายเลขประจำตัวประชาชนหรือรหัสเรื่องร้องทุกข์ หรือรหัส MOI</p>
						<IDInput
							onChange={this._onChangeText}
							name="id_number"
							inputMode="numeric"
							type="text"
							className="form-control form-group"
							onKeyDown={this._handleKeyPresslookReport}
						/>
						<button className="btn-blue" onClick={this._lookReport}>
							ค้นหา
						</button>
					</div>
				</Modal>
				<Modal
					open={openModalFGP}
					onClose={this.onCloseModal}
					little
					classNames={{ modal: 'Look-modal trackingSmall ' }}
				>
					<div className="layout p-4">
						<h4 className="mt-0 mb-3">กรุณากรอกข้อมูล</h4>
						<div className="d-grid text-left">
							<InputFormGroup title="ชื่อผู้ใช้ (Username)" name="user_forgot" placeholder={'กรุณากรอกชื่อผู้ใช้'} value={user_forgot} onChange={this._onChangeText} />
							<label className="form-group">หมายเลขบัตรประชาชน</label>
							<InputMask
								maskChar={null}
								mask={'9-9999-99999-99-9'}
								onChange={this._onChangeText}
								placeholder={'กรุณากรอกหมายเลขบัตรประชาชน 13 หลัก'}
								name={'id_card'}
								inputMode="numeric"
								type="text"
								className="form-control form-group"
								value={id_card}
							/>
							<label className="form-group">หมายเลขหลังบัตรประชาชน</label>
							<InputMask
								maskChar={null}
								mask={'aa9-9999999-99'}
								onChange={this._onChangeText}
								placeholder={'กรุณากรอกหมายเลขหลังบัตรประชาชน'}
								name={'back_id_card'}
								inputMode="numeric"
								type="text"
								className="form-control form-group"
								value={back_id_card}
							/>
						</div>
						<button className="btn-blue" onClick={this._onForgotPassword}>
							ส่ง
						</button>
					</div>
				</Modal>
				<Modal
					open={openModalFGU}
					onClose={this.onCloseModal}
					little
					classNames={{ modal: 'Look-modal trackingSmall ' }}
				>
					<div className="layout p-4">
						<h4 className="mt-0 mb-3">กรุณากรอกข้อมูล</h4>
						<div className="d-grid text-left">
							<label className="form-group">หมายเลขบัตรประชาชน</label>
							<InputMask
								maskChar={null}
								mask={'9-9999-99999-99-9'}
								onChange={this._onChangeText}
								placeholder={'กรุณากรอกหมายเลขบัตรประชาชน 13 หลัก'}
								name={'id_card'}
								inputMode="numeric"
								type="text"
								className="form-control form-group"
								value={id_card}
							/>
							<label className="form-group">หมายเลขหลังบัตรประชาชน</label>
							<InputMask
								maskChar={null}
								mask={'aa9-9999999-99'}
								onChange={this._onChangeText}
								placeholder={'กรุณากรอกหมายเลขหลังบัตรประชาชน'}
								name={'back_id_card'}
								inputMode="numeric"
								type="text"
								className="form-control form-group"
								value={back_id_card}
							/>
						</div>
						<button className="btn-blue" onClick={this._onForgotUsername}>
							ค้นหา
						</button>
					</div>
				</Modal>
				<Modal
					open={openModalResetP}
					onClose={this.onCloseModal}
					little
					classNames={{ modal: 'Look-modal trackingSmall ' }}
				>
					<div className="layout p-4" >
						<h4 className="mt-0">กรอกรหัสผ่านใหม่</h4>
						<InputFormGroup
							title="กรุณากรอกรหัสผ่านใหม่"
							name={'password'}
							type="password"
							mark={'กรุณากรอกรหัสผ่านอย่างน้อย 8 ตัว'}
							value={password}
							onChange={this._onChangeText} />
						<InputFormGroup
							title="กรุณากรอกยืนยันรหัสผ่าน"
							name={'oldpassword'}
							type="password"
							value={oldpassword}
							onChange={this._onChangeText} />

						<button className="btn-blue" onClick={this._onResetPassword}>
							เปลี่ยนรหัสผ่าน
						</button>
					</div>
				</Modal>
			</CenterGd>
		);
	}
}
const IDInput = (props) => (
	<InputMask
		maskChar={null}
		// mask={'9-9999-99999-99-9'}
		{...props}
		placeholder="หมายเลขประจำตัวประชาชนหรือรหัสเรื่องร้องทุกข์ หรือรหัส MOI"
	/>
);

export default PageLogin;
