import React, { Component } from 'react'
import _ from 'lodash';
import User from '../mobx/user'
import Main from '../Components/Main'
import { TextLine } from '../Components/TextHead';
import TextHead from '../Components/TextHead';
import { SumTrack, Sumtext, SumDetail } from '../Components/Div/SetDiv';
import Images from '../Components/Images'
import { post, path } from '../api'
import { address } from '../functions/index';
import { Department, Person, Company } from '../Components/Form/FormComp'
import MapComponent from '../Components/Maps/MapComponent';
import swal from 'sweetalert'
import Modal from 'react-responsive-modal';
import { Container, Row, Col } from 'reactstrap';
import 'moment/locale/th';
import moment from 'moment';
moment.locale('th');

export default class PageLookDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            complaint: [],
            openPic: false,
            showImage: null,
            openModal: false,
            username: '',
            password: '',
            id: '',
        }
    }

    componentDidMount() {
        this.fetchComplaint()
        const { push } = this.props.history
        switch (User.role) {
            case "ADMIN":
                return push("/")
            case "USER":
                return push("/")
            default:
                return
        }
    }

    fetchComplaint = async () => {
        let complaint = this.props.location.state.row
        // console.log('form_id', this.props.location.state.id_number)
        // test deploy with git ignore
        this.setState({ complaint })
        console.log('complaint', complaint)
    }
    _lookReport = () => {
        this.props.history.push({
            pathname: '/looktracking',
            state: {
                id_number: this.props.location.state.id_number,
            }
        })
    }
    onOpenModal = () => { this.setState({ openModal: true }); };
    onOpenPic = (e) => {
        this.setState({ openPic: true, showImage: e });
    };
    onCloseModal = () => {
        this.setState({ openPic: false, openModal: false });
    };

    // -----------------------------------------------------------------------------------------------------------------------
    _onLogin = async () => {
        try {
            const { username, password } = this.state
            const result = await post("/auth/login", { username, password })
            User.saveUser(result)
            this.props.history.push("/")
        } catch (error) {
            swal("login failed", error.message, "error")
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------
    _onChangeText = e => this.setState({ [e.target.name]: e.target.value })
    // -----------------------------------------------------------------------------------------------------------------------

    render() {
        const { username, password, openModal, complaint } = this.state
        let nameFile = complaint.evidence_files && complaint.evidence_files
            .map((e) => {
                let link = e.path;
                let name = e.filename;
                let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0]))).add(543, 'y').format('DD MMMM YYYY');
                return { link: link, name: name, date: date, type: 'pdf' };
            })
            .concat(
                complaint.evidence_pic && complaint.evidence_pic.map((e) => {
                    let link = e.path;
                    let name = e.filename;
                    let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0]))).add(543, 'y').format('DD MMMM YYYY');
                    return { link: link, name: name, date: date, type: 'jpg' };
                })
            );
        let { history } = this.props.location.state.row
        return (
            <div className="App" >
                <div className="setCenter" >
                    <header className='NavHeader-notlogin' >
                        <div className='nav-head-cen'>
                            <img src={Images.Asset42} alt='' />
                        </div>
                        <div className='nav-head' >
                            <a onClick={this._lookReport} className='a_back' style={{ margin: 0 }} > กลับ</a>
                            <span className='btn-login-track' onClick={() => this.props.history.push('/login')}>เข้าสู่ระบบ</span>
                        </div>
                    </header>
                    <Main>
                        <TextHead title="ติดตามเรื่องร้องเรียน" />
                        <br />
                        <div className="row">
                            <div className="-comp-look-content -comp-look-content-nologin col-md-9  px-0">
                                {/* <div className="col-md-12">
                                    <SumTrack title="หัวข้อเรื่องร้องเรียน" detail={complaint.subject} />
                                </div> */}
                                <div className="col-md-12">
                                    <SumTrack title="สถานะเรื่องร้องเรียน" detail={complaint.status_name} />
                                </div>
                                <div className="col-md-7">
                                    <SumTrack title="รหัสเรื่องร้องเรียน" detail={complaint.form_id} />
                                </div>
                                <div className="col-md-5">
                                    <SumTrack title="รหัส MOI" detail={complaint.moi ? complaint.moi : '-'} />
                                </div>
                                <div className="col-md-7">
                                    <SumTrack
                                        title="แจ้งเรื่องวันที่"
                                        detail={moment(complaint.timestamp).add(543, 'y').format('DD MMMM YYYY HH:MM') + ' น.'}
                                    />
                                </div>
                                <div className="col-md-5">
                                    <SumTrack
                                        title="วันที่รับเรื่อง"
                                        detail={complaint.receive_at ? moment(complaint.receive_at).add(543, 'y').format('DD MMMM YYYY HH:MM') + ' น.' : ' - '}
                                    />
                                </div>
                                <div className="col-md-5">
                                    <SumTrack title="เจ้าหน้าที่หน่วยงานที่รับผิดชอบ"
                                        detail={history.length > 1 ? history.sort((a, b) => moment(b.datetime) - moment(a.datetime))[0].fname + ' ' +
                                            history.sort((a, b) => moment(b.datetime) - moment(a.datetime))[0].lname : '-'} />
                                </div>
                            </div>
                        </div>
                        <TextLine title="เรื่องร้องทุกข์" request />
                        <Sumtext title="หัวข้อร้องทุกข์" detail={complaint.subject} wl="160px" />
                        <Sumtext title="ประเภทเรื่อง" detail={complaint.type} wl="160px" />
                        <Sumtext title="ประเภทเรื่องย่อย" detail={complaint.sub_type} wl="160px" />
                        <TextLine title="ข้อมูลผู้ร้อง" request />
                        {complaint.disclose == 0 ? (
                            <Sumtext title="ปกปิดข้อมูล" />
                        ) : (
                                <div>
                                    <Sumtext title="ชื่อ-นามสกุลผู้ร้อง" detail={complaint.prefix + ' ' + complaint.fname + ' ' + complaint.lname} wl="160px" />
                                    <Sumtext title="หมายเลขประจำตัวประชาชน" detail={complaint.id_card} wl="160px" />
                                    <Sumtext title="หมายเลขโทรศัพท์" detail={complaint.phone} wl="160px" />
                                    {/* <Sumtext title="E-mail ผู้ร้องทุกข์" detail={complaint.email} wl="160px" /> */}
                                    <Sumtext
                                        title="ที่อยู่"
                                        detail={
                                            address({
                                                ..._.pick(complaint, [
                                                    'house_number',
                                                    'moo',
                                                    'alleyway',
                                                    'road',
                                                    'sub_district',
                                                    'district',
                                                    'province'
                                                ])
                                            }) + complaint.zipcode
                                        }
                                        wl="160px"
                                    />
                                </div>
                            )}
                        <TextLine title="ผู้ถูกร้องทุกข์" request />
                        {complaint.complainant === 'DEPARTMENT' ? (
                            <Department item={complaint.complainant_data} />
                        ) : complaint.complainant === 'PERSON' ? (
                            <Person item={complaint.complainant_data} />
                        ) : (
                                    null    // <Company item={complaint.complainant_data} />
                                )}
                        {/* -------------------------------------------------------------- */}
                        <TextLine title="รายละเอียดคำร้อง" request />
                        <Row>
                            <Col xs={7}>
                                <MapComponent markerCoords={{ lat: complaint.lat, lng: complaint.lng }} onMarkerChange={(e) => console.log('e', e)} />
                            </Col>
                            <Col xs={5} className="pl-3">
                                <Sumtext title="สถานที่เกิดเหตุ : " detail={complaint.location_name} wl="100px" />
                                <SumDetail title="รายละเอียด : " detail={complaint.detail?complaint.detail:'-'} />
                                <SumDetail title="วัตถุประสงค์ : " detail={complaint.objective?complaint.objective:'-'} />
                            </Col>
                        </Row>
                        {/* -------------------------------------------------------------- */}
                        <div className="mb-5">
                            <TextLine title="ไฟล์เอกสาร" request />
                            {nameFile && nameFile.length > 0 ? (
                                nameFile.map(
                                    (e) =>
                                        e.type === 'pdf' ? (
                                            <a className={'depart-file  pdf'} href={`${path}${e.link}`} target="_blank">
                                                <div>{e.name}</div>
                                                <div>{e.date}</div>
                                            </a>
                                        ) : (
                                                <a
                                                    className={'depart-file jpg'}
                                                    onClick={() => this.onOpenPic(path + e.link)}
                                                >
                                                    <div>{e.name}</div>
                                                    <div>{e.date}</div>
                                                </a>
                                            )
                                )
                            ) : (
                                    <div>---ไม่มีรายการ---</div>
                                )}
                        </div>
                        <TextLine title="รายละเอียดการดำเนินงาน" request />
                        <div className="styleScroll mb-5">
                            {complaint.trackinHistory && complaint.trackinHistory.length > 0 ? (
                                complaint.trackinHistory.sort((a, b) => moment(b.datetime) - moment(a.datetime)).map((e) => (
                                    <div className="depart-tracking">
                                        <div>
                                            <div>
                                                <div className="text-left">{e.fname + ' ' + e.lname}</div>
                                                <span>{moment(e.datetime).add(543, 'y').format('DD MMMM YYYY HH:mm')} น.</span>
                                            </div>
                                            <div>
                                                <span style={{ color: '#777' }}>
                                                    {moment(e.datetime).add(543, 'y').format('DD MMMM YYYY HH:mm')} น.
										    </span>
                                            </div>
                                        </div>
                                        <div>{e.description}</div>
                                        <div className="depart-about-file">
                                            {e.files.length > 0 && 'ไฟล์ที่เกี่ยวข้อง :'}
                                            {e.files.length > 0 ? (
                                                e.files.map((e) => (
                                                    <a className="pdf" href={`${path}${e}`} target="_blank">
                                                        {e.split('/')[2]}
                                                    </a>
                                                ))
                                            ) : (
                                                    ''
                                                )}
                                        </div>
                                        <div>
                                            {e.pic.length === 1 ? (
                                                e.pic.map(
                                                    (e) =>
                                                        <img src={path + e} alt="" onClick={() => this.onOpenPic(path + e)} />
                                                )
                                            ) : (
                                                    e.pic.length > 0 &&
                                                    e.pic.map((e) => <img src={path + e} alt="" onClick={() => this.onOpenPic(path + e)} />)
                                                )}
                                        </div>
                                        <hr />
                                    </div>
                                ))
                            ) : (
                                    <div className="text-center">--- ไม่มีรายละเอียดการดำเนินงาน ---</div>
                                )}
                        </div>
                    </Main>
                </div>
                <Modal
                    open={this.state.openPic}
                    onClose={this.onCloseModal}
                    little
                    classNames={{ modal: 'Look-modal ChangeStatus ' }}
                >
                    <img
                        src={this.state.showImage}
                        style={{ width: '100%', maxWidth: '900px', display: 'flex' }}
                    />
                </Modal>
                <Modal
                    open={openModal}
                    onClose={this.onCloseModal}
                    little
                    classNames={{ modal: 'Look-modal trackingSmall ' }}>
                    <div className="layout">
                        <h2 style={{ marginBottom: 30 }} >เข้าสู่ระบบ</h2>
                        <input type='text' placeholder="ชื่อผู้ใช้" className='form-control form-group login-User' name='username' value={username} onChange={this._onChangeText} />
                        <input type='password' placeholder="รหัสผ่าน" className='form-control form-group login-password' name='password' value={password} onChange={this._onChangeText} />
                        <button onClick={this._onLogin} className='btn-blue'>เข้าสู่ระบบ</button>
                    </div>
                </Modal>
            </div >
        )
    }
}
