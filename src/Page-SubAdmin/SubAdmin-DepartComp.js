import React, { Component } from 'react';
import {
	PanelRight,
	PanelContent,
	PanelHead,
	PanelWhi,
	PanelWhiMar,
	PanelWhiMargL
} from '../Components/PartAdmin/MainAdmin';
import { Bgblue, HeadLeft, HeadBtn, HeadRight } from '../Asset/styleAdmin';
import { SumTrack } from '../Components/Div/SetDiv';
import { LeftRight, ContentLeft, ContentRight, ContainerRadioRow, Label, LabelLight } from '../Asset/styled';
import color from '../Asset/color';
import { changeStatusRadio } from '../Components/Static/static';
import { post, get, path } from '../api';
import moment from 'moment';
import { DetailComp, History, Head_compaint } from '../Components/Form/FormComp';
import Modal from 'react-responsive-modal';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import { TextLine } from '../Components/TextHead';
import { RadioRow, RadioFormGroup } from '../Components/Form/Input';
import ModalCompaint from '../Components/Modal/ModalAdminCompaint';
import ModalPerson from '../Components/Modal/ModalPerson';
import LoaderSaving from '../Components/Load/LoaderSaving';
import USER from '../mobx/user';
import Countdown from '../functions/Countdown';
import QRCode from 'qrcode.react';
import 'moment/locale/th';
moment.locale('th');

// export default class AdminDepartComp extends Component {
class SubAdminDepartComp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form_id: '',
			result: null,
			showPopup: true,
			openModal: false,
			openPic: false,
			showImage: null,
			radiorow: '',
			focut_head: 1,
			intensive: [],
			Modalcompaint: false,
			status_id: null,
			loadingSave: false,
			//----------------
			files: [],
			descrition: '',
			nameFile: [],
			// ---------------
			central: [],
			category_id: [],
			description_transfer: null,
			expire: null,
			chat_staff: [],
			ModalPersons: false,
			waite: 0,
			formstaff_join: [],
			message: '',
			openQRcode: false,
			showQRcode: '',
			readIntensive: false,
			rightIntensive: false,
			readChat: false
		};
	}
	// ------------------------------------------------------------------------------------------------------------------
	componentWillMount() {
		this.comPlaint();
		this.Intensive();
		this.formstaff_join();
		this.central();
		this.readIntensive();
		this.readChat();
		this.chat_staff();
	}

	// componentDidMount = async () => {
	// 	this.interval = setInterval(() => {
	// 		this.readIntensive();
	// 		this.readChat();
	// 		this.chat_staff();
	// 	}, 2000);
	// };
	// componentWillUnmount() {
	// 	clearInterval(this.interval);
	// }
	chat_staff = async () => {
		const form_id = this.props.location.state.id;
		let res = await get('/chat/staff/' + form_id);
		// console.log('res', res);
		this.setState({ chat_staff: res });
	};
	formstaff_join = async () => {
		const form_id = this.props.location.state.id;
		let res = await get('/form/staff_join/' + form_id);
		this.setState({ formstaff_join: res });
		// console.log('res', res);
	};
	async comPlaint() {
		const form_id = this.props.location.state.id;
		// console.log('form_id', form_id);
		try {
			const result = await post(`/complaint/form_tracking`, { form_id });
			const transfer_back = await post(`/complaint/get_tranfer`, {
				category_id: USER.central_id,
				form_id,
				id_user: USER.user_id
			});
			// console.log('form_tracking', result);
			console.log('transfer_back', transfer_back);
			let nameFile = result.evidence_files
				.map((e) => {
					let link = e.path;
					let name = e.filename;
					let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
						.add(543, 'y')
						.format('DD MMMM YYYY');
					return { link: link, name: name, date: date, type: 'pdf' };
				})
				.concat(
					result.evidence_pic.map((e) => {
						let link = e.path;
						let name = e.filename;
						let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
							.add(543, 'y')
							.format('DD MMMM YYYY');
						return { link: link, name: name, date: date, type: 'jpg' };
					})
				);
			this.setState({
				result: result,
				radiorow: result.status,
				nameFile,
				status_id: result.status,
				transfer_back: transfer_back[0]
			});
		} catch (error) {
			console.log('complant', error);
		}
	}
	// ------------------------------------------------------------------------------------------------------------------
	async central() {
		try {
			const result = await get(`/central/`);
			this.setState({ central: result.slice(1) });
			// console.log('central', this.state.central);
		} catch (error) {
			console.log('central', error);
		}
	}
	// ------------------------------------------------------------------------------------------------------------------
	async Intensive() {
		let form_id = this.props.location.state.id;
		let res = await get('/form/precipitation/' + form_id);
		// console.log('res', res);
		this.setState({ intensive: res });
	}
	//-------------------------------------------------------------------------------------------------------------------
	readIntensive = async () => {
		let form_id = this.props.location.state.id;
		let read = await post('/form/get_read_precipitation/', { form_id });
		let r = read.filter(
			(e) =>
				parseInt(e.user_id) === parseInt(USER.user_id) &&
				parseInt(e.form_id) === parseInt(this.props.location.state.id)
		);
		this.setState({
			readIntensive: r.length > 0 ? r.some((e) => e.read === 1) : true,
			rightIntensive: r.length > 0 ? true : false
		});
	};
	updateIntensive = async () => {
		let form_id = this.props.location.state.id;
		let user_id = USER.user_id;
		await post('/form/read_precipitation/', { form_id, user_id });
		this.readIntensive();
	};
	readChat = async () => {
		let form_id = this.props.location.state.id;
		let read = await post('/chat/get_read_chat', { form_id });
		let r = read.filter(
			(e) =>
				parseInt(e.user_id) === parseInt(USER.user_id) &&
				parseInt(e.form_id) === parseInt(this.props.location.state.id)
		);
		this.setState({
			readChat: r.length > 0 ? r.some((e) => e.read === 1) : true
		});
		// console.log('read', read);
	};
	updatereadChat = async () => {
		let form_id = this.props.location.state.id;
		let user_id = USER.user_id;
		await post('/chat/read_chat', { form_id, user_id });
		this.readChat();
	};
	// ------------------------------------------------------------------------------------------------------------------
	updatetypeInput = (e) => {
		this.setState({ [e.target.name]: e.target.value, status_id: e.target.value });
	};
	// ------------------------------------------------------------------------------------------------------------------
	saveChangeStatus = async () => {
		try {
			let obj = {
				form_id: this.state.result.form_id,
				status: Number(this.state.status_id)
			};
			await post('/form/change_status', obj);
			swal({
				title: 'เรียบร้อย',
				text: 'เปลี่ยนสถานะการดำเนินการเรียบร้อย',
				icon: 'success'
			}).then(() => {
				// if (willDelete) {
				window.location.reload();
				// this.props.history.push('/tracking')
				// }
			});
			this.setState({ openModal: false });
		} catch (error) {
			swal('', error, 'warning');
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	onOpenModal = () => {
		this.setState({ openModal: true });
	};
	onOpenPic = (e) => {
		this.setState({ openPic: true, showImage: e });
	};
	onOpeQRcode = (e) => {
		this.setState({ openQRcode: true, showQRcode: e });
	};
	onCloseModal = () => {
		this.setState({ openModal: false, openPic: false, openQRcode: false });
	};
	// ------------------------------------------------------------------------------------------------------------------
	///------------------------------------------------------------------------------------------------------------------
	uploadImg = (event) => {
		Object.keys(event.target.files).forEach((key) => {
			let { files } = this.state;
			files.push(event.target.files[key]);
			this.setState({ files });
		});
	};
	delImages = (i) => {
		let { files } = this.state;
		files.splice(i, 1);
		this.setState({ files });
	};
	async insert_form_tracking() {
		this.setState({ loadingSave: true });
		let { files, descrition } = this.state;
		let USER = JSON.parse(localStorage.getItem('user'));
		// console.log('USER', USER);
		let formdata = new FormData();

		formdata.append('form_id', this.props.location.state.id);
		formdata.append('user_info_id', USER.user_info_id);
		formdata.append('description', descrition);

		files.forEach((element) => {
			formdata.append('file', element);
		});
		try {
			await post('/complaint/insert_form_tracking', formdata, true);
			this.setState({ loadingSave: false });
			swal('สำเร็จ', 'เพิ่มรายการการติดตามเสร็จสิ้น', 'success').then(() => this.componentWillMount());
		} catch (error) {
			swal('', error, 'warning');
		}
	}
	del_tracking = async (e) => {
		Swal.fire({
			title: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			// text: 'ลบสำเร็จ',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				try {
					await post('/complaint/delete_tracking', { tracking_id: e });
					swal('สำเร็จ', 'ลบรายการการติดตามเสร็จสิ้น', 'success').then(() => this.componentWillMount());
				} catch (error) {
					swal('', error, 'warning');
				}
			}
		});
	};
	// ------------------------------------------------------------------------------------------------------------------
	sendTransfer = async (e) => {
		let { expire } = this.state;
		if (expire === null || expire === 'null' || expire === '') {
			swal('คำเตือน!', 'กรุณาเลือกระยะเวลาการดำเนินงาน', 'warning');
		} else {
			Swal.fire({
				title: 'คุณต้องการส่งต่อหน่วยงานใช่หรือไม่',
				// text: 'ลบสำเร็จ',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'ใช่',
				cancelButtonText: 'ไม่'
			}).then(async (result) => {
				if (result.value) {
					try {
						await post('/form/transfer', {
							form_id: this.state.result.form_id,
							category_id: JSON.stringify(this.state.category_id),
							description: this.state.description_transfer,
							expire: moment(new Date()).add(this.state.expire, 'day').format('YYYY-MM-DD')
						});
						await post('/form/change_status', { form_id: this.state.result.form_id, status: 6 });
						this.setState({ Modalcompaint: false });
						swal('สำเร็จ', 'ส่งต่อหน่วยงานแล้ว', 'success').then(() => this.componentWillMount());
					} catch (error) {
						swal('', error, 'warning');
					}
				}
			});
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	_onChangeText = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_onChangeBox = (e) => {
		const { category_id } = this.state;

		if (e.target.checked) {
			category_id.push(Number(e.target.value));
			this.setState({ category_id });
		} else {
			let i = category_id.indexOf(e.target.value);
			category_id.splice(i, 1);
			this.setState({ category_id });
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	btnTables = (row) => {
		Swal.fire({
			title: 'ต้องการดำเนินการต่อหรือไม่!',
			// text: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				let res = await post('/form/staff_join_update_transfer', {
					form_id: row.form_id,
					category_id: USER.central_id
				});

				if (res === 'update staff join success') {
					if (this.state.result.status == 0) {
						await post('/form/change_status', { form_id: row.form_id, status: 6 });
					}
					swal('สำเร็จ!', 'รับเรื่องสำเร็จ', 'success').then(() => this.componentWillMount());
				} else {
					swal('ผิดพลาด!', 'ไม่สามารถรับเรื่องได้', 'error');
				}
			}
		});
	};
	//---------------------------------------------------------------------------------------------------------------------
	insert_staff_chat = async () => {
		let { message } = this.state;
		let obj = {
			message: message,
			form_id: this.props.location.state.id
		};
		if (message === '') {
		} else {
			// console.log('obj', obj);
			let res = await post('/chat/insert_staff_chat', obj);
			if (res === 'insert chat success') {
				this.setState({ message: '' }, () => this.chat_staff());
			}
		}
	};
	//---------------------------------------------------------------------------------------------------------------------
	onEnter = async (e) => {
		if (e.key === 'Enter') {
			let { message } = this.state;
			let obj = {
				message: message,
				form_id: this.props.location.state.id
			};
			if (message === '') {
			} else {
				// console.log('obj', obj);
				let res = await post('/chat/insert_staff_chat', obj);
				if (res === 'insert chat success') {
					this.setState({ message: '' }, () => this.chat_staff());
				}
			}
		}
	};
	//---------------------------------------------------------------------------------------------------------------------
	search = async (e) => {
		let texts = e.target.value.toLowerCase();
		if (texts !== '') {
			let res = await get(`/central/`);
			// console.log('res', res);
			let result = res.slice(1).filter((e) => e.central_name.toLowerCase().indexOf(texts) > -1);
			this.setState({ central: result });
		} else {
			let res = await get(`/central/`);
			this.setState({ central: res.slice(1) });
		}
	};
	sendTransfer_back = async () => {
		Swal.fire({
			title: 'ต้องการดำเนินการต่อหรือไม่!',
			// text: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				await post('/form/update_tranfer_back', {
					form_id: this.props.location.state.id,
					category_id: USER.central_id
				});
				swal('สำเร็จ!', 'ส่งต่อเรื่องให้ศูนย์ดำรงธรรม', 'success').then(() => this.componentWillMount());
			}
		});
	};
	render() {
		const {
			result,
			openModal,
			focut_head,
			Modalcompaint,
			intensive,
			files,
			nameFile,
			formstaff_join,
			chat_staff,
			ModalPersons,
			waite,
			message,
			openQRcode,
			showQRcode,
			readIntensive,
			readChat,
			rightIntensive,
			status_id,
			transfer_back
		} = this.state;
		// console.log('readChat', readChat);
		const currentDate = new Date();
		const year =
			currentDate.getMonth() === 11 && currentDate.getDate() > 23
				? currentDate.getFullYear() + 1
				: currentDate.getFullYear();
		if (result) {
			return (
				<PanelRight className="pdf">
					<Bgblue />
					<PanelContent>
						{/* --------------------------------------------------------------------------------------------- */}
						<PanelHead>
							<HeadLeft>
								<HeadBtn
									onClick={() => this.setState({ focut_head: 1 })}
									bgAct={focut_head == 1 ? '#5DC0EC' : null}
								>
									เรื่องร้องเรียน
								</HeadBtn>
								<HeadBtn
									onClick={() => {
										this.setState({ focut_head: 2 }, () => this.chat_staff());
										this.updatereadChat();
									}}
									bgAct={focut_head == 2 ? '#5DC0EC' : null}
								>
									ศูนย์ดำรงธรรมขอนแก่น
									{readChat === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)}
								</HeadBtn>
								<HeadBtn
									onClick={() => this.setState({ focut_head: 4 })}
									bgAct={focut_head == 4 ? '#5DC0EC' : null}
								>
									พิมพ์เอกสาร
								</HeadBtn>
								<HeadBtn
									onClick={() => {
										this.setState({ focut_head: 5 });
										this.updateIntensive();
									}}
									bgAct={focut_head == 5 ? '#5DC0EC' : null}
								>
									ข้อสั่งการจากผู้บริหารระดับสูง
									{readIntensive === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)}
								</HeadBtn>
								{formstaff_join.some(
									(e) =>
										e.user_info_id === USER.user_info_id &&
										e.status === 1 &&
										e.central_id === USER.central_id
								) &&
									transfer_back &&
									(transfer_back.category_id === USER.central_id &&
									transfer_back.form_id === this.props.location.state.id &&
									transfer_back.id_admin === USER.user_id && (
										<HeadBtn
											onClick={() => {
												this.sendTransfer_back();
											}}
											// bgAct={focut_head == 5 ? '#5DC0EC' : null}
										>
											ส่งต่อเรื่องให้ศูนย์ดำรงธรรม
											{/* {readIntensive === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)} */}
										</HeadBtn>
									))}
							</HeadLeft>
							{result.status < 8 && (
								<HeadRight>
									<div className="d-flex">
										<div className="mr-2" style={{ color: '#fff' }}>
											{result && result.date_expire > 0 ? (
												'เหลือเวลาดำเนินการ ' + result.date_expire
											) : result.date_expire < 0 ? (
												'เลยเวลาดำเนินการ ' + result.date_expire * -1
											) : (
												'เหลือเวลาดำเนินการ '
											)}
										</div>
										{result &&
										result.date_expire === 0 && <Countdown date={`${year}-12-24T00:00:00`} />}
										<div style={{ color: '#fff' }}>
											{result && result.date_expire === 0 ? 'ชม.' : 'วัน'}
										</div>
									</div>
								</HeadRight>
							)}
						</PanelHead>
						{/* --------------------------------------------------------------------------------------------- */}
						<Head_compaint
							data={this.state.result}
							focut_head={focut_head}
							onClick={() => this.setState({ Modalcompaint: true })}
							addonClick={() => this.setState({ focut_head: 2 })}
							intensive={intensive}
							formstaff_join={formstaff_join}
							btnTables={(e) => this.btnTables(e)}
							chat_staff={chat_staff}
							modallistname={(e) => this.setState({ ModalPersons: true, waite: e })}
							insert_staff_chat={() => this.insert_staff_chat()}
							set_message={(e) => this.setState({ message: e })}
							onEnter={this.onEnter}
							message={message}
							messagesEnd={(e) => e && e.scrollIntoView({ behavior: 'smooth' })}
							openPic={(e) => this.onOpenPic(e)}
							ssQRcode={(e) => this.onOpeQRcode(e)}
							rightIntensive={rightIntensive}
						/>
						<Modal
							open={this.state.openPic}
							onClose={this.onCloseModal}
							little
							classNames={{ modal: 'Look-modal ChangeStatus ' }}
						>
							<img
								src={this.state.showImage}
								style={{ width: '100%', maxWidth: '900px', display: 'flex' }}
							/>
						</Modal>
						{/**--------------------------------------------------------------------------------------------- */}
						{focut_head == 1 && (
							<div className="dp-in-fx width100" style={{ position: 'relative' }}>
								{this.state.loadingSave ? <LoaderSaving /> : ''}
								<PanelWhiMar cn="width66 d-table">
									<DetailComp data={this.state.result} />
								</PanelWhiMar>
								{/* ------------------------------------------------------------*/}
								<div className="width33">
									<div className="admin-PanelWhi history width100">
										<TextLine title="ไฟล์เอกสาร" request />
										{nameFile.length > 0 ? (
											nameFile.map(
												(e) =>
													e.type === 'pdf' ? (
														<a
															className={'depart-file pdf'}
															href={`${path}${e.link}`}
															target="_blank"
														>
															<div>{e.name}</div>
															<div>{e.date}</div>
														</a>
													) : (
														<a
															className={'depart-file jpg'}
															onClick={() => this.onOpenPic(path + e.link)}
														>
															<div>{e.name}</div>
															<div>{e.date}</div>
														</a>
													)
											)
										) : (
											<div>---ไม่มีรายการ---</div>
										)}
									</div>
									<div className="admin-PanelWhi history width100">
										<LeftRight>
											<ContentLeft>
												<label id="tracking">รายละเอียดการดำเนินงาน</label>
											</ContentLeft>
											{formstaff_join.some(
												(e) =>
													e.user_info_id === USER.user_info_id &&
													e.status === 1 &&
													e.central_id === USER.central_id
											) && (
												<ContentRight>
													<label id="update" onClick={this.onOpenModal}>
														อัปเดตการดำเนินงาน
													</label>
												</ContentRight>
											)}
										</LeftRight>
										<div>
											<History
												data={this.state.result}
												uploadfile={(e) => this.uploadImg(e)}
												filesupload={files}
												delImages={(e) => this.delImages(e)}
												descrition={(e) => this.setState({ descrition: e })}
												insert_form_tracking={() => this.insert_form_tracking()}
												del_tracking={(e) => this.del_tracking(e)}
												openPic={(e) => this.onOpenPic(e)}
												formstaff_join={formstaff_join}
											/>
											<Modal
												open={this.state.openPic}
												onClose={this.onCloseModal}
												little
												classNames={{ modal: 'Look-modal ChangeStatus ' }}
											>
												<img
													src={this.state.showImage}
													style={{ width: '100%', maxWidth: '900px', display: 'flex' }}
												/>
											</Modal>
										</div>
									</div>
								</div>
							</div>
						)}
						{/**--------------------------------------------------------------------------------------------- */}
					</PanelContent>
					<ModalCompaint
						sendTransfer={this.sendTransfer}
						result={this.state.central}
						Modalcompaint={Modalcompaint}
						onClose={() => this.setState({ Modalcompaint: false })}
						_onChangeText={this._onChangeText}
						onChangeBox={this._onChangeBox}
						search={this.search}
					/>
					<ModalPerson
						Modalcompaint={ModalPersons}
						onClose={() => this.setState({ ModalPersons: false })}
						formstaff_join={formstaff_join}
						waite={waite}
						result={result}
						reload={() => this.componentWillMount()}
					/>
					<Modal
						open={openModal}
						onClose={this.onCloseModal}
						little
						classNames={{ modal: 'Look-modal ChangeStatus ' }}
					>
						<div className="layout">
							<h2 style={{ marginBottom: 30 }}>เปลี่ยนสถานะ</h2>
							{changeStatusRadio.map((el, i) => {
								return (
									<RadioRow
										checked={this.state[el.ref]}
										item={el}
										onChangeRadio={this.updatetypeInput}
									/>
								);
							})}
							<button className="btn-blue" onClick={this.saveChangeStatus}>
								บันทึก
							</button>
						</div>
					</Modal>
					{/* <Modal
						open={openModal}
						onClose={this.onCloseModal}
						little
						classNames={{ modal: 'Look-modal ChangeStatus ' }}
					>
						<div className="layout">
							<h2 style={{ marginBottom: 30 }}>เปลี่ยนสถานะ</h2>
							{changeStatusRadio[0].data.map((el, i) => {
								return (
									<div>
										<ContainerRadioRow key={i}>
											<input
												type="radio"
												value={el.value}
												name={'radiorow' + i}
												// id={value}
												onChange={this.updatetypeInput}
												className="-comp-form-input"
												// checked={Number(checked) == Number(value)}
												checked={el.value == status_id}
											/>
											<Label style={{ marginRight: 5 }} for={el.value}>
												{el.label}
											</Label>
											<LabelLight for={el.value}>{el.sublabel}</LabelLight>
										</ContainerRadioRow>
									</div>
								);
							})}
							<button className="btn-blue" onClick={this.saveChangeStatus}>
								บันทึก
							</button>
						</div>
					</Modal> */}
					<Modal open={openQRcode} onClose={this.onCloseModal} little>
						<div className="m-5">
							<QRCode value={String(showQRcode)} />
						</div>
					</Modal>
				</PanelRight>
			);
		} else {
			return <div />;
		}
	}
}
export default SubAdminDepartComp;
