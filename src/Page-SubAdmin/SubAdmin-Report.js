import React, { Component } from 'react';
// import Main from '../Components/Main';
// import { Link, withRouter } from 'react-router-dom';
// import TextHead from '../Components/TextHead';
// import { Tabs, Tab } from 'react-bootstrap';
// import AdminReportCreate from './Admin-Report-Create';
// import AdminForward from './Admin-Forward';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Images from '../Components/Images';
import { PanelRight, PanelContent, PanelHead, PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { HeadRight, HeadBtn, HeadLeft, Bgblue, HeadRightBtn, Button } from '../Asset/styleAdmin';
import { Noti } from '../Components/Static/adminStatic';
import moment from 'moment';
import { SelectTypeSerach, SelectSubtype, SelectFormGroup } from '../Components/Form/Input';
import { get, post } from '../api';
import { headComplaint, sub_complaint, years, month, changeStatusRadio } from '../Components/Static/static';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { th } from 'date-fns/esm/locale';
import { Search } from '../Components/Search/Search';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import Countdown from '../functions/Countdown';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class SubAdminReport extends Component {
	constructor(props) {
		super(props);

		this.state = {
			Noti: Noti,
			products: null,
			type: 0,
			sub_type: '',
			type_status: 1,
			status_name: '',
			//------------------------------
			startDate: '',
			endDate: '',
			loadingSave: false
		};
	}
	// -----------------------------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.fetchProduct();
	};
	// -----------------------------------------------------------------------------------------------------------------------
	fetchProduct = async () => {
		let { type_status } = this.state;
		this.setState({ loadingSave: true });
		const res = await get('/complaint/admin_or_central'); //status รอเจ้าหน้าที่รับเรื่อง
		let cachedObject = {};
		res.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		console.log('products', products);
		this.setState({
			products:
				type_status === 1
					? products.filter((e) => e.status === 0)
					: type_status === 2
						? products
						: type_status === 3
							? products.filter((e) => e.status >= 1 && e.status <= 7)
							: products.filter((e) => e.status === 8)
		});
		this.setState({ loadingSave: false });
	};
	//------------------------------------------------------------------------------------------------
	datetime = (cell, row) => {
		return (
			<span onClick={() => this.subadmindepartcomp(row)}>
				{moment(cell).add(543, 'year').format('DD/MM/YYYY')}
			</span>
		);
	};
	//------------------------------------------------------------------------------------------------
	subadmindepartcomp = (row) => {
		// console.log('row', row);
		this.props.history.push({
			pathname: '/subadmindepartcomp',
			state: { id: row.id, date_expire: row.expires }
		});
	};
	//-------------------------------------------------------------------------------------------------
	dataFormat = (cell, row) => {
		return <span onClick={() => this.subadmindepartcomp(row)}>{cell}</span>;
	};
	//-------------------------------------------------------------------------------------------------
	dataExpires = (cell, row) => {
		// console.log('row', row);
		const currentDate = new Date();
		const year =
			currentDate.getMonth() === 11 && currentDate.getDate() > 23
				? currentDate.getFullYear() + 1
				: currentDate.getFullYear();

		return (
			<span onClick={() => this.subadmindepartcomp(row)}>
				{row.status === 8 ? (
					'ยุติเรื่องเรียร้อย'
				) : cell === 0 ? (
					<Countdown date={`${year}-12-24T00:00:00`} />
				) : (
					cell
				)}
			</span>
		);
	};
	//--------------------------------------------------------------------------------------------------
	fullname = (cell, row) => {
		// console.log('row', row);
		return (
			<span onClick={() => this.subadmindepartcomp(row)}>
				{row.fname === null || row.lname === null || row.fname === '' || row.lname === '' ? (
					'ปกปิดข้อมูล'
				) : (
					row.fname + ' ' + row.lname
				)}
			</span>
		);
	};
	//-------------------------------------------------------------------------------------------------
	lookStatus = (cell, row) => {
		switch (cell) {
			case 0:
				return <span onClick={() => this.subadmindepartcomp(row)}> รอเจ้าหน้าที่รับเรื่อง </span>;
			case 1:
				return <span onClick={() => this.subadmindepartcomp(row)}> กำลังตรวจสอบคำร้อง </span>;
			case 2:
				return (
					<span onClick={() => this.subadmindepartcomp(row)}>
						เสนอผู้อำนวยการกลุ่มงานศุนย์ดำรงธรรมจังหวัดขอนแก่นพิจรณา
					</span>
				);
			case 3:
				return (
					<span onClick={() => this.subadmindepartcomp(row)}> เสนอหัวหน้าสำนักงานจังหวัดขอนแก่นพิจารณา </span>
				);
			case 4:
				return (
					<span onClick={() => this.subadmindepartcomp(row)}>
						เสนอรองผู้ว่าราชการจังหวัดขอนแก่น/ผู้ว่าราชการจังหวัดขอนแก่นพิจารณา
					</span>
				);
			case 5:
				return <span onClick={() => this.subadmindepartcomp(row)}> สั่งการให้หน่วยงานตรวจสอบข้อเท็จจริง </span>;
			case 6:
				return (
					<span onClick={() => this.subadmindepartcomp(row)}>
						อยู่ระหว่างการดำเนินการของหน่วยงานหรือจังหวัด
					</span>
				);
			case 7:
				return (
					<span style={{ color: '#f44336' }} onClick={() => this.subadmindepartcomp(row)}>
						รายระเอียดคำร้องไม่ชัดเจน
					</span>
				);
			case 8:
				return (
					<span style={{ color: '#8bc34a' }} onClick={() => this.subadmindepartcomp(row)}>
						ยุติเรื่อง
					</span>
				);
			default:
				return <span />;
		}
	};
	//--------------------------------------------------------------------------------------------------
	goAddcompaint = () => {
		return this.props.history.push('/addcompaint');
	};
	//-------------------------------------------------------------------------------------------------
	searchSubject = async (e) => {
		let { type_status } = this.state;
		const ress = await get('/complaint/admin_or_central');
		let cachedObject = {};
		ress.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		// console.log('res', res);
		let res =
			type_status === 1
				? products.filter((e) => e.admin_status === 0)
				: type_status === 2
					? products
					: type_status === 3
						? products.filter((e) => e.status >= 1 && e.status <= 7)
						: products.filter((e) => e.status === 8);
		let texts = e.toLowerCase();
		let res_products = res
			.map((e) => ({ ...e, name: e.fname + ' ' + e.lname, adminname: e.admin_Fname + ' ' + e.admin_Lname }))
			.filter(
				(e, i) =>
					String(e.subject).toLowerCase().indexOf(texts) > -1 ||
					String(e.form_id).toLowerCase().indexOf(texts) > -1 ||
					String(e.id_card).toLowerCase().indexOf(texts) > -1 ||
					String(e.name).toLowerCase().indexOf(texts) > -1 ||
					String(e.adminname).toLowerCase().indexOf(texts) > -1 ||
					String(e.moi).toLowerCase().indexOf(texts) > -1 ||
					String(e.central_name).toLowerCase().indexOf(texts) > -1 ||
					String(e.house_number).toLowerCase().indexOf(texts) > -1 ||
					String(e.moo).toLowerCase().indexOf(texts) > -1 ||
					String(e.alleyway).toLowerCase().indexOf(texts) > -1 ||
					String(e.road).toLowerCase().indexOf(texts) > -1 ||
					String(e.sub_district).toLowerCase().indexOf(texts) > -1 ||
					String(e.district).toLowerCase().indexOf(texts) > -1 ||
					String(e.province).toLowerCase().indexOf(texts) > -1 ||
					String(e.zipcode).toLowerCase().indexOf(texts) > -1
			);
		this.setState({ products: res_products });
	};
	Clicksearch = async () => {
		let { sub_type, type, status_name, type_status, startDate, endDate } = this.state;
		let sub_typeName = type === 0 || type === '' ? '' : sub_type;
		let type_name =
			Number(type) === 1
				? 'ได้รับความเดือดร้อน'
				: Number(type) === 2
					? 'ขอความช่วยเหลือ'
					: Number(type) === 3
						? 'ขอความเป็นธรรม'
						: Number(type) === 4
							? 'ปัญหาที่อยู่อาศัย/ที่ดิน'
							: Number(type) === 5
								? 'แจ้งเบาะแส'
								: Number(type) === 6
									? 'ทรัพยากรธรรมชาติและสิ่งแวดล้อม'
									: Number(type) === 7
										? 'หนี้สิน'
										: Number(type) === 8.1 ? 'ร้องเรียนกล่าวโทษเจ้าหน้าที่/หน่วยงานของรัฐ' : '';
		const ress = await get('/complaint/admin_or_central');
		let cachedObject = {};
		ress.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		let res =
			type_status === 1
				? products.filter((e) => e.admin_status === 0)
				: type_status === 2
					? products
					: type_status === 3
						? products.filter((e) => e.status >= 1 && e.status <= 7)
						: products.filter((e) => e.status === 8);
		let res_products = res
			.filter(
				(e) =>
					endDate !== ''
						? endDate !== 'null'
							? endDate !== null
								? moment(endDate).format('DD MM YYYY') <= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter(
				(e) =>
					startDate !== ''
						? startDate !== 'null'
							? startDate !== null
								? moment(startDate).format('DD MM YYYY') >= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter(
				(e) =>
					startDate !== '' && endDate !== ''
						? startDate !== 'null' && endDate !== 'null'
							? startDate !== null && endDate !== null
								? moment(startDate).format('DD MM YYYY') >= moment(e.timestamp).format('DD MM YYYY') &&
									moment(endDate).format('DD MM YYYY') <= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter((e) => (type_name !== '' ? e.type === type_name : e))
			.filter((e) => (sub_typeName !== '' ? e.sub_type === sub_typeName : e))
			.filter((e) => (status_name !== '' ? Number(e.status) === Number(status_name) : e));
		this.setState({ products: res_products });
		this.setState({ loadingSearch: false });
	};

	render() {
		const { Noti, products, type_status, startDate, endDate } = this.state;
		const optionsTable = {
			paginationSize: 3,
			sizePerPageList: [ 10 ],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			noDataText: 'ไม่พบข้อมูล'
		};

		return (
			<PanelRight>
				<Bgblue />
				<PanelContent>
					{/* ------------------------- */}
					<PanelHead>
						<HeadLeft>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 1, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 1 ? '#5DC0EC' : null}
							>
								เรื่องร้องเรียนเข้าใหม่
							</HeadBtn>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 3, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 3 ? '#5DC0EC' : null}
							>
								เรื่องที่อยู่ระหว่างดำเนินการ
							</HeadBtn>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 4, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 4 ? '#5DC0EC' : null}
							>
								เรื่องที่ยุติ
							</HeadBtn>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 2, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 2 ? '#5DC0EC' : null}
							>
								เรื่องร้องเรียนทั้งหมด
							</HeadBtn>
						</HeadLeft>
						<HeadRight>
							<HeadRightBtn href="/subadminaddcompaint">+ บันทึกเรื่องร้องเรียน</HeadRightBtn>
						</HeadRight>
					</PanelHead>
					{/* ------------------------- */}
					<PanelWhi>
						<div className="row mb-3">
							<div className={`${type_status === 2 ? 'col-10' : 'col-10'}`}>
								{/* ----------search ----------- */}
								<Search
									classNameInp="mr-auto w-100"
									raduis="5px"
									title="ค้นหา"
									placeholder="ค้นหาชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ"
									onChange={(e) => this.searchSubject(e.target.value)}
									// hint="ชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ"
								/>
								{/* /--------------------------------- */}
							</div>
							<div className={`${type_status === 2 || type_status === 3 ? 'col-5' : 'col-10'}`}>
								<div className="form-group select-arrow">
									<label>ค้นหาตามช่วงเวลา</label>
									<div className="d-flex justify-content-between">
										<DatePicker
											placeholderText="เลือกวันเริ่มต้น"
											locale={th}
											dateFormat={'dd MMMM ' + moment(startDate).add(543, 'year').format('YYYY')}
											renderCustomHeader={({ date, changeYear, changeMonth }) => (
												<div
													style={{
														display: 'flex',
														justifyContent: 'center'
													}}
												>
													<select
														className="form-control mx-1"
														value={
															startDate === '' ||
															startDate === 'null' ||
															startDate === null ? (
																moment(new Date()).month()
															) : (
																moment(startDate).month()
															)
														}
														onClick={() => this.setState({ startDate: date })}
														onChange={({ target: { value } }) => {
															changeMonth(value);
														}}
													>
														{month.map((e) => (
															<option value={Number(e.value) - 1}>{e.label}</option>
														))}
													</select>
													<select
														className="form-control mx-1"
														value={
															startDate === '' ||
															startDate === 'null' ||
															startDate === null ? (
																moment(new Date()).year()
															) : (
																moment(startDate).year()
															)
														}
														onClick={() => this.setState({ startDate: date })}
														onChange={({ target: { value } }) => {
															changeYear(value);
														}}
													>
														{years().map((e) => (
															<option value={e.id}>{Number(e.name + 543)}</option>
														))}
													</select>
												</div>
											)}
											selected={startDate}
											onChange={(date) => this.setState({ startDate: date })}
										/>
										<DatePicker
											placeholderText="เลือกวันสิ้นสุด"
											locale={th}
											dateFormat={'dd MMMM ' + moment(endDate).add(543, 'year').format('YYYY')}
											renderCustomHeader={({ date, changeYear, changeMonth }) => (
												<div
													style={{
														display: 'flex',
														justifyContent: 'center'
													}}
												>
													<select
														className="form-control mx-1"
														value={
															endDate === '' || endDate === 'null' || endDate === null ? (
																moment(new Date()).month()
															) : (
																moment(endDate).month()
															)
														}
														onClick={() => this.setState({ endDate: date })}
														onChange={({ target: { value } }) => {
															changeMonth(value);
														}}
													>
														{month.map((e) => (
															<option value={Number(e.value) - 1}>{e.label}</option>
														))}
													</select>
													<select
														className="form-control mx-1"
														value={
															endDate === '' || endDate === 'null' || endDate === null ? (
																moment(new Date()).year()
															) : (
																moment(endDate).year()
															)
														}
														onClick={() => this.setState({ endDate: date })}
														onChange={({ target: { value } }) => {
															changeYear(value);
														}}
													>
														{years().map((e) => (
															<option value={e.id}>{Number(e.name + 543)}</option>
														))}
													</select>
												</div>
											)}
											selected={endDate}
											onChange={(date) => this.setState({ endDate: date })}
										/>
									</div>
								</div>
							</div>
							{(type_status === 2 || type_status === 3) && (
								<div className="col-5">
									<div className="form-group select-arrow">
										<label>สถานะเรื่อง</label>
										<select
											className="form-control"
											onChange={(e) => this.setState({ status_name: e.target.value })}
										>
											<option value="">ทั้งหมด</option>
											{changeStatusRadio[0].data.map((e) => (
												<option value={e.value}>{e.label}</option>
											))}
										</select>
									</div>
								</div>
							)}
							<div className="col-md-5 col-lg-5">
								<SelectTypeSerach
									value={this.state.type}
									item={headComplaint[1]}
									onChangeSelect={(e) => this.setState({ type: e.target.value })}
								/>
							</div>
							<div className="col-md-5 col-lg-5">
								<SelectSubtype
									disabled={this.state.type === 0 || this.state.type === ''}
									value={this.state.sub_type}
									item={sub_complaint[0]}
									head={this.state.type}
									onChangeSelect={(e) => this.setState({ sub_type: e.target.value })}
								/>
							</div>
							<div className="col d-flex align-items-end">
								<Button my=".75rem" onClick={this.Clicksearch}>
									ค้นหา
								</Button>
							</div>
						</div>
						{/* ----------------------------------------------------------------------------------------------- */}
						<div id="my-tableHeader-class-text-left">
							{products ? (
								<BootstrapTable
									data={
										products &&
										products.sort((a, b) => moment(b.timestamp) - moment(a.timestamp)).map((e) => ({
											...e,
											expires: e.date_expire,
											sub: !e.subject ? '-' : e.subject
										}))
									}
									pagination={true}
									options={optionsTable}
									containerClass="my-container-class tb-setSearch"
									tableContainerClass="my-tableContainer-class"
									tableHeaderClass="my-tableHeader-class colorBlue"
									tableBodyClass="my-tableBody-pointer-class"
								>
									<TableHeaderColumn
										dataField="form_id"
										isKey
										dataFormat={this.dataFormat}
										width="10%"
									>
										รหัสเรื่อง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="timestamp" dataFormat={this.datetime} width="10%">
										วันที่แจ้ง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="sub" dataFormat={this.dataFormat} width="20%">
										เรื่องร้องเรียน
									</TableHeaderColumn>
									<TableHeaderColumn dataField="type" dataFormat={this.dataFormat} width="15%">
										ประเภทเรื่องหลัก
									</TableHeaderColumn>
									{/* <TableHeaderColumn dataField="sub_type" dataFormat={this.dataFormat}>
									ประเภทเรื่องย่อย
							</TableHeaderColumn> */}
									<TableHeaderColumn dataField="status" dataFormat={this.lookStatus} width="13%">
										สถานะเรื่อง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="name" dataFormat={this.fullname} width="14%">
										ชื่อผู้ร้องเรียน
									</TableHeaderColumn>
									{type_status && type_status === 1 ? (
										<TableHeaderColumn dataField="id" dataFormat={this.btnTable} width="9%" />
									) : (
										<TableHeaderColumn dataField="expires" dataFormat={this.dataExpires} width="9%">
											เหลือเวลา(วัน)
										</TableHeaderColumn>
									)}
								</BootstrapTable>
							) : this.state.loadingSave ? (
								<div style={{ position: 'relative', height: '300px' }}>
									<LoaderSaving />
								</div>
							) : (
								''
							)}
						</div>
					</PanelWhi>
				</PanelContent>
			</PanelRight>
		);
	}
	//--------------------------------------------------------------------------------------------------
	btnTable = (cell, row) => {
		// console.log('row', row);

		return row.admin_status === 1 ? null : (
			<Button
				disabled
				// onClick={() => this.btnTables(row)}
				onClick={() => alert('asdasd')}
				py={'.667rem'}
				w={'90px'}
				mw={'70px'}
			>
				รับเรื่อง
			</Button>
		);
	};
	//--------------------------------------------------------------------------------------------------
	btnTables = (row) => {
		// console.log('row', row);
		Swal.fire({
			title: 'ต้องการดำเนินการต่อหรือไม่!',
			// text: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				let res = await post('/form/staff_join_update_transfer', {
					form_id: row.id,
					category_id: row.central_id
				});
				if (res === 'update staff join success') {
					let ress = await post('/form/change_status', { form_id: row.id, status: 6 });
					swal('สำเร็จ!', ress, 'success').then(() =>
						this.props.history.push({
							pathname: '/subadmindepartcomp',
							state: { id: row.id, date_expire: row.expires }
						})
					);
				} else {
					swal('ผิดพลาด!', 'ไม่สามารถรับเรื่องได้', 'error');
				}
			}
		});
	};
}
