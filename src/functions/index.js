export const fullname = (name = '', lastname = '') => {
    return `${name} ${lastname}`
}

export const address = (object = {}) => {
    // alert(JSON.stringify(object))
    let short = {
        province: 'จ.', 
        district: 'อ.', 
        tumbon: 'ต.', 
        moo: 'หมู่ ', 
        soi: 'ซอย ', 
        road: 'ถนน ',
        defendant_moo: 'หมู่ ',
        defendant_soi: 'ซอย ',
        defendant_road: 'ถนน ',
        alleyway: 'ซอย ',
        moo_person: 'หมู่ ',
        alleyway_person: 'ซอย ',
        road_person: 'ถนน ',
        sub_district: 'ต.',
        sub_district_company: 'ต.',
        district_company: 'อ.',
        province_company: 'จ.',
        alleyway_person:'ตรอกซอย ',
        road_person:'ถนน ',
        sub_district_person:'ต.',
        district_person:'อ.',
        province_person:'จ.',
        defendant_house_number:'บ้านเลขที่ '
    }
    let address = ""
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            const element = object[key];
            const takeShort = element ? (short[key] || '') : '';
            address += takeShort + element + ' '
            // switch(key){
            //     case "":
            // }
        }
    }

    return address
}