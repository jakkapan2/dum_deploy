import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button } from '../../Asset/styleAdmin';
import USER from '../../mobx/user';
const Step7 = ({ goTO }) => {
	return (
		<div className="d-grid justify-content-center text-center pt-5">
			<i className="far fa-check-circle mb-4" style={{ fontSize: '10rem', color:'#29A4DB' }}></i>
			<h4 style={{color:'#29A4DB'}}>บันทึกเรื่องร้องเรียนสำเร็จ</h4>
			<span>เพิ่มเรื่องร้องเรียนลงรายการเรื่องร้องเรียนเรียบร้อยแล้ว</span>
			<div className="d-flex mt-5">
				{USER.role.toString() === 'ADMIN' ? (
					<a href="/adminreport">
						<Button w={'200px'}>ไปยังรายการร้องเรียน</Button>
					</a>
				) : (
						<a href="/subadminreport">
							<Button w={'200px'}>ไปยังรายการร้องเรียน</Button>
						</a>
					)}
				<Button onClick={goTO} w={'200px'}>รายละเอียดเรื่องร้องเรียนนี้</Button>
			</div>
		</div>
	);
};
export default withRouter(Step7);
