import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { headComplaint, selectComplaint, changeStatusRadio } from '../Static/static';
// import { InputForm } from '../Form/Input';
// import { RadioCol } from '../Form/InputAdmin';
import { TextLine } from '../TextHead';
import { Sumtext, SumDetail } from '../Div/SetDiv';
import { FormCompAgree, FormCompUpload, FormCompUpList } from '../Form/Complaint';
import _ from 'lodash';
import { address } from '../../functions/index';
import Images from '../Images';
import MapComponent from '../Maps/MapComponent';
import { Row, Col } from 'reactstrap';
import { RadioRow } from '../Form/Input';

function bytesToSize(bytes) {
	var sizes = [ 'Bytes', 'KB', 'MB', 'GB', 'TB' ];
	if (bytes === 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

const Step6 = ({
	updatetypeInput,
	type,
	ckDetail,
	sayyes,
	agree,
	value,
	updatetypeStatus,
	typeStatus,
	removeFile,
	removePdfFile,
	set_mapName
}) => {
	// console.log('check detail', ckDetail);
	return (
		<div>
			<p className="headStep">ตรวจสอบ</p>
			<div>
				<TextLine title="เรื่องร้องทุกข์" />
				<Sumtext title="หัวข้อร้องทุกข์" detail={!ckDetail.subject ? '' : ckDetail.subject} />
				<Sumtext
					title="ประเภทเรื่อง"
					detail={headComplaint[1].data.filter((e) => Number(e.value) === Number(ckDetail.type))[0].label}
				/>
				<Sumtext title="ประเภทเรื่องย่อย" detail={ckDetail.subtype} />
				<Sumtext title="ช่องทางการร้องเรียน" detail={ckDetail.complaint_channel} />
				<Sumtext title="รหัส MOI" detail={!ckDetail.moi ? '' : ckDetail.moi} />
				{/* -------------------------------------------------------------- */}
				<TextLine title="ผู้ร้องทุกข์" />
				{ckDetail.activecomplaint ? (
					<div>
						<Sumtext title="ชื่อ-นามสกุล ผู้ร้อง" detail={ckDetail.fname + ' ' + ckDetail.lname} />
						<Sumtext title="หมายเลขประจำตัวประชาชน" detail={ckDetail.id_card} />
						{/* <Sumtext title="ID LINE ผู้ร้องทุกข์" detail={ckDetail.id_line} /> */}
						<Sumtext title="E-mail ผู้ร้องทุกข์" detail={!ckDetail.email ? '' : ckDetail.email} />
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(ckDetail, [ 'house_number', 'moo', 'alleyway', 'road' ]),
								..._.pick(ckDetail.complaint, [ 'sub_district', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				) : (
					<p className="closeInfo py-5">ปกปิดชื่อ</p>
				)}
				{/* -------------------------------------------------------------- */}
				<TextLine title="ผู้ถูกร้องทุกข์" />
				{/* <Sumtext title='ผู้ถูกร้อง' detail={selectComplaint[0].data.find(el => el.value === ckDetail.select_complaintTo).label} /> */}
				<Sumtext
					title="ผู้ถูกร้อง"
					detail={
						selectComplaint[0].data.find((el) => Number(el.value) === Number(ckDetail.select_complaintTo))
							.label
					}
				/>
				{ckDetail.select_complaintTo === 12 || ckDetail.select_complaintTo === '12' ? (
					<div>
						<Sumtext
							title="ประเภท"
							detail={
								ckDetail.companyTypes === 4 || ckDetail.companyTypes === '4' ? (
									ckDetail.others
								) : (
									ckDetail.companyType[Number(ckDetail.companyTypes) - 1].type_name
								)
							}
						/>
						<Sumtext
							title="ชื่อนิติบุคคลหรือบริษัท"
							detail={!ckDetail.corporation_name ? '' : ckDetail.corporation_name}
						/>
						<Sumtext
							title="หมายเลขโทรศัพท์(ถ้ามี)"
							detail={ckDetail.phone_company ? ckDetail.phone_company : ''}
						/>
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(ckDetail, [
									'defendant_house_number',
									'defendant_moo',
									'defendant_alley',
									'defendant_road'
								]),
								..._.pick(ckDetail, [
									'house_number_person',
									'moo_person',
									'alleyway_person',
									'road_person'
								]),
								..._.pick(ckDetail.right, [ 'tumbon', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				) : ckDetail.select_complaintTo === 11 || ckDetail.select_complaintTo === '11' ? (
					<div>
						<Sumtext
							title="ชื่อผู้ถูกร้องทุกข์"
							detail={ckDetail.fname_person + ' ' + ckDetail.lname_person}
						/>
						<Sumtext
							title="หมายเลขโทรศัพท์(ถ้ามี)"
							detail={ckDetail.phone_person ? ckDetail.phone_person : ''}
						/>
						<Sumtext
							title="ที่อยู่"
							detail={address({
								..._.pick(ckDetail, [
									'defendant_house_number',
									'defendant_moo',
									'defendant_alley',
									'defendant_road'
								]),
								..._.pick(ckDetail, [
									'house_number_person',
									'moo_person',
									'alleyway_person',
									'road_person'
								]),
								..._.pick(ckDetail.center, [ 'tumbon', 'district', 'province', 'zipcode' ])
							})}
						/>
					</div>
				) : (
					<Sumtext title="ชื่อหน่วยงาน" detail={!ckDetail.department_name ? '' : ckDetail.department_name} />
				)}

				{/* -------------------------------------------------------------- */}
				<TextLine title="รายละเอียดคำร้อง" />
				<Row>
					<Col sm={6}>
						<MapComponent
							coords={ckDetail.location.coords}
							markerCoords={ckDetail.location.markerCoords}
							onMarkerChange={(e) => set_mapName(e)}
						/>
					</Col>
					<Col sm={6}>
						<div className="d-grid text-left" style={{ gridTemplateColumns: '100px auto' }}>
							<label>สถานที่เกิดเหตุ</label>
							<label className="text-left pl-3">
								{!ckDetail.location.coords.location_name ? '' : ckDetail.location.coords.location_name}
							</label>
						</div>
						{/* <Sumtext title="สถานที่เกิดเหตุ" detail={ckDetail.location.coords.location_name} wl={'100px'} /> */}
						<SumDetail
							title="รายละเอียดคำร้อง"
							detail={!ckDetail.detail ? '' : ckDetail.detail}
							wl={'100px'}
						/>
						<SumDetail
							title="วัตถุประสงค์"
							detail={!ckDetail.objective ? '' : ckDetail.objective}
							wl={'100px'}
						/>
					</Col>
				</Row>
				{/* -------------------------------------------------------------- */}
				<TextLine title="ไฟล์เอกสาร" />
				<FormCompUpload>
					{ckDetail.fileImgList.length === 0 &&
					ckDetail.filePdfList.length === 0 && (
						<div className="-comp-upload text-center" style={{ minHeigth: '40px' }}>
							ไม่มีไฟล์เอกสาร
						</div>
					)}
					{ckDetail.fileImgList.map((fileImg, index) => (
						<FormCompUpList key={fileImg.name}>
							<div className="-namefile">
								<img src={Images.Asset14} alt="" />
								<label>{fileImg.name + ' ' + bytesToSize(fileImg.size)}</label>
							</div>
						</FormCompUpList>
					))}
					{ckDetail.filePdfList.map((filePdf, index) => (
						<FormCompUpList key={filePdf.name}>
							<div className="-namefile">
								<img src={Images.Asset15} alt="" />
								<label>{filePdf.name + ' ' + bytesToSize(filePdf.size)}</label>
							</div>
						</FormCompUpList>
					))}
				</FormCompUpload>
				<hr />
				{/** --------------------------------------------------------------*/}
				<FormCompAgree style={{ width: '100%' }}>
					<input
						type="checkbox"
						id="agree"
						name="feature"
						checked={sayyes === true}
						value={sayyes}
						onChange={agree}
					/>
					<label for="agree">
						ข้าพเจ้าขอรับรองว่าเป็นความจริงทุกประการ หากปรากฏว่าไม่เป็นความจริง ข้าพเจ้ายอกรับผิด
						และให้ดำเนินการได้ตามกฎหมาย
					</label>
				</FormCompAgree>
				{/* -------------------------------------------------------------- */}
				<TextLine title="สถานะเรื่อง" />
				<div className="radioStatus">
					{changeStatusRadio.map((el, i) => {
						return <RadioRow checked={typeStatus} item={el} onChangeRadio={updatetypeStatus} />;
					})}
				</div>
			</div>
		</div>
	);
};
export default withRouter(Step6);
