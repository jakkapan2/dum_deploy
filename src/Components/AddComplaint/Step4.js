import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
// import { headComplaint } from '../Static/static';
// import { InputForm } from '../Form/Input';
// import { RadioCol } from '../Form/InputAdmin';
import { Container, Row, Col } from 'reactstrap';
import MapComponent from '../Maps/MapComponent';
import LabelInput from '../LabelInput';
import { Label, InputTextarea, InputText } from '../../Asset/styled';

const Step4 = ({ updatetypeInput, locations, detail, objective, set_mapName, onChangelocation_name }) => {
	// console.log('location', locations);
	return (
		<div>
			<p className="headStep">รายละเอียดคำร้อง</p>
			<Row>
				<Col sm={6}>
					<MapComponent
						coords={locations.coords}
						markerCoords={locations.markerCoords}
						onMarkerChange={(e) => set_mapName(e)}
					/>
				</Col>
				<Col sm={6}>
					<LabelInput>
						<Label>สถานที่เกิดเหตุ</Label>
						<InputText
							// disabled={
							// 	locations.coords.latitude === 16.439625 && locations.coords.longitude === 102.828728
							// }
							name="location_name"
							value={locations.coords.location_name}
							onChange={onChangelocation_name}
							placeholder=""
							className="form-control form-group styleScroll"
						/>
					</LabelInput>
					<LabelInput>
						<Label>รายละเอียด</Label>
						<InputTextarea
							name="detail"
							value={detail}
							onChange={updatetypeInput}
							placeholder="กรุณากรอก รายละเอียด"
							className="form-control form-group styleScroll"
						/>
					</LabelInput>
					<LabelInput>
						<Label>วัตถุประสงค์</Label>
						<InputTextarea
							name="objective"
							value={objective}
							onChange={updatetypeInput}
							placeholder="กรุณากรอก วัตถุประสงค์"
							className="form-control form-group styleScroll"
						/>
					</LabelInput>
				</Col>
			</Row>
		</div>
	);
};
export default withRouter(Step4);
