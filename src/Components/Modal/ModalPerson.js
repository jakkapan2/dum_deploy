import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import { Search } from '../../Components/Search/Search';
import { Button } from '../../Asset/styleAdmin';
import { InputFormGroupArea, SelectFormGroup } from '../../Components/Form/Input';
import { responseTime } from '../Static/static';
import { ModalHeader, ModalBody, ModalFooter } from '../../Asset/styleAdmin';
import { LeftRight, ContentLeft, ContentRight } from '../../Asset/styled';
import Images from '../Images';
import { get, post, path } from '../../api';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import { Img } from '../Image/Img';
import USER from '../../mobx/user';

export default class ModalPerson extends Component {
	deleteTBL = async (e) => {
		Swal.fire({
			title: 'ยืนยัน?',
			text: 'คุณต้องการลบใช้หรือไม่',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				let { result } = this.props;
				let res = await post('/complaint/delete_transfer_row', { form_id: result.form_id, category_id: e });
				if (res === 'remove success') {
					swal('สำเร็จ!', res, 'success').then(() => {
						this.props.onClose();
						this.props.reload();
					});
				} else {
					swal('ผิดพลาด!', 'remove fail', 'error');
				}
			}
		});
	};
	render() {
		let { Modalcompaint, onClose, formstaff_join, waite } = this.props;
		// console.log('waite>>>', waite);
		return (
			<Modal
				open={Modalcompaint}
				onClose={() => onClose()}
				little
				classNames={{ modal: 'Look-modal ChangeStatus ' }}
			>
				<ModalHeader p={'1rem'}>{waite == 1 ? 'ผู้ร่วมห้องสนทนา' : 'รอการตอบรับ'}</ModalHeader>
				<hr />
				<ModalBody>
					{formstaff_join.length > 0 &&
						waite == 1 &&
						formstaff_join.map((e) => (
							<div className="chat-modal styleScroll">
								<div>
									<div>
										<Img
											w={'40px'}
											h={'40px'}
											src={
												e.profile_pic == 'null' || e.profile_pic == null ? (
													Images.Asset4
												) : (
													path + '/static/' + e.profile_pic
												)
											}
										/>
									</div>
									<div>
										<text>
											{e.status == 1 && e.name} {e.status == 0 && '(รอการตอบรับ)'}
										</text>
										<text>{e.central_name}</text>
									</div>
								</div>
								{USER.role === 'ADMIN' && (
									<div>
										<span className="chat-modal-del" onClick={() => this.deleteTBL(e.central_id)}>
											ลบ
										</span>
									</div>
								)}
							</div>
						))}
					{formstaff_join.length > 0 &&
						waite == 0 &&
						formstaff_join.filter((e) => e.status == 0).map((e, i) => (
							<div
								className="d-flex justify-content-between styleScroll"
								style={{ width: '35rem', maxHeight: '300px' }}
							>
								<div className="d-flex align-items-center">
									<div>{i + 1}.</div>
									<div className="d-flex flex-column">
										<text>{e.central_name}</text>
									</div>
								</div>
								<div>
									<span className="chat-modal-del" onClick={() => this.deleteTBL(e.central_id)}>
										ลบ
									</span>
								</div>
							</div>
						))}
				</ModalBody>
			</Modal>
		);
	}
}
