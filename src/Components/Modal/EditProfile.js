import React, { Component } from 'react'
import { ModalContent, ModalHeader, ModalBody, ModalFooter, Button } from '../../Asset/styleAdmin';
import { InputFormGroup } from '../Form/Input'
import { Img } from '../Image/Img'
import Images from '../Images';
import { get, post, path } from '../../api';
import Modal from 'react-responsive-modal'
import swal from 'sweetalert'

export default class EditProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openModal: false,
            user_info_id: null,
            username: null,
            email: null,
            fname: '',
            lname: null,
            phone: null,
            password: '',
            oldpassword: '',
            newpassword: ''
        }
    }
    componentDidMount() {
        this.fetchUser()
    }
    // ------------------------------------------------------------------------------------------------------------------
    onOpenModal = () => {
        this.setState({ openModal: true });
    };
    onCloseModal = () => {
        this.setState({ onCloseModal: false });
    };
    // ------------------------------------------------------------------------------------------------------------------
    async fetchUser() {
        let userlocal = JSON.parse(localStorage.getItem('user'))
        try {
            const user = await post('/central/central_me/', { user_id: userlocal.user_id })
            this.setState({
                user_info_id: user.user_info_id,
                username: user.username,
                email: user.email,
                fname: user.fname,
                lname: user.lname,
                phone: user.phone,
            })
        } catch (error) {
            console.log('error', error)
        }
        // this.setState({})
    }
    // -----------------------------------------------------------------------------------------------------------------------
    onUpdate = async () => {
        const { user_info_id, fname, lname, phone, username, email } = this.state
        const item = await post('/central/update_user', { user_info_id, fname, lname, phone, username, email })
        if (item) {
            swal({
                title: "เรียบร้อย",
                text: 'บันทึการแก้ไขแล้ว',
                icon: "success",
            })
                .then(() => {
                    window.location.reload()
                    // this.componentDidMount()
                });
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------
    _onChangeText = e => {
        this.setState({ [e.target.name]: e.target.value });
    }
    // -----------------------------------------------------------------------------------------------------------------------
    render() {
        let { onOpenModal, onCloseModal } = this.props
        return (
            <Modal
                open={onOpenModal}
                onClose={onCloseModal}
                little
            >
                <ModalContent>
                    <ModalHeader>แก้ไขเจ้าหน้าที่หน่วยงาน</ModalHeader>
                    <ModalBody>
                        <div className="d-flex justify-content-between">
                            <Img src={Images.logo}
                                w="140px"
                                h="140px"
                                border="1px solid #ddd"
                                px="10px"
                                py="10px"
                            />
                            <div style={{ width: '320px' }}>
                                <InputFormGroup title="ชื่อผู้ใช้" name="username" value={this.state.username} onChange={this._onChangeText} disabled />
                                <InputFormGroup title="E-mail" name="email" value={this.state.email} onChange={this._onChangeText} disabled />
                                <InputFormGroup title="ชื่อจริง" name="fname" value={this.state.fname} onChange={this._onChangeText} />
                                <InputFormGroup title="นามสกุล" name="lname" value={this.state.lname} onChange={this._onChangeText} />
                                <InputFormGroup title="หมายเลขโทรศัพท์" name="phone" value={this.state.phone} onChange={this._onChangeText} />
                                <span>เปลี่ยนรหัสผ่าน</span>
                                <InputFormGroup title="รหัสผ่านเดิม" name="phone" value={this.state.phone} onChange={this._onChangeText} />
                                <InputFormGroup title="รหัสผ่านใหม่" name="phone" value={this.state.phone} onChange={this._onChangeText} />
                                <InputFormGroup title="ยืนยันรหัสผ่าน" name="phone" value={this.state.phone} onChange={this._onChangeText} />
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.onUpdate}>บันทึก</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal >
        )
    }
}
