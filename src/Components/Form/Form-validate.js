import React, { Component } from 'react'
import swal from 'sweetalert'

export default class FormValidate extends Component {
    swalCheck = (message) => {
        swal('คำเตือน!', message, 'warning');
    };

    //     activecomplaint: false
    // activeleft: true
    // activeright: false
    // allDistrict: []
    // allProvince: []
    // allTombon: []
    // allZipcode: {}
    // alley: ""
    // alleyway: "-"
    // alleyway_person: ""
    // center: {province: "", district: "", tumbon: "", zipcode: ""}
    // central_name: null
    // companyType: [{type_id: 1, type_name: "บริษัท"}, {type_id: 2, type_name: "บริษัท (มหาชน)"},…]
    // complainant: "DEPARTMENT"
    // complaint: {province: "", district: "เมืองขอนแก่น", tumbon: "พระลับ", zipcode: "40000"}
    // corporation_name: ""
    // corporation_phone: ""
    // departmentComplaint: [{ref: "department_name", title: "หน่วยงานที่ถูกร้อง", type: "input",…}]
    // department_name: ""
    // department_type: ""
    // detail: ""
    // disclose: 0
    // district: "เมืองขอนแก่น"
    // district_company: ""
    // district_person: ""
    // email: ""
    // fileImgList: []
    // filePdfList: []
    // fname: "ช่อผกา"
    // fname_person: ""
    // house_number: "90"
    // house_number_person: ""
    // id_card: "1409901669302"
    // id_line: ""
    // lat: 0
    // lname: "ชินคำหาญ"
    // lname_person: ""
    // lng: 0
    // loadingSave: false
    // location_name: ""
    // moo: "9"
    // moo_person: ""
    // name_company: ""
    // objective: ""
    // openModal: true
    // path: "/static/user_profile.png"
    // phone: "0942617603"
    // prefix: "นางสาว"
    // province: "ขอนแก่น"
    // province_company: ""
    // province_person: ""
    // right: {province: "", district: "", tumbon: "", zipcode: ""}
    // road: "-"
    // road_person: ""
    // role: "USER"
    // sayyes: true
    // select_complaintTo: 10
    // select_depart: ""
    // select_headcomplaintTo: 1
    // sub_district: "พระลับ"
    // sub_district_company: ""
    // sub_district_person: ""
    // sub_type: "สาธารณูปโภค/โครงสร้างพื้นฐาน"
    // subject: "efc"
    // type: "ได้รับความเดือดร้อน"
    // type_company: "บริษัท"
    // type_id: 0
    // zipcode: "40000"
    // zipcode_company: ""
    // zipcode_person: ""
    onCheck = (e) => {
        console.log('e', e);
        // this.setState({ fff: false })
        if (e.sub_type === 'เลือก' || e.sub_type === '') {
            console.log('success');
            this.swalCheck('กรุณากรอกคำนำหน้า');
            // this.setState({ fff: false })
        } else if (e.fname === null || e.fname === '') {
            this.swalCheck('กรุณากรอกชื่อจริง');
            // this.setState({ fff: false })
        } else {
            this.props.lll(this.setState({ fff: true }))
        }
    }
    render() {
        return (
            <div>
                {this.onCheck(this.props.check)}

            </div>
        )
    }
}
