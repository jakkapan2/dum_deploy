import React, { Component, useState, useEffect } from 'react';
import { TextLine } from '../TextHead';
import { address } from '../../functions/index';
import MapComponent from '../Maps/MapComponent';
import { path } from '../../api';
import _ from 'lodash';
import User from '../../mobx/user';
import { Button, LabelBT } from '../../Asset/styleAdmin';
import { PanelWhi, PanelWhiMar } from '../PartAdmin/MainAdmin';
import { SumTrack, Sumtext, SumDetail } from '../Div/SetDiv';
import { ContentLeft } from '../../Asset/styled';
import { Row, Col, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import '../../Asset/css/adminAddCompaintStyle.css';
import { Img } from '../../Components/Image/Img';
import LoadingSend from '../../Components/Load/LoadingSend';
import Images from '../../Components/Images';
import moment from 'moment';
import 'moment/locale/th';
import { InputFormGroup } from './Input';
import editP from '../../Asset/icons/edit.png';
import conf from '../../Asset/icons/confirm.png';
import { get, post } from '../../api';

moment.locale('th');

export function Divls({ title, detail, onClick, request, requestLink }) {
	return (
		<div>
			<label>{title}</label>
			{!requestLink && <span style={{ color: request ? '#29a4db' : '' }}>{detail}</span>}
			{requestLink && (
				<a style={{ color: request ? '#29a4db' : '' }} onClick={onClick}>
					{detail}
				</a>
			)}
		</div>
	);
}
export function DivInput({ title, detail, onClick, request, requestLink, type, form_id }) {
	const [ dis, setDis ] = useState(0);
	const [ dkk, setDkk ] = useState('');

	useEffect(() => {
		setDkk(detail);
	}, []);
	async function update_add_form(e) {
		// console.log(e.key);
		try {
			await post('/form/update_add_form', { name: dkk, type: type, form_id: form_id });
		} catch (err) {
			console.log(err);
		}
	}
	async function Enter_update_add_form(e) {
		// console.log(e.key);
		if (e.key === 'Enter') {
			try {
				await post('/form/update_add_form', { name: dkk, type: type, form_id: form_id });
				setDis(0);
			} catch (err) {
				console.log(err);
			}
		}
	}
	if (User.role === 'ADMIN') {
		return (
			<div>
				<label>{title}</label>
				<label style={{ width: '50%' }}>
					<InputGroup size="sm">
						<Input
							disabled={dis === 0}
							value={dkk}
							onKeyDown={Enter_update_add_form}
							onChange={(e) => setDkk(e.target.value)}
						/>
						<InputGroupAddon addonType="prepend">
							{dis === 0 ? (
								<button onClick={() => setDis(1)}>
									<img src={editP} />
								</button>
							) : (
								<button
									onClick={() => {
										setDis(0);
										update_add_form();
									}}
								>
									<img src={conf} />
								</button>
							)}
						</InputGroupAddon>
					</InputGroup>
				</label>
			</div>
		);
	} else {
		if (type === 'name_admin') {
			return (
				<div>
					<label>{title}</label>
					<span>{dkk}</span>
				</div>
			);
		} else if (type === 'name_user_central') {
			return (
				<div>
					<label>{title}</label>
					<label style={{ width: '50%' }}>
						<InputGroup size="sm">
							<Input
								disabled={dis === 0}
								value={dkk}
								onKeyDown={Enter_update_add_form}
								onChange={(e) => setDkk(e.target.value)}
							/>
							<InputGroupAddon addonType="prepend">
								{dis === 0 ? (
									<button onClick={() => setDis(1)}>
										<img src={editP} />
									</button>
								) : (
									<button
										onClick={() => {
											setDis(0);
											update_add_form();
										}}
									>
										<img src={conf} />
									</button>
								)}
							</InputGroupAddon>
						</InputGroup>
					</label>
				</div>
			);
		} else {
			return (
				<div>
					<label>{title}</label>
					<label style={{ width: '50%' }}>
						<InputGroup size="sm">
							<Input
								disabled={dis === 0}
								value={dkk}
								onKeyDown={Enter_update_add_form}
								onChange={(e) => setDkk(e.target.value)}
							/>
							<InputGroupAddon addonType="prepend">
								{dis === 0 ? (
									<button onClick={() => setDis(1)}>
										<img src={editP} />
									</button>
								) : (
									<button
										onClick={() => {
											setDis(0);
											update_add_form();
										}}
									>
										<img src={conf} />
									</button>
								)}
							</InputGroupAddon>
						</InputGroup>
					</label>
				</div>
			);
		}
	}
}
export function Head_compaint({
	modallistname,
	onClick,
	data,
	focut_head,
	addonClick,
	intensive,
	formstaff_join,
	btnTables,
	chat_staff,
	insert_staff_chat,
	set_message,
	onEnter,
	messagesEnd,
	message,
	chat_customer,
	uploadImg_chat_customer,
	uploadFile_chat_customer,
	imgnamechat_customer,
	filenamechat_customer,
	DelImg_chat_customer,
	DelFile_chat_customer,
	insert_customer_chat,
	onEnter_customer,
	openPic,
	ssQRcode,
	rightIntensive,
	loadingSend
}) {
	console.log('data', data);
	return focut_head === 1 ? (
		<PanelWhi>
			<div className="d-flex justify-content-between align-items-start">
				<div className="depart-grid">
					<Divls title="รหัสเรื่องร้องเรียน" detail={data.id === null || !data.id ? '-' : data.id} />
					{/* <QRCode value={String(data.form_id)} /> */}
					<Divls title="รหัส MOI" detail={data.moi === null || !data.moi ? '-' : data.moi} />
					<Divls
						title="แจ้งเรื่องวันที่"
						detail={
							data.insert_at === null || !data.insert_at ? (
								'-'
							) : (
								moment(data.insert_at).add(543, 'y').format('DD MMMM YYYY HH:mm:ss')
							)
						}
					/>
					<Divls
						title="วันที่รับเรื่อง"
						detail={
							data.receive_at === null || !data.receive_at ? (
								'-'
							) : (
								moment(data.receive_at).add(543, 'y').format('DD MMMM YYYY HH:mm:ss')
							)
						}
					/>
					<Divls
						title="ช่องทางการร้องเรียน"
						detail={data.platform === null || !data.platform ? '-' : data.platform}
					/>
					<Divls
						title="สถานะเรื่อง"
						detail={data.status_name === null || !data.status_name ? '-' : data.status_name}
						request
					/>
					<Divls title="รับเรื่องโดย" detail={data.insert_by === 'CENTRAL' ? 'หน่วยงาน' : 'ศูนย์ดำรงธรรม'} />
					<DivInput
						title="เจ้าหน้าที่ศูนย์ดำรงธรรมที่รับผิดชอบ"
						detail={data.name_admin}
						type={'name_admin'}
						form_id={data.form_id}
					/>
					<DivInput
						title="เจ้าหน้าที่หน่วยงานที่รับผิดชอบ"
						detail={data.name_user_central}
						type={'name_user_central'}
						form_id={data.form_id}
					/>
					<DivInput
						title="หน่วยงานที่รับผิดชอบ"
						detail={data.name_central}
						type={'name_central'}
						form_id={data.form_id}
					/>
				</div>
				<div className="d-flex">
					<Button onClick={() => ssQRcode(data.form_id)}>
						<i className="fas fa-qrcode" /> QR CODE
					</Button>
				</div>
			</div>
		</PanelWhi>
	) : // ---------------------------------------------------------------------------------------------------------------------------------------------------
	focut_head === 2 ? (
		<div>
			<PanelWhiMar>
				{formstaff_join.length > 0 ? User.role === 'ADMIN' ? (
					<div className="depart-chat-people">
						<div>
							{formstaff_join.filter((e) => e.status === 1).length > 0 && (
								<div>
									<ContentLeft>
										<SumTrack title="ผู้ร่วมห้องสนทนา" />
									</ContentLeft>
									<div>
										{formstaff_join
											.filter((e) => e.status === 1)
											.slice(0, 5)
											.map((e) => (
												<Img
													w={'50px'}
													h={'50px'}
													src={
														e.profile_pic === 'null' || e.profile_pic === null ? (
															Images.Asset4
														) : (
															path + '/static/' + e.profile_pic
														)
													}
												/>
											))}
										<i className="add fas fa-ellipsis-h" onClick={() => modallistname(1)} />
									</div>
								</div>
							)}
							{formstaff_join.filter((e) => e.status === 0).length > 0 && (
								<div className="waiting">
									รอการตอบรับ : &nbsp;
									{formstaff_join
										.filter((e) => e.status === 0)
										.slice(0, 5)
										.map((e) => <text>{e.central_name}&nbsp;,&nbsp;</text>)}
									<i className="add-wait fas fa-ellipsis-h" onClick={() => modallistname(0)} />
								</div>
							)}
						</div>
						<Button onClick={onClick}>เพิ่มหน่วยงาน </Button>
					</div>
				) : (
					<div>
						{formstaff_join.some(
							(e) =>
								e.user_info_id === User.user_info_id &&
								e.status === 1 &&
								e.central_id === User.central_id
						) ? (
							<div className="depart-chat-people">
								<div>
									{formstaff_join.filter((e) => e.status === 1).length > 0 && (
										<div>
											<ContentLeft>
												<SumTrack title="ผู้ร่วมห้องสนทนา" />
											</ContentLeft>
											<div>
												{formstaff_join
													.filter((e) => e.status === 1)
													.slice(0, 5)
													.map((e) => (
														<Img
															w={'50px'}
															h={'50px'}
															src={
																e.profile_pic === 'null' || e.profile_pic === null ? (
																	Images.Asset4
																) : (
																	path + '/static/' + e.profile_pic
																)
															}
														/>
													))}
												<i className="add fas fa-ellipsis-h" onClick={() => modallistname(1)} />
											</div>
										</div>
									)}
									{formstaff_join.filter((e) => e.status === 0).length > 0 && (
										<div className="waiting">
											รอการตอบรับ : &nbsp;
											{formstaff_join
												.filter((e) => e.status === 0)
												.slice(0, 5)
												.map((e) => <text>{e.central_name}&nbsp;,&nbsp;</text>)}
											<i
												className="add-wait fas fa-ellipsis-h"
												onClick={() => modallistname(0)}
											/>
										</div>
									)}
								</div>
								{/* <Button onClick={onClick}>เพิ่มหน่วยงาน </Button> */}
								<div />
							</div>
						) : formstaff_join.some((e) => e.status === 0 && e.central_id === User.central_id) ? (
							<div>
								<text style={{ width: '100%', fontWeight: '200px', fontSize: '15px' }}>
									------ คุณมีสิทธิ์รับเรื่อง ------
								</text>
								<br />
								<div>
									<Button w="100px" mx="auto" onClick={() => btnTables({ form_id: data.form_id })}>
										รับเรื่อง
									</Button>
								</div>
							</div>
						) : (
							<text style={{ width: '100%', fontWeight: '200px', fontSize: '15px' }}>
								------ มีบุคคลากรคนอื่นรับเรื่องไปแล้ว ------
							</text>
						)}
					</div>
				) : User.role === 'ADMIN' ? (
					<div>
						<div>
							<text style={{ width: '100%', fontWeight: '200px', fontSize: '15px' }}>
								กรุณาเลือกหน่วยงาน
							</text>
						</div>
						<div className="d-flex justify-content-center mt-3">
							<Button onClick={onClick}>เลือกหน่วยงาน</Button>
						</div>
					</div>
				) : (
					<div>
						<div>
							<text style={{ width: '100%', fontWeight: '200px', fontSize: '15px' }}>
								------ คุณไม่มีสิทธิ์เข้าร่วมห้องสนทนา ------
							</text>
						</div>
					</div>
				)}
			</PanelWhiMar>
			{/* ----------------------------------------------------------------------------------------------------------- */}
			{User.role === 'ADMIN' && formstaff_join.some((e) => e.status === 1) ? (
				<PanelWhiMar>
					<div className="depart-chat styleScroll">
						{chat_staff.length > 0 &&
							chat_staff.sort((a, b) => moment(a.timestamp) - moment(b.timestamp)).map((e) => (
								<div>
									<div>
										<Img
											w={'40px'}
											h={'40px'}
											src={
												e.profile_pic === 'null' || e.profile_pic === null ? (
													Images.Asset4
												) : (
													path + '/static/' + e.profile_pic
												)
											}
										/>
										<div>
											<div>
												<strong>{e.name}</strong>
												<span>เจ้าหน้าที่{e.central_name}</span>
											</div>
											<text>{e.message}</text>
										</div>
									</div>
									<text>{moment(e.timestamp).add(543, 'years').format('LLL')}</text>
								</div>
							))}
						<div ref={(el) => messagesEnd(el)}> </div>
					</div>
					<div className="d-flex align-items-end w-100">
						<InputFormGroup
							value={message}
							onChange={(e) => set_message(e.target.value)}
							placeholder="กรอกข้อมูล"
							onKeyDown={onEnter}
							className={'w-100'}
							r={'20px'}
						/>
						<Button my={'1rem'} onClick={insert_staff_chat}>
							ส่ง
						</Button>
					</div>
				</PanelWhiMar>
			) : (
				formstaff_join.some(
					(e) => e.user_info_id === User.user_info_id && e.status === 1 && e.central_id === User.central_id
				) && (
					<PanelWhiMar>
						<div className="depart-chat styleScroll">
							{chat_staff.length > 0 &&
								chat_staff.sort((a, b) => moment(a.timestamp) - moment(b.timestamp)).map((e) => (
									<div>
										<div>
											<Img
												w={'40px'}
												h={'40px'}
												src={
													e.profile_pic === 'null' || e.profile_pic === null ? (
														Images.Asset4
													) : (
														path + '/static/' + e.profile_pic
													)
												}
											/>
											<div>
												<div>
													<strong>{e.name}</strong>
													<span>เจ้าหน้าที่{e.central_name}</span>
												</div>
												<text>{e.message}</text>
											</div>
										</div>
										<text>{moment(e.timestamp).add(543, 'years').format('LLL')}</text>
									</div>
								))}
							<div ref={(el) => messagesEnd(el)}> </div>
						</div>
						<div className="d-flex align-items-end w-100">
							<InputFormGroup
								value={message}
								onChange={(e) => set_message(e.target.value)}
								placeholder="กรอกข้อมูล"
								onKeyDown={onEnter}
								className={'w-100'}
								r={'20px'}
							/>
							<Button my={'1rem'} onClick={insert_staff_chat}>
								ส่ง
							</Button>
						</div>
					</PanelWhiMar>
				)
			)}
		</div>
	) : focut_head === 3 ? (
		<PanelWhiMar>
			{rightIntensive == true ? (
				<div>---- ผู้ร้องเรียนปกปิดตัวตน ----</div>
			) : (
				<div>
					<div className="chat-room styleScroll">
						<div>
							{chat_customer.length !== 0 ? (
								chat_customer.sort((a, b) => moment(a.timestamp) - moment(b.timestamp)).map((e) => (
									<div>
										<div>
											<Img
												w={'40px'}
												h={'40px'}
												src={
													e.profile_pic === 'null' || e.profile_pic === null ? (
														Images.Asset4
													) : (
														path + '/static/' + e.profile_pic
													)
												}
											/>
											<div>
												<div>
													<strong>{e.name}</strong>
													<span>เจ้าหน้าที่{e.central_name}</span>
												</div>
												{/* <div className="chat-room-file">
															<div>
																{e.chat_file.map(
																	(el) =>
																		el.split('.')[1] === 'pdf' ? null : (
																			<img
																				src={path + '/static/' + el}
																				height={'100px'}
																				onClick={() => openPic(path + '/static/' + el)}
																			/>
																		)
																)}
															</div> */}
												{console.log('xvsdfsdfsdf', e.chat_file)}
												{e.chat_file &&
												e.chat_file.length > 0 && (
													<div
														className="chat-room-file"
														style={{ gridTemplateColumns: 'auto auto auto auto auto auto' }}
													>
														{e.chat_file.map(
															(el) =>
																el.split('.')[1] === 'pdf' ? null : (
																	<div style={{ width: '150px', height: '100px' }}>
																		<img
																			src={path + '/static/' + el}
																			height={'100px'}
																			onClick={() =>
																				openPic(path + '/static/' + el)}
																		/>
																	</div>
																)
														)}
														<div>
															{e.chat_file.map(
																(el) =>
																	el.split('.')[1] === 'pdf' ? (
																		<a
																			className={`list depart-file pdf`}
																			href={`${path}/static/${el}`}
																			target="_blank"
																		>
																			<div>{el}</div>
																		</a>
																	) : null
															)}
														</div>
													</div>
												)}
												{/* </div> */}
												<text className="text-align-left">{e.message}</text>
											</div>
										</div>
										<text>{moment(e.timestamp).add(543, 'years').format('LLL')}</text>
									</div>
								))
							) : (
								<div className="h1 d-flex justify-content-center align-items-center">โบกมือทักทาย</div>
							)}
							<div>
								<div
									className="chat-room-uploadfile"
									style={{ gridTemplateColumns: 'auto', width: '950px' }}
								>
									{/* <div>
												{imgnamechat_customer.length > 0 &&
													imgnamechat_customer.map((e, i) => (
														<div>
															<img src={e} height={100} />
															{imgnamechat_customer.length > 0 && loadingSend === true ? (
																<div>
																	<span
																		className="d-flex w-100 h-100"
																		style={{
																			background: '#ffffff50',
																			position: 'absolute',
																			top: '0',
																			left: '0'
																		}}
																	/>
																	<LoadingSend
																		w={'25px'}
																		h={'25px'}
																		r={'calc(50% - 12px)'}
																		t={'calc(50% - 12px)'}
																	/>
																</div>
															) : (
																	<i
																		className="far fa-times-circle"
																		onClick={() => DelImg_chat_customer(i)}
																	/>
																)}
														</div>
													))}
											</div> */}
									{imgnamechat_customer.length > 0 && (
										<div>
											{imgnamechat_customer.length > 0 &&
												imgnamechat_customer.map((e, i) => (
													<div>
														<img src={e} height={100} />
														{imgnamechat_customer.length > 0 && loadingSend === true ? (
															<div>
																<span
																	className="d-flex w-100 h-100"
																	style={{
																		background: '#ffffff50',
																		position: 'absolute',
																		top: '0',
																		left: '0'
																	}}
																/>
																<LoadingSend
																	w={'25px'}
																	h={'25px'}
																	r={'calc(50% - 12px)'}
																	t={'calc(50% - 12px)'}
																/>
															</div>
														) : (
															<i
																className="far fa-times-circle"
																onClick={() => DelImg_chat_customer(i)}
																style={{ top: '2px' }}
															/>
														)}
													</div>
												))}
										</div>
									)}
									{filenamechat_customer.length > 0 && (
										<div>
											{filenamechat_customer.map((e, i) => (
												<div
													style={{ width: '230px' }}
													className={`list depart-file ${e.match(/\.(png|jpg|jpeg)$/gi)
														? 'jpg'
														: 'pdf'}`}
												>
													<div>{e}</div>
													{filenamechat_customer.length > 0 && loadingSend === true ? (
														<LoadingSend
															w={'20px'}
															h={'20px'}
															r={'5px'}
															t={'calc(50% - 10px)'}
														/>
													) : (
														<i
															className="far fa-times-circle"
															onClick={() => DelFile_chat_customer(i)}
														/>
													)}
												</div>
											))}
										</div>
									)}
								</div>
							</div>
						</div>
						<div ref={(el) => messagesEnd(el)}> </div>
					</div>
					<div className="d-flex justify-content-between align-items-end">
						<div className="d-flex mb-3">
							<LabelBT
								bg="transparent"
								bgH="transparent"
								c="#000"
								bord="1px solid #ccc"
								mx="0px"
								className="file-upload jpg"
							>
								<input
									type="file"
									accept="image/x-png,image/jpeg"
									multiple
									onChange={uploadImg_chat_customer}
								/>
								&nbsp;&nbsp;&nbsp; อัพโหลดรูปภาพ
							</LabelBT>
							<LabelBT
								bg="transparent"
								bgH="transparent"
								c="#000"
								bord="1px solid #ccc"
								className="file-upload pdf"
							>
								<input
									type="file"
									accept="application/pdf"
									multiple
									onChange={uploadFile_chat_customer}
								/>
								&nbsp;&nbsp;&nbsp; อัพโหลดเอกสาร
							</LabelBT>
						</div>
						<InputFormGroup
							value={message}
							onChange={(e) => set_message(e.target.value)}
							placeholder="กรอกข้อมูล"
							onKeyDown={onEnter_customer}
							className={'w-60'}
							r={'20px'}
						/>
						<Button my={'1rem'} onClick={insert_customer_chat}>
							ส่ง
						</Button>
					</div>
				</div>
			)}
		</PanelWhiMar>
	) : // ---------------------------------------------------------------------------------------------------------------------------------------------------
	focut_head === 4 ? (
		<PanelWhiMar>
			<embed
				src={`https://damrongdhamakk.go.th/api/v1/form/pdf/${data.form_id}`}
				type="application/pdf"
				scrolling="auto"
				height="1000"
				width="850"
			/>
		</PanelWhiMar>
	) : (
		// ---------------------------------------------------------------------------------------------------------------------------------------------------
		focut_head === 5 && (
			<PanelWhiMar>
				<ContentLeft>
					<Sumtext title="คำสั่งการจากผู้หารระดับสูง" />
				</ContentLeft>
				<hr />
				{intensive.length > 0 ? (
					intensive.map((e) => (
						<div style={{ paddingLeft: '3rem', paddingRight: '3rem' }}>
							<div style={{ display: 'flex', justifyContent: 'space-between' }}>
								<div>ผู้ว่าราชการจังหวัด</div>
								<div>{moment(e.timestamp).add(543, 'y').format('DD MMMM YYYY HH:mm:ss')}</div>
							</div>
							<div>{e.message}</div>
						</div>
					))
				) : (
					<div>---ไม่มีคำสั่งการจากผู้หารระดับสูง---</div>
				)}
			</PanelWhiMar>
		)
	);
}
export function History({
	data,
	uploadfile,
	filesupload,
	delImages,
	descrition,
	insert_form_tracking,
	del_tracking,
	openPic,
	role,
	description,
	loadingSend,
	formstaff_join
}) {
	// console.log('data', data);
	let USER = JSON.parse(localStorage.getItem('user'));
	return (
		<div>
			{role !== 'user' ? USER.role === 'ADMIN' ? (
				<div>
					<textarea
						value={description}
						placeholder="กรอกข้อความ"
						style={{ width: '100%', minHeight: '100px', borderRadius: '5px', padding: '5px' }}
						onChange={(e) => descrition(e.target.value)}
					/>
					{filesupload &&
					filesupload.length > 0 && (
						<div className="mb-3">
							<div className="text-left mt-2">รายการอัพโหลด</div>
							<hr />

							{filesupload.map((e, i) => (
								<div
									className={`list depart-file ${e.name.match(/\.(png|jpg|jpeg)$/gi)
										? 'jpg'
										: 'pdf'}`}
								>
									<div>{e.name}</div>
									{loadingSend === true ? (
										<LoadingSend w={'15px'} h={'15px'} r={'3px'} t={'calc(50% - 7px)'} />
									) : (
										<i className="far fa-times-circle" onClick={() => delImages(i)} />
									)}
								</div>
							))}
						</div>
					)}
					<div style={{ display: 'flex', justifyContent: 'space-between' }}>
						<div style={{ display: 'flex' }}>
							<LabelBT
								bg="transparent"
								bgH="transparent"
								c="#000"
								bord="1px solid #ccc"
								mx="0px"
								className="file-upload jpg"
							>
								<input type="file" accept="image/x-png,image/jpeg" multiple onChange={uploadfile} />
								&nbsp;&nbsp;&nbsp; อัพโหลดรูปภาพ
							</LabelBT>
							<LabelBT
								bg="transparent"
								bgH="transparent"
								c="#000"
								bord="1px solid #ccc"
								className="file-upload pdf"
							>
								<input type="file" accept="application/pdf" multiple onChange={uploadfile} />
								&nbsp;&nbsp;&nbsp; อัพโหลดเอกสาร
							</LabelBT>
						</div>
						<Button mx="0" onClick={insert_form_tracking}>
							บันทึก
						</Button>
					</div>
					<hr />
				</div>
			) : (
				formstaff_join.some(
					(e) => e.user_info_id === USER.user_info_id && e.status === 1 && e.central_id === USER.central_id
				) && (
					<div>
						<textarea
							value={description}
							placeholder="กรอกข้อความ"
							style={{ width: '100%', minHeight: '100px', borderRadius: '5px', padding: '5px' }}
							onChange={(e) => descrition(e.target.value)}
						/>
						{filesupload &&
						filesupload.length > 0 && (
							<div className="mb-3">
								<div className="text-left mt-2">รายการอัพโหลด</div>
								<hr />

								{filesupload.map((e, i) => (
									<div
										className={`list depart-file ${e.name.match(/\.(png|jpg|jpeg)$/gi)
											? 'jpg'
											: 'pdf'}`}
									>
										<div>{e.name}</div>
										{loadingSend === true ? (
											<LoadingSend w={'15px'} h={'15px'} r={'3px'} t={'calc(50% - 7px)'} />
										) : (
											<i className="far fa-times-circle" onClick={() => delImages(i)} />
										)}
									</div>
								))}
							</div>
						)}
						<div style={{ display: 'flex', justifyContent: 'space-between' }}>
							<div style={{ display: 'flex' }}>
								<LabelBT
									bg="transparent"
									bgH="transparent"
									c="#000"
									bord="1px solid #ccc"
									mx="0px"
									className="file-upload jpg"
								>
									<input type="file" accept="image/x-png,image/jpeg" multiple onChange={uploadfile} />
									&nbsp;&nbsp;&nbsp; อัพโหลดรูปภาพ
								</LabelBT>
								<LabelBT
									bg="transparent"
									bgH="transparent"
									c="#000"
									bord="1px solid #ccc"
									className="file-upload pdf"
								>
									<input type="file" accept="application/pdf" multiple onChange={uploadfile} />
									&nbsp;&nbsp;&nbsp; อัพโหลดเอกสาร
								</LabelBT>
							</div>
							<Button mx="0" onClick={insert_form_tracking}>
								บันทึก
							</Button>
						</div>
						<hr />
					</div>
				)
			) : null}
			<div className="styleScroll">
				{data.trackinHistory && data.trackinHistory.length > 0 ? (
					data.trackinHistory.sort((a, b) => moment(b.datetime) - moment(a.datetime)).map((e) => (
						<div className="depart-tracking">
							<div>
								<div>
									<div>{e.fname + ' ' + e.lname}</div>
									{role === 'user' ? (
										''
									) : (
										<span>{moment(e.datetime).add(543, 'y').format('DD MMMM YYYY HH:mm')} น.</span>
									)}
								</div>
								<div>
									{role === 'user' ? (
										<span style={{ color: '#777' }}>
											{moment(e.datetime).add(543, 'y').format('DD MMMM YYYY HH:mm')} น.
										</span>
									) : (
										Number(USER.id_card) === Number(e.id_card) && (
											<span onClick={() => del_tracking(e.tracking_id)}>ลบ</span>
										)
									)}
								</div>
							</div>
							<div>{e.description}</div>
							<div className="depart-about-file">
								{e.files.length > 0 && 'ไฟล์ที่เกี่ยวข้อง :'}
								{e.files.length > 0 ? (
									e.files.map((e) => (
										<a className="pdf" href={`${path}${e}`} target="_blank">
											{e.split('/')[2]}
										</a>
									))
								) : (
									''
								)}
							</div>
							<div>
								{e.pic.length === 1 ? (
									e.pic.map(
										(e) =>
											role === 'user' ? (
												<img src={path + e} alt="" onClick={() => openPic(path + e)} />
											) : (
												<img
													className="full"
													src={path + e}
													alt=""
													onClick={() => openPic(path + e)}
												/>
											)
									)
								) : (
									e.pic.length > 0 &&
									e.pic.map((e) => <img src={path + e} alt="" onClick={() => openPic(path + e)} />)
								)}
							</div>
							<hr />
						</div>
					))
				) : (
					<div className="text-center">---ไม่มีรายการ---</div>
				)}
			</div>
		</div>
	);
}
export function DetailComp(props) {
	// console.log('props', props);
	const {
		complainant,
		subject,
		type,
		sub_type,
		prefix,
		fname,
		lname,
		id_card,
		id_line,
		email,
		zipcode,
		location_name,
		disclose,
		complainant_data,
		detail,
		status,
		objective,
		phone
	} = props.data;
	let coords = {
		latitude: props.data.lat,
		longitude: props.data.lng,
		location_name: props.data.location_name
	};
	let markerCoords = {
		lat: props.data.lat,
		lng: props.data.lng
	};
	let nameFile = props.data.evidence_files
		.map((e) => {
			let link = e.path;
			let name = e.filename;
			let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
				.add(543, 'y')
				.format('DD MMMM YYYY');
			return { link: link, name: name, date: date, type: 'pdf' };
		})
		.concat(
			props.data.evidence_pic.map((e) => {
				let link = e.path;
				let name = e.filename;
				let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
					.add(543, 'y')
					.format('DD MMMM YYYY');
				return { link: link, name: name, date: date, type: 'jpg' };
			})
		);
	// console.log('nameFile', nameFile);
	// console.log('props.data', props.data);
	return (
		<div>
			<TextLine title="เรื่องร้องทุกข์" request />
			<Sumtext title="หัวข้อร้องทุกข์" detail={!subject ? '-' : subject} wl="160px" />
			<Sumtext title="ประเภทเรื่อง" detail={type} wl="160px" />
			<Sumtext title="ประเภทเรื่องย่อย" detail={sub_type === 'เลือก' ? '-' : sub_type} wl="160px" />
			{/* -------------------------------------------------------------- */}
			<TextLine title="ข้อมูลผู้ร้อง" request />
			{disclose === 0 ? (
				<Sumtext title="ปกปิดข้อมูล" />
			) : (
				<div>
					<Sumtext title="ชื่อ-นามสกุลผู้ร้อง" detail={prefix + ' ' + fname + ' ' + lname} wl="160px" />
					<Sumtext title="หมายเลขประจำตัวประชาชน" detail={id_card} wl="160px" />
					<Sumtext title="หมายเลขโทรศัพท์" detail={phone ? phone : '-'} wl="160px" />
					{/* <Sumtext title="ID LINE ผู้ร้องทุกข์" detail={id_line ? id_line : '-'} wl="160px" /> */}
					<Sumtext title="E-mail ผู้ร้องทุกข์" detail={email ? email : '-'} wl="160px" />
					<Sumtext
						title="ที่อยู่"
						detail={
							address({
								..._.pick(props.data, [
									'house_number',
									'moo',
									'alleyway',
									'road',
									'sub_district',
									'district',
									'province'
								])
							}) + zipcode
						}
						wl="160px"
					/>
				</div>
			)}
			{/* -------------------------------------------------------------- */}
			<TextLine title="ผู้ถูกร้องทุกข์" request />
			{/* CENTRAL=หน่วยงาน,DEPARTMENT=หน่วยงานจากepam,PERSON=บุคคล,COMPANY=นิติบุคคล  */}
			{complainant === 'DEPARTMENT' ? (
				<Department item={complainant_data} />
			) : complainant === 'PERSON' ? (
				<Person item={complainant_data} />
			) : (
				<Company item={complainant_data} />
			)}
			{/* -------------------------------------------------------------- */}
			<TextLine title="รายละเอียดคำร้อง" request />
			<Row>
				<Col xs={User.role === 'ADMIN' ? 12 : 7}>
					<MapComponent
						coords={coords}
						markerCoords={markerCoords}
						onMarkerChange={(e) => console.log('e', e)}
					/>
				</Col>
				<Col xs={User.role === 'ADMIN' ? 12 : 5} className={`pl-3 ${User.role === 'ADMIN' ? 'pt-3' : ''}`}>
					{/* <Sumtext title="สถานที่เกิดเหตุ  " detail={location_name} wl="100px" /> */}
					<div className="d-grid text-left" style={{ gridTemplateColumns: '100px auto' }}>
						<label>สถานที่เกิดเหตุ</label>
						<label className="text-left pl-3">{location_name ? location_name : '-'}</label>
					</div>
					<SumDetail title="รายละเอียด  " detail={detail ? detail : '-'} />
					<SumDetail title="วัตถุประสงค์  " detail={objective ? objective : '-'} />
				</Col>
			</Row>
			{/* -------------------------------------------------------------- */}
			{props.role === 'user' && (
				<div>
					<TextLine title="ไฟล์เอกสาร" request />
					{nameFile.length > 0 ? (
						nameFile.map(
							(e) =>
								e.type === 'pdf' ? (
									<a className={'depart-file  pdf'} href={`${path}${e.link}`} target="_blank">
										<div>{e.name}</div>
										<div>{e.date}</div>
									</a>
								) : (
									<a className={'depart-file jpg'} onClick={() => props.openPic(path + e.link)}>
										<div>{e.name}</div>
										<div>{e.date}</div>
									</a>
								)
						)
					) : (
						<div className="-comp-upload text-center small" style={{ minHeigth: '50px' }}>
							ไม่มีไฟล์เอกสาร
						</div>
					)}
				</div>
			)}
		</div>
	);
}
export function Department(props) {
	// console.log('propsCompany', props.item)
	let { department_name } = props.item;
	return (
		<React.Fragment>
			<Sumtext title="ข้อมูลผู้ถูกร้อง" detail="หน่วยงาน" wl="160px" />
			<Sumtext title="หน่วยงานที่ถูกร้อง" detail={!department_name ? '-' : department_name} wl="160px" />
		</React.Fragment>
	);
}
export function Person(props) {
	// console.log('propsCompany', props.item)
	let { fname_person, lname_person, zipcode_person, phone_person } = props.item;
	return (
		<React.Fragment>
			<Sumtext title="ข้อมูลผู้ถูกร้อง" detail="บุคคล" wl="160px" />
			<Sumtext title="ชื่อ-นามสกุล" detail={fname_person + ' ' + lname_person} wl="160px" />
			<Sumtext title="เบอร์โทรศัพท์" detail={phone_person ? phone_person : '-'} wl="160px" />
			<Sumtext
				title="ที่อยู่"
				detail={
					address({
						..._.pick(props.item, [
							'house_number_person',
							'moo_person',
							'alleyway_person',
							'road_person',
							'sub_district_person',
							'district_person',
							'province_person'
						])
					}) + zipcode_person
				}
				wl="160px"
			/>
		</React.Fragment>
	);
}
export function Company(props) {
	// console.log('propsCompany', props);
	let { name_company, zipcode_company, phone_company, type_name } = props.item;
	return (
		<React.Fragment>
			<Sumtext title="ข้อมูลผู้ถูกร้อง" detail="นิติบุคคล" wl="160px" />
			<Sumtext title="ประเภท" detail={type_name} wl="160px" />
			<Sumtext title="ชื่อนิติบุคคลหรือบริษัท" detail={name_company ? name_company : '-'} wl="160px" />
			<Sumtext title="หมายเลขโทรศัพท์" detail={phone_company ? phone_company : '-'} wl="160px" />
			<Sumtext
				title="ที่อยู่"
				detail={
					address({
						..._.pick(props.item, [ 'sub_district_company', 'district_company', 'province_company' ])
					}) + zipcode_company
				}
				wl="160px"
			/>
		</React.Fragment>
	);
}
function HistoryStatus(status) {
	// console.log('status', status)
	switch (status) {
		case 0:
			return 'รอเจ้าหน้าที่รับเรื่อง';
		case 1:
			return 'กำลังตรวจสอบคำร้อง';
		case 2:
			return 'ตรวจสอบข้อเท็จจริง';
		case 3:
			return 'หน่วยงานกำลังดำเนินการ';
		case 4:
			return 'คำร้องไม่ชัดเจน';
		case 5:
			return 'ยุติเรื่อง';
		default:
			break;
	}
}
