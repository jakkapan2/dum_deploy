import React, { useState, useEffect } from 'react';
import { Row } from 'reactstrap';
import { Label } from '../../Asset/styled';
import { get, post } from '../../api';

export const Selecter = ({ name, onChange = () => {}, active = true, value }) => {
	// console.log('value', value);
	const [ province, setProvince ] = useState([]); // allProvince
	const [ province_name, setProviceName ] = useState('');
	const [ district, setDistrict ] = useState([]); // all
	const [ district_name, setDistrictName ] = useState('');
	const [ tumbon, setTumbon ] = useState([]); // all
	const [ tumbon_name, setTumbonName ] = useState('');
	const [ zipcode, setZipcode ] = useState('');

	const [ province_selected, setSelectProvince ] = useState(null);
	const [ district_selected, setSelectDistrict ] = useState(null);
	// const [ sub_district_selected, setSelectSubDistrict ] = useState(null);
	// console.log(object);
	useEffect(() => {
		get_province();
	}, []);

	// set province if have got
	useEffect(
		() => {
			const idProvince = province.find((el) => el.label === value.complaint.province);
			// console.log('idProvince', idProvince);
			if (idProvince) {
				get_district(value.complaint.province, idProvince.value);
				setProviceName(idProvince.label);
			} else {
				// console.log('idProvince', idProvince);
				setProviceName(value.complaint.province);
				setSelectProvince('all');
			}
		},
		[ province, value.complaint.province ]
	);

	// set district
	useEffect(
		() => {
			const idDistrict = district.find((el) => el.label === value.complaint.district);
			if (idDistrict) {
				get_tombon(value.complaint.district, idDistrict.value);
				setDistrictName(idDistrict.label);
			} else {
				setDistrictName(value.complaint.district);
				setSelectDistrict('all');
			}
		},
		[ district, value.complaint.district ]
	);

	// set sub district
	useEffect(
		() => {
			// console.log('tumbon', tumbon);
			const idSubDistrict = tumbon.find((el) => el.label === value.complaint.tumbon);
			if (idSubDistrict) {
				// console.log('idSubDistrict', idSubDistrict);
				// setSelectSubDistrict(idSubDistrict.value);
				setTumbonName(idSubDistrict.label);
			} else {
				// console.log('! idSubDistrict', idSubDistrict);
				setTumbonName(value.complaint.tumbon);
			}
		},
		[ tumbon, value.complaint.tumbon ]
	);

	useEffect(
		() => {
			if (value.complaint.zipcode) {
				setZipcode(value.complaint.zipcode);
			} else {
				setZipcode(value.complaint.zipcode);
			}
		},
		[ value.complaint.zipcode ]
	);

	useEffect(
		() => {
			onChange(name, { province: province_name, district: district_name, tumbon: tumbon_name, zipcode });
		},
		[ zipcode ]
	);

	async function get_province() {
		try {
			const allProvince = await get('/homeland/provinces');
			setProvince(allProvince);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_district(province_name, province_id) {
		try {
			const allDistrict = await post(`/homeland/districts`, { province_id });
			setDistrict(allDistrict);
			setProviceName(province_name);
			setSelectProvince(province_id);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_tombon(district_name, district_id) {
		try {
			const allTombon = await post(`/homeland/sub_districts`, { district_id });
			setTumbon(allTombon);
			setDistrictName(district_name);
			setSelectDistrict(district_id);
		} catch (err) {
			console.log(err);
		}
	}

	function _updateProvince(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_district(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateDistrict(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_tombon(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateTombon(event) {
		// console.log(event.target.value);
		const [ tumbon, zipcode ] = event.target.value.split(',');
		setTumbonName(tumbon);
		setZipcode(zipcode);
	}

	return (
		<Row style={{ paddingTop: 10 }}>
			<div className="col-sm-3 LabelInput-panel">
				<Label>จังหวัด</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateProvince}
					value={province_selected}
				>
					<option value="all">โปรดเลือกจังหวัด</option>
					{/* {console.log('province', province)} */}
					{province.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>

			<div className="col-sm-3 LabelInput-panel">
				<Label>อำเภอ</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateDistrict}
					value={district_selected}
				>
					<option value="all">โปรดเลือกอำเภอ</option>
					{district.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className="col-sm-3 LabelInput-panel">
				<Label>ตำบล</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateTombon}
					value={[ tumbon_name, zipcode ]}
				>
					<option value="all">โปรดเลือกตำบล</option>
					{tumbon.map((el) => (
						<option key={el.label} value={[ el.label, el.zipcode ]}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className={`col-sm-3 LabelInput-panel zipcodeStyle2 ${active?'zipcodeStyle22':''}`}>
				<Label>รหัสไปรษณีย์</Label>
				<input
					type="text"
					name="inputZipcode"
					value={zipcode}
					className=""
					disabled
					placeholder="รหัสไปรษณีย์"
				/>
			</div>
		</Row>
	);
};
export const SelecterPerson = ({ name, onChange = () => {}, active = true, value }) => {
	// console.log('value', value);
	const [ province, setProvince ] = useState([]); // allProvince
	const [ province_name, setProviceName ] = useState('');
	const [ district, setDistrict ] = useState([]); // all
	const [ district_name, setDistrictName ] = useState('');
	const [ tumbon, setTumbon ] = useState([]); // all
	const [ tumbon_name, setTumbonName ] = useState('');
	const [ zipcode, setZipcode ] = useState('');

	const [ province_selected, setSelectProvince ] = useState(null);
	const [ district_selected, setSelectDistrict ] = useState(null);
	// const [ sub_district_selected, setSelectSubDistrict ] = useState(null);
	// console.log(object);
	useEffect(() => {
		get_province();
	}, []);

	// set province if have got
	useEffect(
		() => {
			const idProvince = province.find((el) => el.label === value.center.province);
			if (idProvince) {
				get_district(value.center.province, idProvince.value);
			} else {
				setProviceName(value.center.province);
			}
		},
		[ province, value.center.province ]
	);

	// set district
	useEffect(
		() => {
			const idDistrict = district.find((el) => el.label === value.center.district);
			if (idDistrict) {
				get_tombon(value.center.district, idDistrict.value);
			} else {
				setDistrictName(value.center.district);
			}
		},
		[ district, value.center.district ]
	);

	// set sub district
	useEffect(
		() => {
			// console.log('tumbon', tumbon);
			const idSubDistrict = tumbon.find((el) => el.label === value.center.tumbon);
			if (idSubDistrict) {
				// console.log('idSubDistrict', idSubDistrict);
				// setSelectSubDistrict(idSubDistrict.value);
				setTumbonName(idSubDistrict.label);
			} else {
				// console.log('! idSubDistrict', idSubDistrict);
				setTumbonName(value.center.tumbon);
			}
		},
		[ tumbon, value.center.tumbon ]
	);

	useEffect(
		() => {
			if (value.center.zipcode) {
				setZipcode(value.center.zipcode);
			}
		},
		[ value.center.zipcode ]
	);

	useEffect(
		() => {
			onChange(name, { province: province_name, district: district_name, tumbon: tumbon_name, zipcode });
		},
		[ zipcode ]
	);

	async function get_province() {
		try {
			const allProvince = await get('/homeland/provinces');
			setProvince(allProvince);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_district(province_name, province_id) {
		try {
			const allDistrict = await post(`/homeland/districts`, { province_id });
			setDistrict(allDistrict);
			setProviceName(province_name);
			setSelectProvince(province_id);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_tombon(district_name, district_id) {
		try {
			const allTombon = await post(`/homeland/sub_districts`, { district_id });
			setTumbon(allTombon);
			setDistrictName(district_name);
			setSelectDistrict(district_id);
		} catch (err) {
			console.log(err);
		}
	}

	function _updateProvince(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_district(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateDistrict(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_tombon(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateTombon(event) {
		// console.log(event.target.value);
		const [ tumbon, zipcode ] = event.target.value.split(',');
		setTumbonName(tumbon);
		setZipcode(zipcode);
	}

	return (
		<Row style={{ paddingTop: 10 }}>
			<div className="col-sm-3 LabelInput-panel">
				<Label>จังหวัด</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateProvince}
					value={province_selected}
				>
					<option value="all">โปรดเลือกจังหวัด</option>
					{/* {console.log('province', province)} */}
					{province.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>

			<div className="col-sm-3 LabelInput-panel">
				<Label>อำเภอ</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateDistrict}
					value={district_selected}
				>
					<option value="all">โปรดเลือกอำเภอ</option>
					{district.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className="col-sm-3 LabelInput-panel">
				<Label>ตำบล</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateTombon}
					value={[ tumbon_name, zipcode ]}
				>
					<option value="all">โปรดเลือกตำบล</option>
					{tumbon.map((el) => (
						<option key={el.label} value={[ el.label, el.zipcode ]}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className="col-sm-3 LabelInput-panel zipcodeStyle2">
				<Label>รหัสไปรษณีย์</Label>
				<input
					type="text"
					name="inputZipcode"
					value={zipcode}
					className=""
					disabled
					placeholder="รหัสไปรษณีย์"
				/>
			</div>
		</Row>
	);
};
export const SelecterCompany = ({ name, onChange = () => {}, active = true, value }) => {
	// console.log('value', value);
	const [ province, setProvince ] = useState([]); // allProvince
	const [ province_name, setProviceName ] = useState('');
	const [ district, setDistrict ] = useState([]); // all
	const [ district_name, setDistrictName ] = useState('');
	const [ tumbon, setTumbon ] = useState([]); // all
	const [ tumbon_name, setTumbonName ] = useState('');
	const [ zipcode, setZipcode ] = useState('');

	const [ province_selected, setSelectProvince ] = useState(null);
	const [ district_selected, setSelectDistrict ] = useState(null);
	// const [ sub_district_selected, setSelectSubDistrict ] = useState(null);
	// console.log(object);
	useEffect(() => {
		get_province();
	}, []);

	// set province if have got
	useEffect(
		() => {
			const idProvince = province.find((el) => el.label === value.right.province);
			if (idProvince) {
				get_district(value.right.province, idProvince.value);
			} else {
				setProviceName(value.right.province);
			}
		},
		[ province, value.right.province ]
	);

	// set district
	useEffect(
		() => {
			const idDistrict = district.find((el) => el.label === value.right.district);
			if (idDistrict) {
				get_tombon(value.right.district, idDistrict.value);
			} else {
				setDistrictName(value.right.district);
			}
		},
		[ district, value.right.district ]
	);

	// set sub district
	useEffect(
		() => {
			// console.log('tumbon', tumbon);
			const idSubDistrict = tumbon.find((el) => el.label === value.right.tumbon);
			if (idSubDistrict) {
				// console.log('idSubDistrict', idSubDistrict);
				// setSelectSubDistrict(idSubDistrict.value);
				setTumbonName(idSubDistrict.label);
			} else {
				// console.log('! idSubDistrict', idSubDistrict);
				setTumbonName(value.right.tumbon);
			}
		},
		[ tumbon, value.right.tumbon ]
	);

	useEffect(
		() => {
			if (value.right.zipcode) {
				setZipcode(value.right.zipcode);
			}
		},
		[ value.right.zipcode ]
	);

	useEffect(
		() => {
			onChange(name, { province: province_name, district: district_name, tumbon: tumbon_name, zipcode });
		},
		[ zipcode ]
	);

	async function get_province() {
		try {
			const allProvince = await get('/homeland/provinces');
			setProvince(allProvince);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_district(province_name, province_id) {
		try {
			const allDistrict = await post(`/homeland/districts`, { province_id });
			setDistrict(allDistrict);
			setProviceName(province_name);
			setSelectProvince(province_id);
		} catch (err) {
			console.log(err);
		}
	}

	async function get_tombon(district_name, district_id) {
		try {
			const allTombon = await post(`/homeland/sub_districts`, { district_id });
			setTumbon(allTombon);
			setDistrictName(district_name);
			setSelectDistrict(district_id);
		} catch (err) {
			console.log(err);
		}
	}

	function _updateProvince(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_district(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateDistrict(event) {
		var index = event.nativeEvent.target.selectedIndex;
		get_tombon(event.nativeEvent.target[index].text, event.target.value);
	}
	function _updateTombon(event) {
		// console.log(event.target.value);
		const [ tumbon, zipcode ] = event.target.value.split(',');
		setTumbonName(tumbon);
		setZipcode(zipcode);
	}

	return (
		<Row style={{ paddingTop: 10 }}>
			<div className="col-sm-3 LabelInput-panel">
				<Label>จังหวัด</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateProvince}
					value={province_selected}
				>
					<option value="all">โปรดเลือกจังหวัด</option>
					{/* {console.log('province', province)} */}
					{province.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>

			<div className="col-sm-3 LabelInput-panel">
				<Label>อำเภอ</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateDistrict}
					value={district_selected}
				>
					<option value="all">โปรดเลือกอำเภอ</option>
					{district.map((el) => (
						<option key={el.label} value={el.value}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className="col-sm-3 LabelInput-panel">
				<Label>ตำบล</Label>
				<select
					disabled={!active}
					className="selectTitleName"
					onChange={_updateTombon}
					value={[ tumbon_name, zipcode ]}
				>
					<option value="all">โปรดเลือกตำบล</option>
					{tumbon.map((el) => (
						<option key={el.label} value={[ el.label, el.zipcode ]}>
							{el.label}
						</option>
					))}
				</select>
			</div>
			<div className="col-sm-3 LabelInput-panel zipcodeStyle2">
				<Label>รหัสไปรษณีย์</Label>
				<input
					type="text"
					name="inputZipcode"
					value={zipcode}
					className=""
					disabled
					placeholder="รหัสไปรษณีย์"
				/>
			</div>
		</Row>
	);
};
