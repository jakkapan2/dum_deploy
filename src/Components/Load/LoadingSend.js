import React, { Component } from 'react'
import './loaderstyle.css'
// import '../css/loaderstyle.css'
export default class LoadingSend extends Component {
    render() {
        let { w, h, r, t } = this.props
        return (
            <div className="lds-ring" style={{ width: w, height: h, right: r, top: t }}><div></div><div></div><div></div><div></div></div>
        )
    }
}