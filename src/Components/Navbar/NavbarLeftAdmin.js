import React, { useState, useEffect } from 'react';
// import { Link, withRouter } from 'react-router-dom';
import Images from '../Images';
import NavbarToptAdmin from './NavbarTopAdmin';
import { get, path } from '../../api';
import EditProfile from '../Modal/EditProfile'

const NavbarLeftAdmin = () => {
	const [profile, setProfile] = useState(null);
	const [openModal, setopenModal] = useState(false);
	const [Menuslide, setMenuslide] = useState(false);
	// ------------------------------------------------------------------------------------------------------------------
	function onOpenModal() {
		setopenModal(true);
	};
	function onCloseModal() {
		setopenModal(false);
	};
	// ------------------------------------------------------------------------------------------------------------------
	function openMenuSlide() {
		setMenuslide(!Menuslide);
	};

	// ------------------------------------------------------------------------------------------------------------------
	// load profile data
	useEffect(async () => {
		try {
			const get_profile = await get('/auth/profile');
			setProfile({ ...get_profile });
		} catch (error) {
			console.error(error.message);
		}
	}, []);
	return (
		<div className={`nav-left -admin ${Menuslide ? 'openmenu' : 'closemenu'}`}>
			<i className={`fas ${Menuslide ? 'fa-times':'fa-bars'} mw768-menu`} onClick={() => openMenuSlide()}></i>
			<div className="nav-left-label -admin">
				<div className="nav-left-top -admin">
					<div className="admin-logo-head">
						<img src={Images.logo} alt="" />
						<div className="-right">
							<h2>ศูนย์ดำรงธรรม</h2>
							<h5>จังหวัดขอนแก่น</h5>
						</div>
					</div>
					<div className="setFxCen">
						<div className="-cycle">
							<img src={profile ? path + profile.path : Images.Asset30} alt="" />
						</div>
					</div>
					<p>
						{profile && profile.fname} {profile && profile.lname}
					</p>
					<label>{profile && profile.central_name}</label>
					<button onClick={() => onOpenModal()}>แก้ไขข้อมูล</button>
				</div>
				<p style={{ fontWeight: '400' }}>เมนู</p>
				<NavbarToptAdmin />
				<EditProfile onCloseModal={onCloseModal} onOpenModal={openModal} />
			</div>
		</div>
	);
};
export default NavbarLeftAdmin;
