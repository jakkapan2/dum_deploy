import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import NarbarReport from './NarbarReport';
import Images from '../Images';
import user from '../../mobx/user';
import { Noti } from '../Static/adminStatic';
import { post } from '../../api';
import USER from '../../mobx/user';

const NavbarHeadAdmin = ({
	location,
	userRole,
	menuNav,
	history,
	dropNoti,
	openDrop,
	notiDetail,
	amount_noti,
	check_noti
}) => {
	let [ path, setPath ] = useState('/');
	let { pathname } = location;

	let title =
		pathname === '/adminreport'
			? 'รายการร้องเรียน'
			: pathname === '/thing2'
				? 'หน่วยงานและการถูกร้องเรียน'
				: pathname === '/admin/reportsummary'
					? 'สรุปผลการดำเนินการ'
					: pathname === '/admin/news'
						? 'ข่าวสาร'
						: pathname === '/admin/hotline'
							? 'สายด่วน'
							: pathname === '/admin/question'
								? 'แนวทางแก้ทุกข์'
								: pathname === '/admin/rating'
									? 'การประเมินความพึงพอใจ'
									: pathname === '/admin/ipaddress' ? 'ค้นหาตาม IP Address' : null;

	return (
		<React.Fragment>
			<div className="nav-head-admin">
				<div className="-left">{title}</div>
				<div className="-right">
					<div className="nav-head-right-radius -noti" onClick={dropNoti}>
						{amount_noti.length !== 0 ? (
							<div className="noti-alert">
								<p>{check_noti === true && '!'}</p>
							</div>
						) : null}
						<img src={Images.Noti} alt="" />
					</div>
					{openDrop ? (
						<React.Fragment>
							<div className="dropdownNoti styleScroll">
								{notiDetail.length == 0 ? (
									<div className="layerNotNoti">
										<p>ไม่มีการตอบกลับ</p>
									</div>
								) : (
									notiDetail.map((el, i) => {
										// console.log(Noti.length);
										return (
											<div
												className={`layerNoti ${el.read === 0 ? 'even' : ''}`}
												key={i}
												onClick={async () => {
													if (USER.role === 'ADMIN') {
														await post('/form/update_noti', {
															id: el.id,
															key: el.key
														});
														history.push({
															pathname: '/admindepartcompaint',
															state: { id: Number(el.form_id) }
														});
														// dropNoti();
														window.location.reload();
													} else {
														await post('/form/update_noti', {
															id: el.id,
															key: el.key
														});
														history.push({
															pathname: '/subadmindepartcomp',
															state: { id: Number(el.form_id) }
														});
														// dropNoti();
														window.location.reload();
													}
												}}
											>
												<h4>{el.name}</h4>
												<p>
													{el.key}
													{el.status && ' : ' + el.status}
													{/* {el.central_name && ' : ' + el.central_name} */}
												</p>
												<p>รหัสเรื่องร้องเรียน {el.DKK}</p>
												<span>{el.date}</span>
											</div>
										);
									})
								)}
							</div>
							<div className="bgDrop" onClick={dropNoti} />
						</React.Fragment>
					) : null}
					<div
						className="-logout"
						onClick={async () => {
							await post('/auth/logout', { noti_token: user.token });
							history.push('/login');
							user.removeUser();
						}}
					>
						<span>ออกจากระบบ</span>
						<i className="fas fa-sign-out-alt" />
					</div>
				</div>
			</div>
			{/* ---------------------------------------- */}
		</React.Fragment>
	);
};
export default withRouter(NavbarHeadAdmin);
