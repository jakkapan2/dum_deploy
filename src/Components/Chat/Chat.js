import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { PanelPopup, BlockPopup, DetailPopup, NamePopup } from '../../Asset/styled'
import Images from '../Images'
import { observer } from "mobx-react"
import Message from '../../mobx/message'
import { path } from '../../api';
import { Button, LabelBT } from '../../Asset/styleAdmin';
import { Img } from '../../Components/Image/Img';
import { InputFormGroup } from '../Form/Input';
import LoadingSend from '../../Components/Load/LoadingSend'
import moment from 'moment';
import 'moment/locale/th';
moment.locale('th');


const Chat = ({
    className,
    messagesEnd,
    message,
    set_message,
    chat_customer,
    uploadImg_chat_customer,
    uploadFile_chat_customer,
    imgnamechat_customer,
    filenamechat_customer,
    DelImg_chat_customer,
    DelFile_chat_customer,
    insert_customer_chat,
    onEnter_customer,
    openPic,
    user_profile,
    loadingsend,
    loadingpic
}) => {
    let user_name = user_profile.fname + ' ' + user_profile.lname;
    return (
        <PanelPopup className={className}>
            <label id='close' onClick={Message.onShowChat}><i className="fas fa-times"></i></label>
            <p style={{ textAlign: 'center' }} >การตอบกลับเจ้าหน้าที่</p>
            <BlockPopup>
                <div className="chat-room-user styleScroll">
                    <div>
                        {chat_customer.length !== 0 ?
                            chat_customer.sort((a, b) => moment(a.timestamp) - moment(b.timestamp)).map((e) => (
                                <div>
                                    <div className="-chat-profile">
                                        <Img
                                            w={'40px'}
                                            h={'40px'}
                                            src={
                                                e.profile_pic == 'null' || e.profile_pic == null ? (
                                                    Images.Asset4
                                                ) : (
                                                        path + '/static/' + e.profile_pic
                                                    )
                                            }
                                        />
                                        <div>
                                            <div >
                                                <strong>{e.name}</strong>
                                                <span>{user_name === e.name ? 'ผู้ร้องทุกข์' : 'เจ้าหน้าที่'}</span>
                                            </div>
                                            <text>{moment(e.timestamp).add(543, 'years').format('DD/MM/YYYY เวลา HH:mm น.')}</text>
                                        </div>
                                    </div>
                                    <div className="-chat-text">
                                        <div className="chat-room-file">
                                            <div>
                                                {e.chat_file.map((el) => (
                                                    el.split('.')[1] === 'pdf' ? null : (
                                                        <div>
                                                            <img src={path + '/static/' + el} onClick={() => openPic(path + '/static/' + el)} />
                                                        </div>
                                                    )
                                                ))}
                                            </div>
                                            <div>
                                                {e.chat_file.map((el) => (
                                                    el.split('.')[1] === 'pdf' ? (
                                                        <a className={`list depart-file pdf`} href={`${path}/static/${el}`} target="_blank">
                                                            <div>{el}</div>
                                                        </a>
                                                    ) : null
                                                ))}
                                            </div>
                                        </div>
                                        <text className="text-align-left">{e.message}</text>
                                    </div>
                                </div>
                            )) :
                            <div className="h1 d-flex justify-content-center align-items-center"> โบกมือทักทาย </div>
                        }
                        <div>
                            <div className="chat-room-uploadfile">
                                <div>
                                    {imgnamechat_customer.length > 0 &&
                                        imgnamechat_customer.map((e, i) => (
                                            <div>
                                                <img src={e} />
                                                {imgnamechat_customer.length > 0 && loadingsend === true ?
                                                    <div>
                                                        <span className="d-flex w-100 h-100" style={{ background: '#ffffff50', position: 'absolute', top: '0' }} /> 
                                                        <LoadingSend w={'25px'} h={'25px'} r={'calc(50% - 12px)'} t={'calc(50% - 12px)'} />
                                                    </div>
                                                    : <i className="far fa-times-circle" onClick={() => DelImg_chat_customer(i)} />}
                                            </div>
                                        ))}

                                </div>
                                {filenamechat_customer.length > 0 && (
                                    <div>
                                        {filenamechat_customer.map((e, i) => (
                                            <div style={{ width: '325px' }}
                                                className={`list depart-file ${e.match(/\.(png|jpg|jpeg)$/gi) ? 'jpg' : 'pdf'}`}
                                            >
                                                <div>{e}</div>
                                                {filenamechat_customer.length > 0 && loadingsend === true ?
                                                    <LoadingSend w={'20px'} h={'20px'} r={'5px'} t={'calc(50% - 10px)'} />
                                                    : <i className="far fa-times-circle" onClick={() => DelFile_chat_customer(i)} />}
                                            </div>
                                        ))}

                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="onBottom" ref={(el) => messagesEnd(el)}> </div>
                </div>
                <div className="-chat-send">
                    <div className="d-flex w-100 mt-3 justify-content-around">
                        <LabelBT
                            bg="transparent"
                            bgH="transparent"
                            c="#000"
                            bord="1px solid #ccc"
                            mx="0px"
                            className="file-upload jpg"
                        >
                            <input
                                type="file"
                                accept="image/x-png,image/jpeg"
                                multiple
                                onChange={uploadImg_chat_customer}
                            />
                            &nbsp;&nbsp;&nbsp; อัพโหลดรูปภาพ
					</LabelBT>
                        <LabelBT
                            bg="transparent"
                            bgH="transparent"
                            c="#000"
                            bord="1px solid #ccc"
                            className="file-upload pdf"
                        >
                            <input type="file" accept="application/pdf" multiple onChange={uploadFile_chat_customer} />
                            <input type="file" accept="application/pdf" multiple />
                            &nbsp;&nbsp;&nbsp; อัพโหลดเอกสาร
					</LabelBT>
                    </div>
                    <div>
                        <InputFormGroup
                            value={message}
                            onChange={(e) => set_message(e.target.value)}
                            placeholder="กรอกข้อมูล"
                            onKeyDown={onEnter_customer}
                            className={'w-70'}
                            r={'20px'}
                        />
                        <Button my={'1rem'} py={'.5rem'} onClick={insert_customer_chat}>ส่ง</Button>
                    </div>
                </div>
            </BlockPopup>
        </PanelPopup>
    )
}
export default Chat