import React, { Component } from 'react';
import TextHead from '../Components/TextHead';
import { PanelRight, PanelContent, PanelHead, PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { HeadBtn, HeadLeft, Bgblue, Contents, ContentsLeft, ContentsRight, Button } from '../Asset/styleAdmin';
import { post, path } from '../api';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import swal from 'sweetalert';
import Swal from 'sweetalert2'
import LoaderSaving from '../Components/Load/LoaderSaving';
import Modal from 'react-responsive-modal';
import { Carousel } from "react-responsive-carousel"
import 'moment/locale/th';
import moment from 'moment';
moment.locale('th');

export default class AdminNewsEdit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			img_core: '',
			Images: [],
			Images_core: [],
			Images_core_id: null,
			editorState: EditorState.createEmpty(),
			title: '',
			news_id: null,
			deletePic: [],
			loading: false,
			openModal: false,
			ImgPreview: [],
			slide: 0,
			id: null
		};
	}
	// ------------------------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		let id = this.props.match.params.id;
		this.setState({ id })
		if (id !== -1) {
			this.fetchNews(id);
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	fetchNews = async (id) => {
		try {
			const news = await post(`/news/read`, { news_id: id });
			// set news id
			this.setState({ news_id: news.news_id, title: news.title });
			// set img
			news.images.forEach((e, i) => {
				let { Images, ImgPreview } = this.state;
				let obj = {
					name: e.path.split('/')[2],
					pic_id: e.news_pic_id
				};
				Images.push(obj);
				this.setState({ Images });
				ImgPreview.push(path + news.images[i].path)
				this.setState({ ImgPreview });
			});
			this.setState(
				{ news: news, img_core: path + news.images[0].path, Images_core_id: news.images[0].news_pic_id },
				() => {
					let { Images } = this.state;
					Images.splice(0, 1);
					this.setState({ Images });
				}
			);
			// set html to draft
			const { contentBlocks, entityMap } = htmlToDraft(news.detail);
			const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
			this.setState({ editorState: EditorState.createWithContent(contentState) });
		} catch (error) { }
	};
	_onChangeText = (e) => this.setState({ [e.target.name]: e.target.value });
	// -----------------------------------------------------------------------------------------------------------------------
	onEditorStateChange = (editorState) => {
		this.setState({
			editorState
		});
	};
	// -----------------------------------------------------------------------------------------------------------------------
	uploadImg_core = (event) => {
		let { deletePic, Images_core_id } = this.state;
		let Images_core = [];
		Images_core.push(event.target.files[0]);
		// console.log('Images[0].news_pic_id', Images_core_id)
		deletePic.push(Images_core_id);
		this.setState({ Images_core });
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {

				this.setState({ img_core: e.target.result });
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	//-----------------------------------------------------------------------------------------------
	uploadImg = (event) => {
		Object.keys(event.target.files).forEach((key) => {
			let { Images } = this.state;
			Images.push(event.target.files[key]);
			this.setState({ Images });
			let reader = new FileReader();
			reader.onload = (e) => {
				let { ImgPreview } = this.state
				ImgPreview.push(e.target.result)
				this.setState({ ImgPreview });
			};
			reader.readAsDataURL(event.target.files[key]);
		});
	};
	//-----------------------------------------------------------------------------------------------
	delImages = (i, pic_id) => {
		let { Images, deletePic, ImgPreview } = this.state;
		deletePic.push(pic_id);
		Images.splice(i, 1);
		ImgPreview.splice(i, 1);
		this.setState({ Images, deletePic, ImgPreview });
		// console.log('>>>>', pic_id);
	};
	//-----------------------------------------------------------------------------------------------
	Submit = async () => {
		const formData = new FormData();
		let { Images, Images_core, title, editorState, img_core } = this.state;
		//
		let obj = {
			title: this.state.title,
			detail: this.state.editorState,
			Images: Images_core.concat(Images),
			date: ''
		};
		formData.append('title', obj.title);
		formData.append('detail', draftToHtml(convertToRaw(obj.detail.getCurrentContent())));
		obj.Images.forEach((element) => {
			formData.append('file', element);
		});
		// for (let value of formData.values()) {
		// 	console.log('formData', value);
		// }
		console.log('Images_core',img_core)
		if (!img_core) {
			swal({
				title: 'กรุณาเพิ่มรูปหลัก',
				// text: 'แก้ไขข่าวสำเร็จ',
				icon: 'warning'
			})
		} else if (!title || !editorState) {
			swal({
				title: 'กรุณากรอกข้อมูลให้ครบ',
				// text: 'แก้ไขข่าวสำเร็จ',
				icon: 'warning'
			})
		} else {
			if (this.state.news_id) {
				formData.append('deletePic', JSON.stringify(this.state.deletePic));
				formData.append('news_id', this.state.news_id);

				Swal.fire({
					title: 'แก้ไข',
					text: 'คุณต้องการบันทึกการแก้ไขใช่หรือไม่',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'ใช่',
					cancelButtonText: 'ไม่'
				}).then(async (result) => {
					if (result.value) {
						// set loading
						this.setState({ loading: true });
						try {
							const item = await post('/news/update', formData, true);
							if (item) {
								// set loading
								this.setState({ loading: false });
								swal({
									title: 'เรียบร้อย',
									text: 'เพิ่มสำเร็จ',
									icon: 'success'
								}).then(() => {
									this.props.history.goBack();
								});
							}
						} catch (error) {
							swal({
								title: error,
								// text: 'แก้ไขข่าวสำเร็จ',
								icon: 'danger'
							})
						}
					}
				})
			} else {
				Swal.fire({
					title: 'บันทึก',
					text: 'คุณต้องการบันทึกใช่หรือไม่',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'ใช่',
					cancelButtonText: 'ไม่'
				}).then(async (result) => {
					if (result.value) {
						// set loading
						this.setState({ loading: true });
						try {
							const item = await post('/news/insert', formData, true);
							if (item) {
								// set loading
								this.setState({ loading: false });
								swal({
									title: 'เรียบร้อย',
									text: 'เพิ่มข่าวสำเร็จ',
									icon: 'success'
								}).then(() => {
									this.props.history.goBack();
								});
							}
						} catch (error) {
							swal({
								title: error,
								// text: 'แก้ไขข่าวสำเร็จ',
								icon: 'danger'
							})
						}
					}
				})
			}
		}
	}

	onDelete = async () => {
		Swal.fire({
			title: "คุณต้องการลบข่าวใช่หรือไม่",
			// text: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		})
			.then(async (result) => {
				if (result.value) {
					const item = await post('/news/delete', { news_id: this.state.news_id })
					if (item) {
						swal(
							'ลบสำเร็จ!',
							'ข่าวได้ถูกลบแล้ว',
							'success'
						)
							.then(() => {
								this.props.history.goBack()
							});
					}
				}
			})
	}
	// ------------------------------------------------------------------------------------------------------------------
	onOpenModal = () => {
		this.setState({ openModal: true });
	};
	onCloseModal = () => {
		this.setState({ openModal: false });
	};
	// ------------------------------------------------------------------------------------------------------------------
	pickpic(e) {
		this.setState({ slide: -624 * e })
	}
	// ------------------------------------------------------------------------------------------------------------------
	render() {
		let { img_core, Images, editorState, news_id } = this.state;
		return (
			<PanelRight>
				<Bgblue />
				<PanelContent>
					{/* ------------------------- */}
					<PanelHead>
						<HeadLeft>
							<HeadBtn
								onClick={() => {
									this.props.history.goBack();
								}}
								bgAct="#5DC0EC"
							>
								กลับ
							</HeadBtn>
						</HeadLeft>
					</PanelHead>
					{/* ------------------------- */}
					<PanelWhi>
						{this.state.loading ? <LoaderSaving /> : ''}
						{/* <TextHead title={this.props.title} /> */}
						<TextHead title="รายละเอียดข่าวสาร" />
						<Button bg="#29A4DB" bgH="#0e79a9" style={{ position: 'absolute', top: '20px', right: '10px' }} onClick={this.onOpenModal}>
							Preview
						</Button>
						<Contents>
							<ContentsLeft>
								<div className="core-img">
									<label className="custom-file-upload">
										<input
											type="file"
											onChange={this.uploadImg_core}
											accept="image/x-png,image/jpeg"
										/>
										อัพโหลดรูปภาพ
									</label>
									{img_core !== '' && (
										<div>
											<img src={img_core} alt="" />
										</div>
									)}
								</div>
								<div className="second-display">
									<div style={{ fontSize: 'small' }}>ไฟล์ภาพเพิ่มเติม</div>
									<label className="custom-file-upload-2">
										<input
											type="file"
											onChange={this.uploadImg}
											accept="image/x-png,image/jpeg"
											multiple
										/>
										อัพโหลดรูปภาพ
									</label>
								</div>
								<div className="second-display-list">
									{Images.length !== 0 &&
										Images.map((e, i) => (
											<div className="list">
												<div>{e.name}</div>
												<div className="close" onClick={() => this.delImages(i, e.pic_id)}>
													x
												</div>
											</div>
										))}
								</div>
							</ContentsLeft>
							<ContentsRight flexDirection="column" style={{ textAlign: 'left', fontWeight: '300' }}>
								{/* <InputFormGroup title="หัวข้อข่าว" /> */}
								<div className="form-group">
									<label for="">หัวข้อข่าว</label>
									<input
										type="text"
										className="form-control"
										name="title"
										value={this.state.title}
										onChange={this._onChangeText}
									/>
								</div>
								<div className="form-group">
									<label for="">เนื้อหาข่าว</label>
									<Editor
										wrapperClassName="wrapper-class"
										editorClassName="editor-class"
										toolbarClassName="toolbar-class"
										toolbar={{
											options: ['inline', 'textAlign'],
											inline: {
												options: ['bold', 'italic', 'underline'],
												bold: { className: 'bordered-option-classname' },
												italic: { className: 'bordered-option-classname' },
												underline: { className: 'bordered-option-classname' }
											},
											textAlign: {
												visible: true
											}
										}}
										editorState={editorState}
										onEditorStateChange={this.onEditorStateChange}
									/>
								</div>
								{/* <div dangerouslySetInnerHTML={{ __html: this.state.cc }} /> */}
								{/* <textarea
									disabled
								value={draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))}
								/> */}
							</ContentsRight>
							<div className="d-flex flex-row w-100 justify-content-center">
								{news_id && <Button bg="#ff3300" bgH="#e23105" px="4rem" onClick={this.onDelete}>ลบข่าว</Button>}
								{news_id && <Button onClick={this.Submit}>บันทึกการแก้ไข</Button>}
								{!news_id && <Button onClick={this.Submit}>เพิ่มข่าว</Button>}
							</div>
						</Contents>
					</PanelWhi>
					{/* ------------------------- */}
					<Modal
						open={this.state.openModal}
						onClose={this.onCloseModal}
						little
						classNames={{ modal: 'Look-modal ChangeStatus ' }}
					>
						<div>
							{this.renderImage()}
							<div className='-newsDetail-70-text' style={{ width: '750px' }}>
								<h1>{this.state.title}</h1>
								<span>{moment().format('DD/MM/YYYY')}</span>
								<div dangerouslySetInnerHTML={{ __html: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())) }} />
							</div>
						</div>
					</Modal>

				</PanelContent>
			</PanelRight>
		);
	}
	renderImage() {
		let { ImgPreview, img_core, id } = this.state
		return (
			<div className="news-carousel">
				<div>
					<div style={{ left: this.state.slide }}>
						{img_core && <img src={img_core} />}
						{ImgPreview.slice(id === -1 || id === '-1' ? 0 : 1).map((e) =>
							<img src={e} />
						)}
					</div>
				</div>
				<div className="styleScroll">
					<div>
						{img_core && <a><img src={img_core} onClick={() => this.pickpic(0)} /></a>}
						{ImgPreview.slice(id === -1 || id === '-1' ? 0 : 1).map((e, i) =>
							<a><img src={e} onClick={() => this.pickpic(i + 1)} /></a>
						)}
					</div>
				</div>
			</div>
		)
	}
}
