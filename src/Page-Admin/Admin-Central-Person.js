import React, { Component } from 'react';
import Images from '../Components/Images';
import { PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { get } from '../api';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Search } from '../Components/Search/Search';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class AdminPerson extends Component {
	constructor(props) {
		super(props);
		this.state = {
			Item: null
		};
	}
	componentDidMount = () => {
		this.fetchItem();
	};
	// -----------------------------------------------------------------------------------------------------------------------
	fetchItem = async () => {
		this.setState({ loadingSave: true });
		const item = await get('/complaint/person');
		console.log('item', item);
		this.setState({
			Item: item.map((e) => ({
				name: !e.name ? '-' : e.name,
				phone: !e.phone ? '-' : e.phone,
				sub_district: !e.sub_district ? '-' : e.sub_district,
				district: !e.district ? '-' : e.district,
				province: !e.province ? '-' : e.province
			})),
			oldItem: item,
			loadingSave: false
		});
		// console.log('sdsd>>>', item)
	};

	// -----------------------------------------------------------------------------------------------------------------------
	fullname = (cell, row) => {
		//   alert(row)
		return (
			<p>
				{row.name} &nbsp; {row.lname}
			</p>
		);
	};
	// -----------------------------------------------------------------------------------------------------------------------
	searchItem = async (e) => {
		const texts = e.target.value;
		const item = await get('/complaint/person');
		let res = item
			.map((e) => ({
				name: !e.name ? '-' : e.name,
				phone: !e.phone ? '-' : e.phone,
				sub_district: !e.sub_district ? '-' : e.sub_district,
				district: !e.district ? '-' : e.district,
				province: !e.province ? '-' : e.province
			}))
			.filter(
				(e, i) =>
					String(e.name).toLowerCase().indexOf(texts) > -1 ||
					String(e.phone).toLowerCase().indexOf(texts) > -1 ||
					String(e.sub_district).toLowerCase().indexOf(texts) > -1 ||
					String(e.district).toLowerCase().indexOf(texts) > -1 ||
					String(e.province).toLowerCase().indexOf(texts) > -1
			);
		this.setState({
			Item: res
		});
	};
	// -----------------------------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------------------------------------
	render() {
		const { Item } = this.state;
		const optionsTable = {
			paginationSize: 3,
			sizePerPageList: [ 10 ],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			noDataText: 'ไม่พบข้อมูล'
		};
		return (
			<div>
				{/* ------------------------- */}
				<PanelWhi>
					{/* ----------search ----------- */}
					<Search
						title="ค้นหาชื่อผู้ถูกร้อง"
						placeholder="ค้นหา"
						onChange={this.searchItem}
						classNameDiv={'mb-5'}
					/>
					{Item ? (
						<div id="my-tableHeader-class-text-left">
							<BootstrapTable
								data={Item}
								pagination={true}
								options={optionsTable}
								// hover
								containerClass="my-container-class tb-setSearch"
								tableContainerClass="my-tableContainer-class"
								tableHeaderClass="my-tableHeader-class colorBlue"
								// tableBodyClass='my-tableBody-pointer-class'
							>
								<TableHeaderColumn dataField="name" dataFormat={this.fullname} isKey width="35%">
									ชื่อผู้ถูกร้อง
								</TableHeaderColumn>
								<TableHeaderColumn dataField="phone" width="20%">
									หมายเลขโทรศัพท์(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="sub_district" width="15%">
									แขวง/ตำบล(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="district" width="15%">
									อำเภอ(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="province" width="15%">
									จังหวัด(ถ้ามี)
								</TableHeaderColumn>
							</BootstrapTable>
						</div>
					) : this.state.loadingSave ? (
						<div style={{ position: 'relative', height: '300px' }}>
							<LoaderSaving />
						</div>
					) : (
						''
					)}
				</PanelWhi>
			</div>
		);
	}
}
