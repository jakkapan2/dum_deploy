import React, { Component } from 'react';
import Images from '../Components/Images';
import { PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { get } from '../api';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Search } from '../Components/Search/Search';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class AdminCompanyAndOther extends Component {
	constructor(props) {
		super(props);
		this.state = {
			Item: null,
			oldItem: []
		};
	}
	componentDidMount = () => {
		this.fetchItem();
	};
	// -----------------------------------------------------------------------------------------------------------------------
	fetchItem = async () => {
		this.setState({ loadingSave: true });
		const item = await get('/complaint/company_and_other');
		// console.log('item', item);
		this.setState({
			Item: item.map((e) => ({
				name: !e.name ? '-' : e.name,
				kind: e.kind,
				phone: !e.phone ? '-' : e.phone,
				sub_district: !e.sub_district ? '-' : e.sub_district,
				district: !e.district ? '-' : e.district,
				province: !e.province ? '-' : e.province
			})),
			oldItem: item,
			loadingSave: false
		});
	};

	// -----------------------------------------------------------------------------------------------------------------------
	searchItem = async (e) => {
		const texts = e.target.value;
		const item = await get('/complaint/company_and_other');
		let res = item
			.map((e) => ({
				name: !e.name ? '-' : e.name,
				kind: e.kind,
				phone: !e.phone ? '-' : e.phone,
				sub_district: !e.sub_district ? '-' : e.sub_district,
				district: !e.district ? '-' : e.district,
				province: !e.province ? '-' : e.province
			}))
			.filter((e, i) => String(e.name).toLowerCase().indexOf(texts) > -1);
		this.setState({
			Item: res
		});
	};
	// -----------------------------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------------------------------------
	render() {
		const { Item } = this.state;
		const optionsTable = {
			paginationSize: 3,
			sizePerPageList: [ 10 ],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			noDataText: 'ไม่พบข้อมูล'
		};
		return (
			<div>
				{/* ------------------------- */}
				<PanelWhi>
					{/* ----------search ----------- */}
					<Search
						title="ค้นหานิติบุคคลผู้ถูกร้อง"
						placeholder="ค้นหาชื่อบริษัท/ห้าง/ร้านที่ถูกร้อง"
						onChange={this.searchItem}
						classNameDiv={'mb-5'}
					/>
					{Item ? (
						<div id="my-tableHeader-class-text-left">
							<BootstrapTable
								data={Item}
								pagination={true}
								options={optionsTable}
								// hover
								containerClass="my-container-class tb-setSearch"
								tableContainerClass="my-tableContainer-class"
								tableHeaderClass="my-tableHeader-class colorBlue"
								// tableBodyClass='my-tableBody-pointer-class'
							>
								<TableHeaderColumn dataField="name" isKey width="29%">
									ชื่อบริษัท/ห้าง/ร้านที่ถูกร้อง
								</TableHeaderColumn>
								<TableHeaderColumn dataField="kind" width="20%">
									ประเภท
								</TableHeaderColumn>
								<TableHeaderColumn dataField="phone" width="15%">
									หมายเลขโทรศัพท์(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="sub_district" width="12%">
									แขวง/ตำบล(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="district" width="12%">
									อำเภอ(ถ้ามี)
								</TableHeaderColumn>
								<TableHeaderColumn dataField="province" width="12%">
									จังหวัด(ถ้ามี)
								</TableHeaderColumn>
							</BootstrapTable>
						</div>
					) : this.state.loadingSave ? (
						<div style={{ position: 'relative', height: '300px' }}>
							<LoaderSaving />
						</div>
					) : (
						''
					)}
				</PanelWhi>
			</div>
		);
	}
}
