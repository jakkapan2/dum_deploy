import React, { Component } from 'react';
// import Main from '../Components/Main';
// import { Link, withRouter } from 'react-router-dom';
import TextHead from '../Components/TextHead';
// import { Tabs, Tab } from 'react-bootstrap';
// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Images from '../Components/Images';
import { PanelRight, PanelContent, PanelHead, PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { HeadLeft, Bgblue } from '../Asset/styleAdmin';
// import { Noti } from '../Components/Static/adminStatic';
// import moment from 'moment';
import { get, post } from '../api';
import { Label } from '../Asset/styled';
import '../Asset/css/adminAddCompaintStyle.css';
import HeadStep from '../Components/AddComplaint/HeadStep';
import Step1 from '../Components/AddComplaint/Step1';
import Step2 from '../Components/AddComplaint/Step2';
// import Step3 from '../Components/AddComplaint/Step3';
import Step4 from '../Components/AddComplaint/Step4';
// import Step5 from '../Components/AddComplaint/Step5';
import Step6 from '../Components/AddComplaint/Step6';
import Step7 from '../Components/AddComplaint/Step7';
import { Row, Col } from 'reactstrap';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import LoaderSaving from '../Components/Load/LoaderSaving';
import {
	complaint,
	headComplaint,
	sub_complaint,
	complaintToRight,
	complaintToCenter,
	selectComplaint
} from '../Components/Static/static';
import {
	InputForm,
	RadioForm,
	RadioFormGroup,
	SelectFormGroup,
	InputRow,
	Textarea,
	InputIdentifi,
	InputID,
	SelectName,
	RadioeDePart,
	InputFormGroup
} from '../Components/Form/Input';
import { RadioCol } from '../Components/Form/InputAdmin';
import InputMask from 'react-input-mask';
import { Selecter, SelecterPerson, SelecterCompany } from '../Components/Form/SelectProvince';
import UploadFileAddmin from '../Components/Form/UploadFileAddmin';
import _ from 'lodash';
import USER from '../mobx/user';

export default class SubAdminAddCompaint extends Component {
	constructor(props) {
		super(props);

		this.state = {
			step: 0,
			disableNext: false,
			step_success: [ false, false, false, false, false, false, false ],
			// ----------------------
			type: 1,
			disclose: 8,
			select_complaintTo: 10,
			complaint_channel: 'เว็บไซต์',
			id_card: null,
			// ----------------------
			activecomplaint: false,
			activeleft: true,
			activeright: false,
			// ----------------------
			typeItem: 'depart',
			departmentComplaint: [ { data: [] } ],
			// ----------------------
			fileImgList: [],
			filePdfList: [],
			typeStatus: 0,
			// ----------------------
			complaint: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			// ----------------------
			center: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			// ----------------------
			right: {
				province: '',
				district: '',
				tumbon: '',
				zipcode: ''
			},
			// ----------------------
			province: '',
			district: '',
			sub_district: '',
			zipcode: '',
			//---------------------------
			location: {
				coords: {
					latitude: 16.439625,
					longitude: 102.828728,
					location_name: ''
				},
				markerCoords: {
					lat: 16.439625,
					lng: 102.828728
				}
			},
			companyTypes: 1
		};
	}
	componentDidMount = () => {
		this._fetchDepartmentAndCompanyType();
	};
	// ------------------------------------------------------------------------------------
	_fetchDepartmentAndCompanyType = async () => {
		try {
			const res = await get('/department/all');
			const companyType = await get('/form/company_type');
			this.setState({
				departmentComplaint: [
					{
						ref: 'department_name',
						title: 'หน่วยงานที่ถูกร้อง',
						type: 'input',
						placeholder: 'กรุณากรอกหน่วยงานที่ถูกร้อง'
					}
				],
				companyType
			});
		} catch (error) {
			console.log(error);
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	updatetypeInput = async (e) => {
		// console.log('e.target.name', e.target.name);
		// console.log('e.target.value', e.target.value);
		if (e.target.name === 'id_card' && e.target.value.length === 17) {
			// console.log('object', e.target.value.replace(/(-)/g, ''));
			this.idcard_info(e.target.value.replace(/(-)/g, ''));
		}
		if (e.target.name === 'id_card' && e.target.value.length < 17) {
			// console.log('object', e.target.value.replace(/(-)/g, ''));
			this.resetID();
		}
		if (e.target.name === 'disclose') {
			if (Number(e.target.value) === 8) {
				this.resetID();
			}
		}
		this.setState({ [e.target.name]: e.target.value });
	};
	idcard_info = async (e) => {
		let res = await post('/form/idcard_info', { id_card: e });
		// console.log('res', res);
		this.setState({
			alleyway: res.alleyway === '' || res.alleyway === null || res.alleyway === 'null' ? '-' : res.alleyway,
			complaint: {
				district: res.district,
				province: res.province,
				tumbon: res.sub_district,
				zipcode: res.zipcode
			},
			email: res.email === '' || res.email === null || res.email === 'null' ? '-' : res.email,
			fname: res.fname,
			house_number: res.house_number,
			lname: res.lname,
			moo: res.moo === '' || res.moo === null || res.moo === 'null' ? '-' : res.moo,
			phone: res.phone,
			prefix: res.prefix,
			road: res.road === '' || res.road === null || res.road === 'null' ? '-' : res.road
		});
	};
	resetID() {
		this.setState({
			id_card: '',
			alleyway: '',
			complaint: {
				district: '',
				province: '',
				tumbon: '',
				zipcode: ''
			},
			email: '',
			fname: '',
			house_number: '',
			lname: '',
			moo: '',
			phone: '',
			prefix: '0',
			road: ''
		});
	}
	updatetypeItem = (event) => {
		this.setState({ typeItem: event.target.value });
	};
	updatetypeStatus = (event) => {
		// console.log('event.target.value', event.target.value);
		this.setState({ typeStatus: event.target.value });
	};
	// ------------------------------------------------------------------------------------
	componentWillUpdate = (prevProps, prevState) => {
		if (prevState.select_complaintTo !== this.state.select_complaintTo) {
			this.setState((prev) => ({ activeleft: !prev.activeleft, activeright: !prev.activeright }));
		} else if (prevState.disclose !== this.state.disclose) {
			this.setState((prev) => ({ activecomplaint: !prev.activecomplaint }));
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	onChange = (ref, value) => {
		// console.log('ref', ref, 'value', value);
		this.setState({ [ref]: value });
	};
	// ------------------------------------------------------------------------------------------------------------------
	_FirstBtn = () => {
		this.setState({ step: 0 });
	};
	// _SecondBtn = () => { this.setState({ step: 1 }) }
	// _ThirdBtn = () => { this.setState({ step: 2 }) }
	// _FourthBtn = () => { this.setState({ step: 3 }) }
	// _FifthBtn = () => { this.setState({ step: 4 }) }
	// _SixthBtn = () => { this.setState({ step: 5 }) }
	// _SeventhBtn = () => { this.setState({ step: 6 }) }
	_SecondBtn = () => {
		if (this.state.step >= 6 || !this.state.step_success[0]) {
		} else {
			this.setState({ step: 1 });
		}
	};
	_ThirdBtn = () => {
		if (this.state.step >= 6 || !this.state.step_success[1]) {
		} else {
			this.setState({ step: 2 });
		}
	};
	_FourthBtn = () => {
		if (this.state.step >= 6 || !this.state.step_success[2]) {
		} else {
			this.setState({ step: 3 });
		}
	};
	_FifthBtn = () => {
		if (this.state.step >= 6 || !this.state.step_success[3]) {
		} else {
			this.setState({ step: 4 });
		}
	};
	_SixthBtn = () => {
		if (this.state.step >= 6 || !this.state.step_success[4]) {
		} else {
			this.setState({ step: 5 });
		}
	};
	_SeventhBtn = () => {
		this.setState({ step: 6 });
	};
	// ------------------------------------------------------------------------------------------------------------------
	resetState = () => {
		this.setState({ step: 0 });
	};
	// ------------------------------------------------------------------------------------------------------------------
	swalCheck = (message) => {
		swal('คำเตือน!', message, 'warning');
	};
	nextstepSuccess = () => {
		let { step } = this.state;
		if (step < 6) {
			this.setState((prevState) => ({
				step_success: [
					...prevState.step_success.slice(0, prevState.step),
					true,
					...prevState.step_success.slice(prevState.step + 1)
				],
				step: prevState.step + 1
			}));
		}
	};
	nextstep = () => {
		let { step } = this.state;
		if (step === 0) {
			// let { subject, type, subtype, moi, complaint_channel, other } = this.state;
			// let type_name = headComplaint[1].data.filter((e) => Number(e.value) === Number(type))[0].label;
			// this.setState({ type_name: type_name });
			// // if (subject === undefined || subject === '') {
			// // 	this.swalCheck('กรุณากรอกหัวข้อร้องทุกข์');
			// // }
			// if (subtype === undefined || subtype === '') {
			// 	this.swalCheck('กรุณากรอกประเภทเรื่องย่อย');
			// 	// } else if (moi === undefined || moi === '') {
			// 	// 	this.swalCheck('กรุณากรอกรหัส MOI');
			// } else if (complaint_channel === 'หน่วยงานอื่น ๆ' && (other === undefined || other === '')) {
			// 	this.swalCheck('กรุณากรอกระบุหน่วยงานอื่น ๆ');
			// } else {
			this.nextstepSuccess();
			// }
		}
		if (step === 1) {
			// console.log('this.sta', this.state);
			let {
				disclose,
				id_card,
				prefix,
				fname,
				lname,
				phone,
				id_line,
				email,
				house_number,
				moo,
				alleyway,
				road,
				complaint
			} = this.state;
			if (disclose === 8 || disclose === '8') {
				this.nextstepSuccess();
			} else {
				// console.log('all', this.state);
				// if (!id_card || id_card === null || id_card === undefined || id_card === '') {
				// 	this.swalCheck('กรุณากรอกเลขที่บัตรประจำตัวประชาชนผู้ร้องทุกข์');
				// 	// console.log('this.state', this.state);
				// }
				if (!prefix || prefix === undefined || prefix === '') {
					this.swalCheck('กรุณากรอกคำนำหน้า');
				} else if (!fname || fname === undefined || fname === '') {
					this.swalCheck('กรุณากรอกชื่อผู้ร้องทุกข์');
				} else if (!lname || lname === undefined || lname === '') {
					this.swalCheck('กรุณากรอกนามสกุลผู้ร้องทุกข์');
					// 	// } else if (!phone || phone === undefined || phone === '') {
					// 	// 	this.swalCheck('กรุณากรอกเบอร์โทรศัพท์ผู้ร้องทุกข์');
					// 	// } else if (!id_line || id_line === undefined || id_line === '') {
					// 	// 	this.swalCheck('กรุณากรอก ID Line ผู้ร้องทุกข์');
					// 	// } else if (!email || email === undefined || email === '') {
					// 	// 	this.swalCheck('กรุณากรอก E-mail ผู้ร้องทุกข์');
					// } else if (!house_number || house_number === undefined || house_number === '') {
					// 	this.swalCheck('กรุณากรอกบ้านเลขที่');
					// 	// } else if (!moo || moo === undefined || moo === '') {
					// 	// 	this.swalCheck('กรุณากรอกหมู่ที่');
					// 	// } else if (!alleyway || alleyway === undefined || alleyway === '') {
					// 	// 	this.swalCheck('กรุณากรอกตรอกซอย');
					// 	// } else if (!road || road === undefined || road === '') {
					// 	// 	this.swalCheck('กรุณากรอกถนน');
					// } else if (complaint.district === undefined || complaint.district === '') {
					// 	this.swalCheck('กรุณากรอกอำเภอ');
					// } else if (complaint.province === undefined || complaint.province === '') {
					// 	// console.log('this.state', this.state);
					// 	this.swalCheck('กรุณากรอกจังหวัด');
					// } else if (complaint.tumbon === undefined || complaint.tumbon === '') {
					// 	this.swalCheck('กรุณากรอกตำบล');
				} else {
					this.nextstepSuccess();
					// console.log('this.state', this.state);
				}
			}
		}
		if (step === 2) {
			let {
				select_complaintTo,
				select_depart,
				companyTypes,
				corporation_name,
				phone_company,
				right,
				center,
				fname_person,
				lname_person,
				phone_person,
				house_number_person,
				moo_person,
				alleyway_person,
				road_person,
				others,
				department_name
			} = this.state;
			// console.log('this.state', this.state);
			if (select_complaintTo === 10 || select_complaintTo === '10') {
				// if (
				// 	!department_name ||
				// 	department_name === null ||
				// 	department_name === undefined ||
				// 	department_name === ''
				// ) {
				// 	this.swalCheck('กรุณากรอกชื่อหน่วยงานที่ถูกร้อง');
				// } else {
				this.nextstepSuccess();
				// }
			}
			if (select_complaintTo === 12 || select_complaintTo === '12') {
				// if (
				// 	!corporation_name ||
				// 	corporation_name === null ||
				// 	corporation_name === undefined ||
				// 	corporation_name === ''
				// ) {
				// 	this.swalCheck('กรุณากรอกชื่อนิติบุคคลหรือบริษัท');
				// }
				if (
					(companyTypes === 4 || companyTypes === '4') &&
					(!others || others === '' || others === null || others === undefined)
				) {
					this.swalCheck('กรุณาระบุอื่น ๆ');
				} else if (right.district === undefined || right.district === '') {
					this.swalCheck('กรุณากรอกจังหวัด');
				} else if (right.province === undefined || right.province === '') {
					this.swalCheck('กรุณากรอกอำเภอ');
				} else if (right.tumbon === undefined || right.tumbon === '') {
					this.swalCheck('กรุณากรอกตำบล');
				} else {
					this.nextstepSuccess();
				}
			}
			if (select_complaintTo === 11 || select_complaintTo === '11') {
				// if (!fname_person || fname_person === null || fname_person === undefined || fname_person === '') {
				// 	this.swalCheck('กรุณากรอกชื่อผู้ถูกร้องทุกข์');
				// }
				if (!lname_person || lname_person === null || lname_person === undefined || lname_person === '') {
					this.swalCheck('กรุณากรอกนามสกุลผู้ถูกร้องทุกข์');
				} else if (
					!house_number_person ||
					house_number_person === null ||
					house_number_person === undefined ||
					house_number_person === ''
				) {
					this.swalCheck('กรุณากรอกบ้านเลขที่');
					// } else if (!moo_person || moo_person === null || moo_person === undefined || moo_person === '') {
					// 	this.swalCheck('กรุณากรอกหมู่ที่');
					// } else if (
					// 	!alleyway_person ||
					// 	alleyway_person === null ||
					// 	alleyway_person === undefined ||
					// 	alleyway_person === ''
					// ) {
					// 	this.swalCheck('กรุณากรอกหมู่ที่');
					// } else if (!road_person || road_person === null || road_person === undefined || road_person === '') {
					// 	this.swalCheck('กรุณากรอกถนน');
				} else if (center.district === undefined || center.district === '') {
					this.swalCheck('กรุณากรอกจังหวัด');
				} else if (center.province === undefined || center.province === '') {
					this.swalCheck('กรุณากรอกอำเภอ');
				} else if (center.tumbon === undefined || center.tumbon === '') {
					this.swalCheck('กรุณากรอกตำบล');
				} else {
					this.nextstepSuccess();
				}
			}
		}
		if (step === 3) {
			// let { location, detail, objective } = this.state;
			// if (location.coords.location_name === '') {
			// 	this.swalCheck('กรุณากรอกสถานที่เกิดเหตุ');
			// } else if (!detail || detail === undefined || detail === '') {
			// 	this.swalCheck('กรุณากรอกรายละเอียด');
			// } else if (!objective || objective === undefined || objective === '') {
			// 	this.swalCheck('กรุณากรอกวัตถุประสงค์');
			// } else {
			this.nextstepSuccess();
			// }
		}
		if (step === 4) {
			this.nextstepSuccess();
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	_handleNextStep = () => {
		let { step } = this.state;
		switch (step) {
			case 0:
				this.nextstep();
				break;
			case 1:
				this.nextstep();
				break;
			case 2:
				this.nextstep();
				break;
			case 3:
				this.nextstep();
				break;
			case 4:
				this.nextstep();
				break;
			case 5:
				this.nextstep();
				break;
			case 6:
				this.resetState();
				break;
			default:
				// alert('err');
				break;
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	_handlePrevStep = () => {
		let { step } = this.state;
		if (step > 0) {
			this.setState((prevState) => ({ step: prevState.step - 1 }));
			// console.log('PrevStep', step);
		} else {
		}
	};
	// -----------------------------------------------------------------------------------------------------------------
	stepController = () => {
		switch (this.state.step) {
			case 0:
				return this.Step1();
			case 1:
				return (
					<Step2
						updatetypeInput={this.updatetypeInput}
						disclose={this.state.disclose}
						value={this.state}
						refRadio={'complaint'}
						activecomplaint={this.state.activecomplaint}
						onChange={this.onChange}
						id_card={this.state.id_card}
					/>
				);
			case 2:
				return this.renderStep3();
			case 3:
				return (
					<Step4
						updatetypeInput={this.updatetypeInput}
						set_mapName={(e) => {
							let { location } = this.state;
							location.coords.latitude = e.lat;
							location.coords.longitude = e.lng;
							location.coords.location_name = e.location_name;
							location.markerCoords.lat = e.lat;
							location.markerCoords.lng = e.lng;
							this.setState({ location });
						}}
						detail={this.state.detail}
						objective={this.state.objective}
						locations={this.state.location}
						onChangelocation_name={(e) => {
							let { location } = this.state;
							location.coords.location_name = e.target.value;
							this.setState({ location });
						}}
					/>
				);
			case 4:
				return (
					<UploadFileAddmin
						handleFileAdd={this.handleFileAdd}
						handleFilePdfAdd={this.handleFilePdfAdd}
						fileImgList={this.state.fileImgList}
						filePdfList={this.state.filePdfList}
						removeFile={this.removeFile}
						removePdfFile={this.removePdfFile}
					/>
				);
			case 5:
				return (
					<Step6
						sayyes={this.state.sayyes}
						ckDetail={this.state}
						agree={this.agree}
						updatetypeStatus={this.updatetypeStatus}
						value={this.state}
						typeStatus={this.state.typeStatus}
						removeFile={this.removeFile}
						removePdfFile={this.removePdfFile}
						set_mapName={(e) => {
							let { location } = this.state;
							location.coords.latitude = e.lat;
							location.coords.longitude = e.lng;
							location.coords.location_name = e.location_name;
							location.markerCoords.lat = e.lat;
							location.markerCoords.lng = e.lng;
							this.setState({ location });
						}}
					/>
				);
			case 6:
				return (
					<Step7
						goTO={() =>
							this.props.history.push({
								pathname: '/admindepartcompaint',
								state: { id: this.state.form_id }
							})}
					/>
				);
			default:
				return <Step1 />;
		}
	};
	// ------------------------------------------------------------------------------------
	handleFileAdd = (event) => {
		if (event.target.files[0].size <= 5999999) {
			let { fileImgList } = this.state;
			let newFile = event.target.files[0];
			let name = event.target.files[0].name;
			if (fileImgList.some((e) => e.name === name)) {
			} else {
				fileImgList.push(newFile);
				this.setState({ fileImgList });
			}
		} else {
			swal('คำเตือน', 'ไฟล์เกินขนาดที่กำหนด', 'warning');
		}
	};
	// ------------------------------------------------------------------------------------
	removeFile = (index) => {
		let { fileImgList } = this.state;
		fileImgList.splice(index, 1);
		this.setState({ fileImgList });
	};
	// ------------------------------------------------------------------------------------
	handleFilePdfAdd = (event) => {
		if (event.target.files[0].size <= 5999999) {
			let { filePdfList } = this.state;
			let newFile = event.target.files[0];
			let name = event.target.files[0].name;
			if (filePdfList.some((e) => e.name === name)) {
			} else {
				filePdfList.push(newFile);
				this.setState({ filePdfList });
			}
		} else {
			swal('คำเตือน', 'ไฟล์เกินขนาดที่กำหนด', 'warning');
		}
	};
	// ------------------------------------------------------------------------------------
	removePdfFile = (index) => {
		let { filePdfList } = this.state;
		filePdfList.splice(index, 1);
		this.setState({ filePdfList });
	};
	// ------------------------------------------------------------------------------------
	agree = (e) => this.setState((prev) => ({ sayyes: !prev.sayyes }));
	// ------------------------------------------------------------------------------------
	// address = () => { ({ ..._.pick(['house_number', 'moo', 'alleyway', 'road']), ..._.pick(['sub_district', 'district', 'province', 'zipcode']) }) }
	// ------------------------------------------------------------------------------------
	_onSentComplaint = async () => {
		// console.log('this.state', this.state);
		let { sayyes } = this.state;
		if (!sayyes || sayyes === undefined || sayyes === '' || sayyes === false) {
			this.swalCheck('กรุณากดยอมรับข้อตกลง');
		} else {
			Swal.fire({
				title: 'ยืนยัน?',
				text: 'ต้องการดำเนินการต่อหรือไม่!',
				icon: 'warning',
				buttons: true,
				dangerMode: true
			}).then(async (result) => {
				if (result.value) {
					let {
						subject,
						type,
						type_name,
						subtype,
						complaint_channel,
						moi,
						//----------------------------
						disclose,
						id_card,
						prefix,
						fname,
						lname,
						phone,
						id_line,
						email,
						house_number,
						moo,
						alleyway,
						road,
						complaint,
						///--------------------------
						select_complaintTo,
						select_depart,
						companyTypes,
						corporation_name,
						phone_company,
						right,
						center,
						fname_person,
						lname_person,
						phone_person,
						house_number_person,
						moo_person,
						alleyway_person,
						road_person,
						others,
						location,
						detail,
						objective,
						typeStatus,
						//---------------------------
						departmentComplaint,
						companyType,
						other,
						fileImgList,
						filePdfList,
						department_name
					} = this.state;
					let obj = {
						subject,
						type: headComplaint[1].data.filter((e) => Number(e.value) === Number(type))[0].label,
						sub_type: subtype,
						complaint_channel: complaint_channel === 'ช่องทางอื่น ๆ' ? other : complaint_channel,
						moi,
						disclose: disclose === 8 || disclose === '8' ? false : true,
						id_card:
							disclose === 9 || disclose === '9'
								? String(
										id_card.split('-')[0] +
											id_card.split('-')[1] +
											id_card.split('-')[2] +
											id_card.split('-')[3] +
											id_card.split('-')[4]
									)
								: undefined,
						prefix: prefix,
						fname: fname,
						lname: lname,
						phone: phone,
						id_line: id_line,
						email,
						house_number,
						moo: moo,
						alleyway,
						road,
						province: complaint.province,
						district: complaint.district,
						sub_district: complaint.tumbon,
						zipcode: complaint.zipcode,
						complainant:
							select_complaintTo === 10 || select_complaintTo === '10'
								? 'DEPARTMENT'
								: select_complaintTo === 11 || select_complaintTo === '11' ? 'PERSON' : 'COMPANY',
						department_name:
							select_complaintTo === 10 || select_complaintTo === '10' ? department_name : undefined,
						name_company: corporation_name,
						phone_company,
						type_company:
							companyTypes === 4 || companyTypes === '4'
								? others
								: companyType[Number(companyTypes) - 1].type_name,
						province_company: right.province,
						district_company: right.district,
						sub_district_company: right.tumbon,
						zipcode_company: right.zipcode,
						fname_person,
						lname_person,
						phone_person,
						house_number_person,
						moo_person,
						alleyway_person,
						road_person,
						province_person: center.province,
						district_person: center.district,
						sub_district_person: center.tumbon,
						zipcode_person: center.zipcode,
						location_name: location.coords.location_name,
						lat: location.coords.latitude,
						lng: location.coords.longitude,
						detail,
						objective,
						status: typeStatus
					};
					// console.log('obj', obj);
					try {
						this.setState({ loadingSave: true });
						let res = await post('/form/insert_by_admin', obj);
						const formData = new FormData();
						formData.append('form_id', res.form_id);
						fileImgList.concat(filePdfList).forEach(async (e) => {
							await formData.append('files', e);
						});
						await post('/form/insert/evidence', formData, true);
						this.setState({ step: 6, form_id: res.form_id });
						this.setState({ loadingSave: false });
					} catch (error) {
						swal('Error', error, 'error');
						this.setState({ loadingSave: false });
					}
				}
			});
		}
	};
	render() {
		const { step } = this.state;
		return (
			<PanelRight>
				<Bgblue />
				<PanelContent>
					{/* ------------------------- */}
					<PanelHead>
						<HeadLeft>
							<HeadStep
								step={step}
								_FirstBtn={this._FirstBtn}
								_SecondBtn={this._SecondBtn}
								_ThirdBtn={this._ThirdBtn}
								_FourthBtn={this._FourthBtn}
								_FifthBtn={this._FifthBtn}
								_SixthBtn={this._SixthBtn}
								_SeventhBtn={this._SeventhBtn}
							/>
						</HeadLeft>
					</PanelHead>
					{/* ------------------------- */}
					<PanelWhi>
						<button
							className={
								step === 0 || step === 6 ? 'box btn-orange-absol -hiddle' : 'box btn-orange-absol'
							}
							onClick={this._handlePrevStep}
						>
							ย้อนกลับ
						</button>
						{this.stepController()}
						{this.state.loadingSave ? <LoaderSaving /> : ''}
						<div className="labelAddComplaint">
							{step <= 4 ? (
								<button
									className="box btn-blue"
									onClick={this._handleNextStep}
									disabled={this.state.disableNext}
								>
									ดำเนินการต่อ
								</button>
							) : step === 5 ? (
								<button
									className="box btn-blue"
									onClick={this._onSentComplaint}
									disabled={this.state.disableNext}
								>
									ส่งคำร้อง
								</button>
							) : null}
						</div>
					</PanelWhi>
				</PanelContent>
				{/* <pre>{JSON.stringify(this.state, null, '\t')}</pre> */}
			</PanelRight>
		);
	}
	// ------------------------------------------------------------------------------------------------------------------
	renderStep3() {
		return (
			<Row>
				<Col sm={12}>
					<p className="headStep">ผู้ถูกร้องทุกข์</p>
				</Col>
				<Col sm={12}>
					{selectComplaint.map((el, i) => {
						return this.onSwitch(el, 'selectTo');
					})}
				</Col>
				{parseInt(this.state.select_complaintTo) === 10 ? (
					<Col sm={12}>
						{this.state.departmentComplaint.map((el, i) => {
							return this.onSwitch(el, 'left');
						})}
					</Col>
				) : parseInt(this.state.select_complaintTo) === 11 ? (
					<Col sm={12}>
						{complaintToCenter.map((el, i) => {
							return this.onSwitch(el, 'center');
						})}
					</Col>
				) : (
					<Col sm={12}>
						{complaintToRight.map((el, i) => {
							return this.onSwitch(el, 'right');
						})}
					</Col>
				)}
			</Row>
		);
	}
	// ------------------------------------------------------------------------------------------------------------------
	onSwitch(el, ref) {
		// console.log('el', el);
		// console.log('this.state', this.state);
		switch (el.type) {
			case 'selectName':
				return (
					<SelectName
						active={this.state['active' + ref]}
						item={el}
						onChangeText={this.updatetypeInput}
						value={this.state}
					/>
				);
			case 'input':
				return <InputForm value={this.state} item={el} onChangeText={this.updatetypeInput} />;
			case 'radio':
				return <RadioForm checked={this.state[el.ref]} item={el} onChangeRadio={this.updatetypeInput} />;
			case 'radioDepart':
				return <RadioeDePart item={el} onChangeRadio={this.updatetypeInput} />;
			case 'radioDep':
				return <RadioForm item={el} onChangeRadio={this.updatetypeInput} />;
			case 'row':
				return (
					<InputRow
						active={ref === 'right' ? true : this.state['active' + ref]}
						item={el}
						onChangeText={this.updatetypeInput}
						value={this.state}
					/>
				);
			case 'cardno':
				return (
					<div key={el.ref} className="-comp-form-cardno">
						<Label>{el.title}</Label>
						<InputMask
							disabled={!this.state.activecomplaint}
							maskChar={null}
							mask={'9-9999-99999-99-9'}
							onChange={this.updatetypeInput}
							name={'id_card'}
							inputMode="numeric"
							type="text"
							className="form-control form-group"
							value={this.state.id_card}
							placeholder="หมายเลขประจำตัวประชาชน"
						/>
					</div>
				);
			case 'textarea':
				return <Textarea item={el} onChangeText={this.updatetypeInput} />;
			case 'rowProv':
				return this.state.select_complaintTo === 12 || this.state.select_complaintTo === '12' ? (
					<SelecterCompany
						active={ref === 'right' ? true : this.state['active' + ref]}
						name={ref}
						onChange={(ref, value) => this.onChange(ref, value)}
						value={ref === 'complaint' ? this.state : this.state}
					/>
				) : this.state.select_complaintTo === 11 || this.state.select_complaintTo === '11' ? (
					<SelecterPerson
						active={ref === 'right' ? true : this.state['active' + ref]}
						name={ref}
						onChange={(ref, value) => this.onChange(ref, value)}
						value={ref === 'complaint' ? this.state : this.state}
					/>
				) : (
					<Selecter
						active={ref === 'right' ? true : this.state['active' + ref]}
						name={ref}
						onChange={(ref, value) => this.onChange(ref, value)}
						value={ref === 'complaint' ? this.state : this.state}
					/>
				);
			case 'corporationType':
				return (
					<div key={ref} className="LabelInput-panel">
						<Label>ประเภท</Label>
						<RadioCol
							others={this.state.others}
							checked={this.state.companyTypes}
							item={{
								ref: 'companyTypes',
								data: this.state.companyType.map((e) => ({
									label: e.type_name,
									value: e.type_id
								}))
							}}
							onChangeRadio={this.updatetypeInput}
						/>
					</div>
				);
			default:
				return <div />;
		}
	}

	Step1() {
		const { step, title, type, subtype, complaint_channel, subject, other } = this.state;
		return (
			<div className={step !== 0 ? 'd-none' : 'd-grid'}>
				<p className="headStep">หัวข้อร้องทุกข์</p>

				<InputFormGroup
					title="หัวข้อร้องทุกข์"
					onChange={this.updatetypeInput}
					name={'subject'}
					value={subject}
				/>

				<RadioFormGroup
					title="ประเภทเรื่องหลัก"
					name={'type'}
					currentItem={type}
					allItem={headComplaint[1].data}
					onChange={this.updatetypeInput}
					xs={5}
				/>
				<SelectFormGroup
					title="ประเภทเรื่องย่อย"
					name={'subtype'}
					currentItem={subtype}
					allItem={sub_complaint[0][type].data.map((e) => ({ label: e.value, value: e.value }))}
					onChange={this.updatetypeInput}
				/>
				<RadioFormGroup
					title="ช่องทางการร้องเรียน"
					name={'complaint_channel'}
					currentItem={complaint_channel}
					allItem={headComplaint[2].data.map((e) => ({ label: e.label, value: e.label }))}
					onChange={this.updatetypeInput}
					xs={5}
					other={other}
				/>
				<InputFormGroup title="รหัส MOI" name={'moi'} onChange={this.updatetypeInput} value={this.state.moi} />
				<div />
			</div>
		);
	}
}
