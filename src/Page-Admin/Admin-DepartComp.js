/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/jsx-pascal-case */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import { PanelRight, PanelContent, PanelHead, PanelWhiMar } from '../Components/PartAdmin/MainAdmin';
import { Bgblue, HeadLeft, HeadBtn } from '../Asset/styleAdmin';
// import { SumTrack } from '../Components/Div/SetDiv';
import { LeftRight, ContentLeft, ContentRight, ContainerRadioRow, Label, LabelLight } from '../Asset/styled';
// import color from '../Asset/color';
import { changeStatusRadio } from '../Components/Static/static';
import { post, get, path } from '../api';
import 'moment/locale/th';
import moment from 'moment';
import { DetailComp, History, Head_compaint } from '../Components/Form/FormComp';
import Modal from 'react-responsive-modal';
import swal from 'sweetalert';
import Swal from 'sweetalert2';
import { TextLine } from '../Components/TextHead';
import { RadioRow } from '../Components/Form/Input';
import ModalCompaint from '../Components/Modal/ModalAdminCompaint';
import ModalPerson from '../Components/Modal/ModalPerson';
import LoaderSaving from '../Components/Load/LoaderSaving';
import USER from '../mobx/user';
import QRCode from 'qrcode.react';
moment.locale('th');

// export default class AdminDepartComp extends Component {
class AdminDepartComp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			form_id: '',
			result: null,
			showPopup: true,
			openModal: false,
			openPic: false,
			showImage: null,
			radiorow: '',
			focut_head: 1,
			intensive: [],
			Modalcompaint: false,
			status_id: null,
			loadingSave: false,
			//----------------
			files: [],
			descrition: '',
			nameFile: [],
			// ---------------
			central: [],
			category_id: [],
			description_transfer: null,
			expire: null,
			formstaff_join: [],
			chat_staff: [],
			ModalPersons: false,
			waite: 0,
			chat_customer: [],
			filechat_customer: [],
			filenamechat_customer: [],
			imgchat_customer: [],
			imgnamechat_customer: [],
			message: '',
			openQRcode: false,
			showQRcode: '',
			readIntensive: false,
			rightIntensive: false,
			readChat: false,
			readChatCustomer: false,
			loadingSend: false
		};
	}
	// ------------------------------------------------------------------------------------------------------------------
	componentWillMount() {
		this.comPlaint();
		this.Intensive();
		this.formstaff_join();
		this.central();
		this.readChatCustomer();
		this.readChat();
		this.readIntensive();
		this.chat_staff();
		this.chat_customer();
		// console.log('object');
	}

	// componentDidMount = async () => {
	// 	this.interval = setInterval(() => {
	// 		this.readChatCustomer();
	// 		this.readChat();
	// 		this.readIntensive();
	// 		this.chat_staff();
	// 		this.chat_customer();
	// 	}, 2000);
	// };
	// componentWillUnmount() {
	// 	clearInterval(this.interval);
	// }
	chat_customer = async () => {
		const form_id = this.props.location.state.id;
		let res = await get('/chat/customer/' + form_id);
		// console.log('res', res);
		this.setState({ chat_customer: res });
	};
	chat_staff = async () => {
		const form_id = this.props.location.state.id;
		let res = await get('/chat/staff/' + form_id);
		// console.log('res', res);
		this.setState({ chat_staff: res });
	};
	formstaff_join = async () => {
		const form_id = this.props.location.state.id;
		let res = await get('/form/staff_join/' + form_id);
		this.setState({ formstaff_join: res });
		// console.log('res', res);
	};
	async comPlaint() {
		const form_id = this.props.location.state.id;
		try {
			const result = await post(`/complaint/form_tracking`, { form_id });
			// console.log('result1', result);
			let nameFile = result.evidence_files
				.map((e) => {
					let link = e.path;
					let name = e.filename;
					let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
						.add(543, 'y')
						.format('DD MMMM YYYY');
					return { link: link, name: name, date: date, type: 'pdf' };
				})
				.concat(
					result.evidence_pic.map((e) => {
						let link = e.path;
						let name = e.filename;
						let date = moment(new Date(Number(e.path.split('/')[2].split('.')[0])))
							.add(543, 'y')
							.format('DD MMMM YYYY');
						return { link: link, name: name, date: date, type: 'jpg' };
					})
				);
			this.setState({
				result: result,
				radiorow: result.status,
				nameFile,
				status_id: result.status,
				rightIntensive: result.disclose == 0 ? true : false
			});
		} catch (error) {
			console.log('error', error);
		}
	}
	// ------------------------------------------------------------------------------------------------------------------
	async central() {
		try {
			const result = await get(`/central/`);
			this.setState({ central: result.slice(1) });
			// console.log('result', result);
		} catch (error) {
			// alert('central', error);
			console.log(error);
		}
	}
	search = async (e) => {
		let texts = e.target.value.toLowerCase();
		if (texts !== '') {
			let res = await get(`/central/`);
			// console.log('res', res);
			let result = res.slice(1).filter((e) => e.central_name.toLowerCase().indexOf(texts) > -1);
			this.setState({ central: result });
		} else {
			let res = await get(`/central/`);
			this.setState({ central: res.slice(1) });
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	async Intensive() {
		let form_id = this.props.location.state.id;
		let res = await get('/form/precipitation/' + form_id);
		this.setState({ intensive: res });
	}
	//-------------------------------------------------------------------------------------------------------------------
	readIntensive = async () => {
		let form_id = this.props.location.state.id;
		let read = await post('/form/get_read_precipitation/', { form_id });
		let r = read.filter(
			(e) =>
				parseInt(e.user_id) === parseInt(USER.user_id) &&
				parseInt(e.form_id) === parseInt(this.props.location.state.id)
		);
		this.setState({
			readIntensive: r.length > 0 ? r.some((e) => e.read === 1) : true
		});
	};
	updateIntensive = async () => {
		let form_id = this.props.location.state.id;
		let user_id = USER.user_id;
		await post('/form/read_precipitation/', { form_id, user_id });
		this.readIntensive();
	};
	// ------------------------------------------------------------------------------------------------------------------
	updatetypeInput = (e) => {
		// console.log('e', e.target.value);
		this.setState({ [e.target.name]: e.target.value, status_id: e.target.value });
	};
	// ------------------------------------------------------------------------------------------------------------------
	saveChangeStatus = async () => {
		try {
			let obj = {
				form_id: this.state.result.form_id,
				status: Number(this.state.status_id)
			};
			await post('/form/change_status', obj);
			swal({
				title: 'เรียบร้อย',
				text: 'เปลี่ยนสถานะการดำเนินการเรียบร้อย',
				icon: 'success'
			}).then(() => {
				// if (willDelete) {
				window.location.reload();
				// this.props.history.push('/tracking')
				// }
			});
			this.setState({ openModal: false });
		} catch (error) {
			swal('', error, 'warning');
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	onOpenModal = () => {
		this.setState({ openModal: true });
	};
	onOpenPic = (e) => {
		this.setState({ openPic: true, showImage: e });
	};
	onOpeQRcode = (e) => {
		this.setState({ openQRcode: true, showQRcode: e });
	};
	onCloseModal = () => {
		this.setState({ openModal: false, openPic: false, openQRcode: false });
	};
	// ------------------------------------------------------------------------------------------------------------------
	///------------------------------------------------------------------------------------------------------------------
	uploadImg = (event) => {
		Object.keys(event.target.files).forEach((key) => {
			let { files } = this.state;
			files.push(event.target.files[key]);
			this.setState({ files });
		});
	};
	delImages = (i) => {
		let { files } = this.state;
		files.splice(i, 1);
		this.setState({ files });
	};
	async insert_form_tracking() {
		this.setState({ loadingSend: true });
		let { files, descrition } = this.state;
		let USER = JSON.parse(localStorage.getItem('user'));
		// console.log('USER', USER);
		let formdata = new FormData();

		formdata.append('form_id', this.props.location.state.id);
		formdata.append('user_info_id', USER.user_info_id);
		formdata.append('description', descrition);

		files.forEach((element) => {
			formdata.append('file', element);
		});
		try {
			await post('/complaint/insert_form_tracking', formdata, true);
			this.setState({ loadingSend: false });
			swal('สำเร็จ', 'เพิ่มรายการการติดตามเสร็จสิ้น', 'success').then(() => {
				this.setState({ files: [], descrition: '' });
				this.componentWillMount();
			});
		} catch (error) {
			swal('', error, 'warning');
		}
	}
	del_tracking = async (e) => {
		Swal.fire({
			title: 'คุณต้องการลบหน่วยงานใช่หรือไม่',
			// text: 'ลบสำเร็จ',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่'
		}).then(async (result) => {
			if (result.value) {
				try {
					await post('/complaint/delete_tracking', { tracking_id: e });
					swal('สำเร็จ', 'ลบรายการการติดตามเสร็จสิ้น', 'success').then(() => this.componentWillMount());
				} catch (error) {
					swal('', error, 'warning');
				}
			}
		});
	};
	// ------------------------------------------------------------------------------------------------------------------
	sendTransfer = async (e) => {
		// console.log('object', moment(new Date()).add(this.state.expire, 'day').format('YYYY-MM-DD'));
		let { expire } = this.state;
		if (expire === null || expire === 'null' || expire === '') {
			swal('คำเตือน!', 'กรุณาเลือกระยะเวลาการดำเนินงาน', 'warning');
		} else {
			Swal.fire({
				title: 'คุณต้องการส่งต่อหน่วยงานใช่หรือไม่',
				// text: 'ลบสำเร็จ',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'ใช่',
				cancelButtonText: 'ไม่'
			}).then(async (result) => {
				if (result.value) {
					try {
						await post('/form/transfer', {
							form_id: this.state.result.form_id,
							category_id: JSON.stringify(this.state.category_id),
							description: this.state.description_transfer,
							expire: moment(new Date()).add(this.state.expire, 'day').format('YYYY-MM-DD')
						});
						// await post('/form/change_status', { form_id: this.state.result.form_id, status: 6 });
						this.setState({ Modalcompaint: false });
						swal('สำเร็จ', 'ส่งต่อหน่วยงานแล้ว', 'success').then(() => this.componentWillMount());
					} catch (error) {
						swal('', error, 'warning');
					}
				}
			});
		}
	};
	// ------------------------------------------------------------------------------------------------------------------
	_onChangeText = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	// -----------------------------------------------------------------------------------------------------------------------
	_onChangeBox = (e) => {
		const { category_id } = this.state;

		if (e.target.checked) {
			category_id.push(Number(e.target.value));
			this.setState({ category_id });
		} else {
			let i = category_id.indexOf(e.target.value);
			category_id.splice(i, 1);
			this.setState({ category_id });
		}
	};
	// -----------------------------------------------------------------------------------------------------------------------
	insert_staff_chat = async () => {
		let { message } = this.state;
		let obj = {
			message: message,
			form_id: this.props.location.state.id
		};
		if (message === '') {
		} else {
			// console.log('obj', obj);
			let res = await post('/chat/insert_staff_chat', obj);
			if (res === 'insert chat success') {
				this.setState({ message: '' }, () => this.chat_staff());
			}
		}
	};
	//---------------------------------------------------------------------------------------
	onEnter = async (e) => {
		if (e.key === 'Enter') {
			let { message } = this.state;
			let obj = {
				message: message,
				form_id: this.props.location.state.id
			};
			if (message === '') {
			} else {
				// console.log('obj', obj);
				let res = await post('/chat/insert_staff_chat', obj);
				if (res === 'insert chat success') {
					this.setState({ message: '' }, () => this.chat_staff());
				}
			}
		}
	};
	//---------------------------------------------------------------------------------------
	insert_customer_chat = async () => {
		let { message, imgchat_customer, filechat_customer } = this.state;
		// alert(message);
		let files = imgchat_customer.concat(filechat_customer);
		let formdata = new FormData();
		formdata.append('form_id', this.props.location.state.id);
		formdata.append('message', message);
		files.forEach((e) => {
			formdata.append('file', e);
		});

		if (message === '' && files.length === 0) {
		} else {
			// console.log('obj', obj);
			this.setState({ loadingSend: true });
			let res = await post('/chat/insert_customer_chat', formdata, true);
			if (res === 'insert chat success') {
				this.setState(
					{
						message: '',
						imgnamechat_customer: [],
						filenamechat_customer: [],
						imgchat_customer: [],
						filechat_customer: [],
						loadingSend: false
					},
					() => this.chat_customer()
				);
			}
		}
	};
	onEnter_customer = async (e) => {
		if (e.key === 'Enter') {
			let { message, imgchat_customer, filechat_customer } = this.state;
			let files = imgchat_customer.concat(filechat_customer);
			let formdata = new FormData();
			formdata.append('form_id', this.props.location.state.id);
			formdata.append('message', message);
			files.forEach((e) => {
				formdata.append('file', e);
			});
			// alert(message);
			if (message === '' && files.length === 0) {
			} else {
				// console.log('obj', obj);
				this.setState({ loadingSend: true });
				let res = await post('/chat/insert_customer_chat', formdata, true);
				if (res === 'insert chat success') {
					this.setState(
						{
							message: '',
							imgnamechat_customer: [],
							filenamechat_customer: [],
							imgchat_customer: [],
							filechat_customer: [],
							loadingSend: false
						},
						() => this.chat_customer()
					);
				}
			}
		}
	};
	//---------------------------------------------------------------------------------------
	uploadImg_chat_customer = (event) => {
		// console.log('event.target.files', event.target.files);
		Object.keys(event.target.files).forEach((key) => {
			let { imgchat_customer } = this.state;
			imgchat_customer.push(event.target.files[key]);
			this.setState({ imgchat_customer });
			// console.log('event.target.files[key]', event.target.files[key]);
			if (event.target.files && event.target.files[key]) {
				let reader = new FileReader();
				reader.onload = (e) => {
					let { imgnamechat_customer } = this.state;
					imgnamechat_customer.push(e.target.result);
					this.setState({ imgnamechat_customer });
				};
				reader.readAsDataURL(event.target.files[key]);
			}
		});
	};
	//--------------------------------------------------------------------------------------
	uploadFile_chat_customer = (event) => {
		// console.log('event.target.files', event.target.files);
		Object.keys(event.target.files).forEach((key) => {
			let { filenamechat_customer, filechat_customer } = this.state;
			filechat_customer.push(event.target.files[key]);
			filenamechat_customer.push(event.target.files[key].name);
			this.setState({ filechat_customer, filenamechat_customer });
			// console.log('event.target.files[key]', event.target.files[key]);
		});
	};
	//---------------------------------------------------------------------------------------
	DelImg_chat_customer = (i) => {
		let { imgchat_customer, imgnamechat_customer } = this.state;
		imgchat_customer.splice(i, 1);
		imgnamechat_customer.splice(i, 1);
		this.setState({ imgchat_customer, imgnamechat_customer });
	};
	DelFile_chat_customer = (i) => {
		let { filechat_customer, filenamechat_customer } = this.state;
		filechat_customer.splice(i, 1);
		filenamechat_customer.splice(i, 1);
		this.setState({ filenamechat_customer, filechat_customer });
	};
	//---------------------------------------------------------------------------------------
	readChat = async () => {
		let form_id = this.props.location.state.id;
		let read = await post('/chat/get_read_chat', { form_id });
		let r = read.filter(
			(e) =>
				parseInt(e.user_id) === parseInt(USER.user_id) &&
				parseInt(e.form_id) === parseInt(this.props.location.state.id)
		);
		this.setState({
			readChat: r.length > 0 ? r.some((e) => e.read === 1) : true
		});
		// console.log('read', read);
	};
	updatereadChat = async () => {
		let form_id = this.props.location.state.id;
		let user_id = USER.user_id;
		await post('/chat/read_chat', { form_id, user_id });
		this.readChat();
	};
	readChatCustomer = async () => {
		let form_id = this.props.location.state.id;
		let read = await post('/chat/get_user_read_chat', { form_id });
		let r = read.filter(
			(e) =>
				parseInt(e.user_id) === parseInt(USER.user_id) &&
				parseInt(e.form_id) === parseInt(this.props.location.state.id)
		);
		this.setState({
			readChatCustomer: r.length > 0 ? r.some((e) => e.read === 1) : true
		});
	};
	updatereadChatCustomer = async () => {
		let form_id = this.props.location.state.id;
		let user_id = USER.user_id;
		await post('/chat/user_read_chat', { form_id, user_id });
		this.readChatCustomer();
	};
	//---------------------------------------------------------------------------------------
	render() {
		const {
			result,
			openModal,
			focut_head,
			Modalcompaint,
			intensive,
			files,
			nameFile,
			formstaff_join,
			ModalPersons,
			waite,
			chat_staff,
			message,
			chat_customer,
			imgnamechat_customer,
			filenamechat_customer,
			openQRcode,
			showQRcode,
			readIntensive,
			readChat,
			rightIntensive,
			readChatCustomer,
			loadingSend,
			status_id
		} = this.state;
		// console.log('readChat', readChat);
		if (result) {
			return (
				<PanelRight className="pdf">
					<Bgblue />
					<PanelContent>
						{/* --------------------------------------------------------------------------------------------- */}
						<PanelHead>
							<HeadLeft>
								<HeadBtn
									onClick={() =>
										this.setState({
											focut_head: 1,
											imgnamechat_customer: [],
											filenamechat_customer: [],
											imgchat_customer: [],
											filechat_customer: [],
											message: ''
										})}
									bgAct={focut_head === 1 ? '#5DC0EC' : null}
								>
									เรื่องร้องเรียน
								</HeadBtn>
								<HeadBtn onClick={this.onOpenModal}>เปลี่ยนสถานะ</HeadBtn>
								<HeadBtn
									onClick={() => {
										this.setState(
											{
												focut_head: 2,
												imgnamechat_customer: [],
												filenamechat_customer: [],
												imgchat_customer: [],
												filechat_customer: [],
												message: ''
											},
											() => this.chat_staff()
										);
										this.updatereadChat();
									}}
									bgAct={focut_head === 2 ? '#5DC0EC' : null}
								>
									ส่งต่อหน่วยงาน
									{readChat === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)}
								</HeadBtn>
								<HeadBtn
									onClick={() => {
										this.setState({
											focut_head: 3,
											imgnamechat_customer: [],
											filenamechat_customer: [],
											imgchat_customer: [],
											filechat_customer: [],
											message: ''
										});
										this.updatereadChatCustomer();
									}}
									bgAct={focut_head === 3 ? '#5DC0EC' : null}
								>
									ห้องสนทนาผู้ร้องทุกข์
									{readChatCustomer === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)}
								</HeadBtn>
								<HeadBtn
									onClick={() => {
										this.setState({
											focut_head: 5,
											imgnamechat_customer: [],
											filenamechat_customer: [],
											imgchat_customer: [],
											filechat_customer: [],
											message: ''
										});
										this.updateIntensive();
									}}
									bgAct={focut_head === 5 ? '#5DC0EC' : null}
								>
									ข้อสั่งการจากผู้บริหารระดับสูง
									{readIntensive === false && (
										<i className="fas fa-exclamation-circle" style={{ color: '#f00' }} />
									)}
								</HeadBtn>
								<HeadBtn
									onClick={() =>
										this.setState({
											focut_head: 4,
											imgnamechat_customer: [],
											filenamechat_customer: [],
											imgchat_customer: [],
											filechat_customer: [],
											message: ''
										})}
									bgAct={focut_head === 4 ? '#5DC0EC' : null}
								>
									พิมพ์เอกสาร
								</HeadBtn>
							</HeadLeft>
						</PanelHead>
						{/* --------------------------------------------------------------------------------------------- */}
						<Head_compaint
							data={this.state.result}
							focut_head={focut_head}
							onClick={() => this.setState({ Modalcompaint: true })}
							addonClick={() => this.setState({ focut_head: 2 })}
							intensive={intensive}
							formstaff_join={formstaff_join}
							chat_staff={chat_staff}
							modallistname={(e) => this.setState({ ModalPersons: true, waite: e })}
							insert_staff_chat={() => this.insert_staff_chat()}
							set_message={(e) => this.setState({ message: e })}
							onEnter={this.onEnter}
							message={message}
							chat_customer={chat_customer}
							messagesEnd={(e) => e && e.scrollIntoView({ behavior: 'smooth' })}
							uploadImg_chat_customer={(e) => this.uploadImg_chat_customer(e)}
							uploadFile_chat_customer={(e) => this.uploadFile_chat_customer(e)}
							imgnamechat_customer={imgnamechat_customer}
							filenamechat_customer={filenamechat_customer}
							DelImg_chat_customer={(i) => this.DelImg_chat_customer(i)}
							DelFile_chat_customer={(i) => this.DelFile_chat_customer(i)}
							insert_customer_chat={() => this.insert_customer_chat()}
							onEnter_customer={this.onEnter_customer}
							openPic={(e) => this.onOpenPic(e)}
							ssQRcode={(e) => this.onOpeQRcode(e)}
							rightIntensive={rightIntensive}
							loadingSend={loadingSend}
						/>
						<Modal
							open={this.state.openPic}
							onClose={this.onCloseModal}
							little
							classNames={{ modal: 'Look-modal ChangeStatus ' }}
						>
							<img
								src={this.state.showImage}
								style={{ width: '100%', maxWidth: '900px', display: 'flex' }}
							/>
						</Modal>
						{/**--------------------------------------------------------------------------------------------- */}
						{focut_head === 1 && (
							<div className="dp-in-fx width100" style={{ position: 'relative' }}>
								{this.state.loadingSave ? <LoaderSaving /> : ''}
								<PanelWhiMar cn="width66 d-table">
									<DetailComp data={this.state.result} />
								</PanelWhiMar>
								{/* ------------------------------------------------------------*/}
								<div className="width33">
									<div className="admin-PanelWhi history width100">
										<TextLine title="ไฟล์เอกสาร" request />
										{nameFile.length > 0 ? (
											nameFile.map(
												(e) =>
													e.type === 'pdf' ? (
														<a
															className={'depart-file pdf'}
															href={`${path}${e.link}`}
															target="_blank"
														>
															<div>{e.name}</div>
															<div>{e.date}</div>
														</a>
													) : (
														<a
															className={'depart-file jpg'}
															onClick={() => this.onOpenPic(path + e.link)}
														>
															<div style={{ width: '250px' }}>{e.name}</div>
															<div>{e.date}</div>
														</a>
													)
											)
										) : (
											<div className="-comp-upload"> ไม่มีไฟล์เอกสาร </div>
										)}
									</div>
									<div className="admin-PanelWhi history width100">
										<LeftRight>
											<ContentLeft>
												<label id="tracking">รายละเอียดการดำเนินงาน</label>
											</ContentLeft>
											<ContentRight>
												<label id="update" onClick={this.onOpenModal}>
													อัปเดตการดำเนินงาน
												</label>
											</ContentRight>
										</LeftRight>
										<div>
											<History
												data={this.state.result}
												uploadfile={(e) => this.uploadImg(e)}
												filesupload={files}
												delImages={(e) => this.delImages(e)}
												descrition={(e) => this.setState({ descrition: e })}
												insert_form_tracking={() => this.insert_form_tracking()}
												del_tracking={(e) => this.del_tracking(e)}
												openPic={(e) => this.onOpenPic(e)}
												description={this.state.descrition}
												loadingSend={this.state.loadingSend}
												formstaff_join={formstaff_join}
											/>
										</div>
									</div>
								</div>
							</div>
						)}
						{/**--------------------------------------------------------------------------------------------- */}
					</PanelContent>
					<ModalCompaint
						sendTransfer={this.sendTransfer}
						result={this.state.central}
						Modalcompaint={Modalcompaint}
						onClose={() => this.setState({ Modalcompaint: false })}
						_onChangeText={this._onChangeText}
						onChangeBox={this._onChangeBox}
						search={this.search}
					/>
					<ModalPerson
						Modalcompaint={ModalPersons}
						onClose={() => this.setState({ ModalPersons: false })}
						formstaff_join={formstaff_join}
						waite={waite}
						result={result}
						reload={() => this.componentWillMount()}
					/>
					<Modal
						open={openModal}
						onClose={this.onCloseModal}
						little
						classNames={{ modal: 'Look-modal ChangeStatus ' }}
					>
						<div className="layout">
							<h2 style={{ marginBottom: 30 }}>เปลี่ยนสถานะ</h2>
							{changeStatusRadio.map((el, i) => {
								return (
									<RadioRow
										checked={this.state[el.ref]}
										item={el}
										onChangeRadio={this.updatetypeInput}
									/>
								);
							})}
							<button className="btn-blue" onClick={this.saveChangeStatus}>
								บันทึก
							</button>
						</div>
					</Modal>
					{/* <Modal
						open={openModal}
						onClose={this.onCloseModal}
						little
						classNames={{ modal: 'Look-modal ChangeStatus ' }}
					>
						<div className="layout">
							<h2 style={{ marginBottom: 30 }}>เปลี่ยนสถานะ</h2>
							{changeStatusRadio[0].data.map((el, i) => {
								return (
									<div>
										<ContainerRadioRow key={i}>
											<input
												type="radio"
												value={el.value}
												name={'radiorow' + i}
												// id={value}
												onChange={this.updatetypeInput}
												className="-comp-form-input"
												// checked={Number(checked) == Number(value)}
												checked={el.value == status_id}
											/>
											<Label style={{ marginRight: 5 }} for={el.value}>
												{el.label}
											</Label>
											<LabelLight for={el.value}>{el.sublabel}</LabelLight>
										</ContainerRadioRow>
									</div>
								);
							})}
							<button className="btn-blue" onClick={this.saveChangeStatus}>
								บันทึก
							</button>
						</div>
					</Modal> */}
					<Modal open={openQRcode} onClose={this.onCloseModal} little>
						<div className="m-5">
							<QRCode value={String(showQRcode)} size={300} />
						</div>
					</Modal>
				</PanelRight>
			);
		} else {
			return <div />;
		}
	}
}
export default AdminDepartComp;
