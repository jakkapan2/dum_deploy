import React, { Component } from 'react';
// import Main from '../Components/Main';
// import { Link, withRouter } from 'react-router-dom';
// import TextHead from '../Components/TextHead';
// import { Tabs, Tab } from 'react-bootstrap';
// import AdminReportCreate from './Admin-Report-Create';
// import AdminForward from './Admin-Forward';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import Images from '../Components/Images';
import { PanelRight, PanelContent, PanelHead, PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { HeadRight, HeadBtn, HeadLeft, Bgblue, HeadRightBtn, Button } from '../Asset/styleAdmin';
import { Noti } from '../Components/Static/adminStatic';
import moment from 'moment';
import { SelectTypeSerach, SelectSubtype } from '../Components/Form/Input';
import { get } from '../api';
import { headComplaint, sub_complaint, month, years, changeStatusRadio } from '../Components/Static/static';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { th } from 'date-fns/esm/locale';
import { Search } from '../Components/Search/Search';
import LoaderSaving from '../Components/Load/LoaderSaving';

export default class AdminReport extends Component {
	constructor(props) {
		super(props);

		this.state = {
			Noti: Noti,
			products: null,
			type: 0,
			sub_type: '',
			type_status: 1,
			status_name: '',
			//-----------------
			startDate: '',
			endDate: ''
		};
	}
	// -----------------------------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		// navigator.serviceWorker.addEventListener('message', (message) => console.log('message', message));
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.fetchProduct();
	};
	// -----------------------------------------------------------------------------------------------------------------------
	fetchProduct = async () => {
		let { type_status } = this.state;
		this.setState({ loadingSave: true });
		const res = await get('/complaint/admin_or_central'); //status รอเจ้าหน้าที่รับเรื่อง
		let cachedObject = {};
		res.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		this.setState({
			products:
				type_status === 1
					? products.filter((e) => e.status === 0)
					: type_status === 2
						? products
						: type_status === 3
							? products.filter((e) => e.status >= 1 && e.status <= 7)
							: products.filter((e) => e.status === 8)
		});
		this.setState({ loadingSave: true });
	};
	datetime = (cell, row) => {
		return moment(cell).add(543, 'year').format('DD/MM/YYYY');
	};
	moi = (cell, row) => {
		return cell ? cell : '-';
	};
	lookStatus = (cell, row) => {
		switch (cell) {
			case 0:
				return <span>รอเจ้าหน้าที่รับเรื่อง</span>;
			case 1:
				return <span>กำลังตรวจสอบคำร้อง</span>;
			case 2:
				return <span>เสนอผู้อำนวยการกลุ่มงานศุนย์ดำรงธรรมจังหวัดขอนแก่นพิจรณา</span>;
			case 3:
				return <span>เสนอหัวหน้าสำนักงานจังหวัดขอนแก่นพิจารณา</span>;
			case 4:
				return <span>เสนอรองผู้ว่าราชการจังหวัดขอนแก่น/ผู้ว่าราชการจังหวัดขอนแก่นพิจารณา</span>;
			case 5:
				return <span>สั่งการให้หน่วยงานตรวจสอบข้อเท็จจริง</span>;
			case 6:
				return <span>อยู่ระหว่างการดำเนินการของหน่วยงานหรือจังหวัด</span>;
			case 7:
				return <span style={{ color: '#f44336' }}>รายระเอียดคำร้องไม่ชัดเจน</span>;
			case 8:
				return <span style={{ color: '#8bc34a' }}>ยุติเรื่อง</span>;
			default:
				return <span />;
		}
	};
	fullname = (cell, row) => {
		return (
			<span>
				{row.fname === null || row.lname === null || row.fname === '' || row.lname === '' ? (
					'ปกปิดข้อมูล'
				) : (
					row.fname + ' ' + row.lname
				)}
			</span>
		);
	};
	goAddcompaint = () => {
		return this.props.history.push('/addcompaint');
	};
	searchSubject = async (e) => {
		let { type_status } = this.state;
		const ress = await get('/complaint/admin_or_central');
		let cachedObject = {};
		ress.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		// console.log('products', products);
		let res =
			type_status === 1
				? products.filter((e) => e.status === 0)
				: type_status === 2
					? products
					: type_status === 3
						? products.filter((e) => e.status >= 1 && e.status <= 7)
						: products.filter((e) => e.status === 8);
		let texts = e.toLowerCase();
		let res_products = res
			.map((e) => ({ ...e, name: e.fname + ' ' + e.lname, adminname: e.admin_Fname + ' ' + e.admin_Lname }))
			.filter(
				(e, i) =>
					String(e.subject).toLowerCase().indexOf(texts) > -1 ||
					String(e.form_id).toLowerCase().indexOf(texts) > -1 ||
					String(e.id_card).toLowerCase().indexOf(texts) > -1 ||
					String(e.name).toLowerCase().indexOf(texts) > -1 ||
					String(e.adminname).toLowerCase().indexOf(texts) > -1 ||
					String(e.moi).toLowerCase().indexOf(texts) > -1 ||
					String(e.central_name).toLowerCase().indexOf(texts) > -1 ||
					String(e.house_number).toLowerCase().indexOf(texts) > -1 ||
					String(e.moo).toLowerCase().indexOf(texts) > -1 ||
					String(e.alleyway).toLowerCase().indexOf(texts) > -1 ||
					String(e.road).toLowerCase().indexOf(texts) > -1 ||
					String(e.sub_district).toLowerCase().indexOf(texts) > -1 ||
					String(e.district).toLowerCase().indexOf(texts) > -1 ||
					String(e.province).toLowerCase().indexOf(texts) > -1 ||
					String(e.zipcode).toLowerCase().indexOf(texts) > -1
			);
		this.setState({ products: res_products });
	};
	Clicksearch = async () => {
		this.setState({ loadingSearch: true });
		let { sub_type, type, status_name, type_status, startDate, endDate } = this.state;
		let sub_typeName = type === 0 || type === '' ? '' : sub_type;
		let type_name =
			Number(type) === 1
				? 'ได้รับความเดือดร้อน'
				: Number(type) === 2
					? 'ขอความช่วยเหลือ'
					: Number(type) === 3
						? 'ขอความเป็นธรรม'
						: Number(type) === 4
							? 'ปัญหาที่อยู่อาศัย/ที่ดิน'
							: Number(type) === 5
								? 'แจ้งเบาะแส'
								: Number(type) === 6
									? 'ทรัพยากรธรรมชาติและสิ่งแวดล้อม'
									: Number(type) === 7
										? 'หนี้สิน'
										: Number(type) === 8.1 ? 'ร้องเรียนกล่าวโทษเจ้าหน้าที่/หน่วยงานของรัฐ' : '';
		const ress = await get('/complaint/admin_or_central');
		let cachedObject = {};
		ress.map((item) => (cachedObject[item.form_id] = item));
		let products = Object.values(cachedObject);
		let res =
			type_status === 1
				? products.filter((e) => e.status === 0)
				: type_status === 2
					? products
					: type_status === 3
						? products.filter((e) => e.status >= 1 && e.status <= 7)
						: products.filter((e) => e.status === 8);
		// console.log('startDate', moment(startDate).format('DD MM YYYY'), 'endDate', endDate);
		let res_products = res
			.filter(
				(e) =>
					endDate !== ''
						? endDate !== 'null'
							? endDate !== null
								? moment(endDate).format('DD MM YYYY') <= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter(
				(e) =>
					startDate !== ''
						? startDate !== 'null'
							? startDate !== null
								? moment(startDate).format('DD MM YYYY') >= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter(
				(e) =>
					startDate !== '' && endDate !== ''
						? startDate !== 'null' && endDate !== 'null'
							? startDate !== null && endDate !== null
								? moment(startDate).format('DD MM YYYY') >= moment(e.timestamp).format('DD MM YYYY') &&
									moment(endDate).format('DD MM YYYY') <= moment(e.timestamp).format('DD MM YYYY')
								: e
							: e
						: e
			)
			.filter((e) => (type_name !== '' ? e.type === type_name : e))
			.filter((e) => (sub_typeName !== '' ? e.sub_type === sub_typeName : e))
			.filter((e) => (status_name !== '' ? Number(e.status) === Number(status_name) : e));
		// console.log('res_products', res_products);
		this.setState({ products: res_products });
		this.setState({ loadingSearch: false });
	};
	render() {
		const { Noti, products, type_status, startDate, endDate } = this.state;
		const optionsTable = {
			paginationSize: 5,
			sizePerPageList: [ 10 ],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			onRowClick: (cell) => {
				this.props.history.push({ pathname: '/admindepartcompaint', state: { id: cell.id } });
			}
		};
		return (
			<PanelRight>
				<Bgblue />
				<PanelContent>
					{/* ------------------------- */}
					<PanelHead>
						<HeadLeft>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 1, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 1 ? '#5DC0EC' : null}
							>
								เรื่องร้องเรียนเข้าใหม่
							</HeadBtn>

							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 3, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 3 ? '#5DC0EC' : null}
							>
								เรื่องที่อยู่ระหว่างดำเนินการ
							</HeadBtn>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 4, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 4 ? '#5DC0EC' : null}
							>
								เรื่องที่ยุติ
							</HeadBtn>
							<HeadBtn
								onClick={() =>
									this.setState({ type_status: 2, products: null }, () => this.fetchProduct())}
								bgAct={type_status === 2 ? '#5DC0EC' : null}
							>
								เรื่องร้องเรียนทั้งหมด
							</HeadBtn>
						</HeadLeft>
						<HeadRight>
							<HeadRightBtn href="/adminaddcompaint">+ บันทึกเรื่องร้องเรียน</HeadRightBtn>
						</HeadRight>
					</PanelHead>
					{/* ------------------------- */}
					<PanelWhi>
						<div className="row mb-3">
							<div
								className={`${type_status === 2 || type_status === 3
									? 'col-12 col-lg-10'
									: 'col-12 col-lg-10'} mb-3`}
							>
								{/* ----------search ----------- */}
								<Search
									classNameInp="mr-auto w-100"
									raduis="5px"
									title="ค้นหา"
									placeholder="ค้นหาชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ"
									onChange={(e) => this.searchSubject(e.target.value)}
									// hint="ชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ"
								/>

								{/* /--------------------------------- */}
							</div>
							<div
								className={`${type_status === 2 || type_status === 3
									? 'col-12 col-lg-5'
									: 'col-12 col-lg-10'}`}
							>
								<div className="form-group select-arrow">
									<label>ค้นหาตามช่วงเวลา</label>
									<div className="d-flex justify-content-between">
										<DatePicker
											placeholderText="เลือกวันเริ่มต้น"
											locale={th}
											dateFormat={'dd MMMM ' + moment(startDate).add(543, 'year').format('YYYY')}
											renderCustomHeader={({ date, changeYear, changeMonth }) => (
												<div
													style={{
														display: 'flex',
														justifyContent: 'between'
													}}
												>
													<select
														className="form-control mx-1"
														value={
															startDate === '' ||
															startDate === 'null' ||
															startDate === null ? (
																moment(new Date()).month()
															) : (
																moment(startDate).month()
															)
														}
														onClick={() => this.setState({ startDate: date })}
														onChange={({ target: { value } }) => {
															changeMonth(value);
														}}
													>
														{month.map((e) => (
															<option value={Number(e.value) - 1}>{e.label}</option>
														))}
													</select>
													<select
														className="form-control mx-1"
														value={
															startDate === '' ||
															startDate === 'null' ||
															startDate === null ? (
																moment(new Date()).year()
															) : (
																moment(startDate).year()
															)
														}
														onClick={() => this.setState({ startDate: date })}
														onChange={({ target: { value } }) => {
															changeYear(value);
														}}
													>
														{years().map((e) => (
															<option value={e.id}>{Number(e.name + 543)}</option>
														))}
													</select>
												</div>
											)}
											selected={startDate}
											onChange={(date) => this.setState({ startDate: date })}
										/>
										<DatePicker
											placeholderText="เลือกวันสิ้นสุด"
											locale={th}
											dateFormat={'dd MMMM ' + moment(endDate).add(543, 'year').format('YYYY')}
											renderCustomHeader={({ date, changeYear, changeMonth }) => (
												<div
													style={{
														display: 'flex',
														justifyContent: 'center'
													}}
												>
													<select
														className="form-control mx-1"
														value={
															endDate === '' || endDate === 'null' || endDate === null ? (
																moment(new Date()).month()
															) : (
																moment(endDate).month()
															)
														}
														onClick={() => this.setState({ endDate: date })}
														onChange={({ target: { value } }) => {
															changeMonth(value);
														}}
													>
														{month.map((e) => (
															<option value={Number(e.value) - 1}>{e.label}</option>
														))}
													</select>
													<select
														className="form-control mx-1"
														value={
															endDate === '' || endDate === 'null' || endDate === null ? (
																moment(new Date()).year()
															) : (
																moment(endDate).year()
															)
														}
														onClick={() => this.setState({ endDate: date })}
														onChange={({ target: { value } }) => {
															changeYear(value);
														}}
													>
														{years().map((e) => (
															<option value={e.id}>{Number(e.name + 543)}</option>
														))}
													</select>
												</div>
											)}
											selected={endDate}
											onChange={(date) => this.setState({ endDate: date })}
										/>
									</div>
								</div>
							</div>
							{type_status === 2 || type_status === 3 ? (
								<div className="col-12 col-lg-5">
									<div className="form-group select-arrow">
										<label>สถานะเรื่อง</label>
										<select
											className="form-control"
											onChange={(e) => this.setState({ status_name: e.target.value })}
										>
											<option value="">ทั้งหมด</option>
											{changeStatusRadio[0].data.map((e) => (
												<option value={e.value}>{e.label}</option>
											))}
										</select>
									</div>
								</div>
							) : (
								''
							)}
							<div className="col-12 col-md-6 col-lg-5">
								<SelectTypeSerach
									value={this.state.type}
									item={headComplaint[1]}
									onChangeSelect={(e) => this.setState({ type: e.target.value })}
								/>
							</div>
							<div className="col-12 col-md-6 col-lg-5">
								<SelectSubtype
									disabled={this.state.type === 0 || this.state.type === ''}
									value={this.state.sub_type}
									item={sub_complaint[0]}
									head={this.state.type}
									onChangeSelect={(e) => this.setState({ sub_type: e.target.value })}
								/>
							</div>
							<div className="col d-flex align-items-end">
								<Button my=".75rem" onClick={this.Clicksearch}>
									ค้นหา
								</Button>
							</div>
						</div>
						{/* ----------------------------------------------------------------------------------------------- */}
						<div id="my-tableHeader-class-text-left" style={{ position: 'relative' }}>
							{products ? (
								<BootstrapTable
									data={products
										.sort((a, b) => moment(b.timestamp) - moment(a.timestamp))
										.map((e) => ({ ...e, sub: !e.subject ? '-' : e.subject }))}
									pagination={true}
									options={optionsTable}
									containerClass="my-container-class tb-setSearch"
									tableContainerClass="my-tableContainer-class"
									tableHeaderClass="my-tableHeader-class colorBlue"
									tableBodyClass="my-tableBody-pointer-class"
								>
									<TableHeaderColumn dataField="form_id" isKey width={'12%'}>
										รหัสเรื่อง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="moi" dataFormat={this.moi} width={'10%'}>
										รหัส MOI
									</TableHeaderColumn>
									<TableHeaderColumn dataField="timestamp" dataFormat={this.datetime} width={'10%'}>
										วันที่แจ้ง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="sub" width={'25%'}>
										เรื่องร้องเรียน
									</TableHeaderColumn>
									<TableHeaderColumn dataField="type" width={'15%'}>
										ประเภทเรื่องหลัก
									</TableHeaderColumn>
									<TableHeaderColumn dataField="name" dataFormat={this.fullname} width={'15%'}>
										ชื่อผู้ร้อง
									</TableHeaderColumn>
									<TableHeaderColumn dataField="status" dataFormat={this.lookStatus} width={'13%'}>
										สถานะเรื่อง
									</TableHeaderColumn>
								</BootstrapTable>
							) : this.state.loadingSave ? (
								<div style={{ position: 'relative', height: '300px' }}>
									<LoaderSaving />
								</div>
							) : (
								''
							)}
							{this.state.loadingSearch ? (
								<div style={{ position: 'absolute', height: '300px', width: '100%', top: '0' }}>
									<LoaderSaving />
								</div>
							) : (
								''
							)}
						</div>
					</PanelWhi>
				</PanelContent>
			</PanelRight>
		);
	}
}

// ----------------------------------------------------------------------------------------
