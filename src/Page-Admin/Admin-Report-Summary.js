import React, { Component } from 'react';
import TextHead from '../Components/TextHead';
// import Main from '../Components/Main';
// import Images from '../Components/Images';
import { PanelRight, PanelContent, PanelHead, PanelWhi } from '../Components/PartAdmin/MainAdmin';
import { BarChart, Bar, YAxis, XAxis, Tooltip, PieChart, Pie, Sector, Cell } from 'recharts';
import { Bgblue, HeadLeft, Button } from '../Asset/styleAdmin';
import { Search } from '../Components/Search/Search';
import { SelectSubtype, SelectFormGroup, SelectCagetory, SelectTypeSerach } from '../Components/Form/Input';
import { disTrict, years, month, headComplaint, sub_complaint, changeStatusRadio } from '../Components/Static/static';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import { get, post } from '../api';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import LoaderSaving from '../Components/Load/LoaderSaving';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { th } from 'date-fns/esm/locale';
import moment from 'moment';

const CustomTooltip = ({ active, status, label, payload }) => {
	// console.log('object', status[label], 'ghjk', length);
	if (active) {
		return (
			<div className="custom-tooltip">
				<p className="label">{status[label].status_name}</p>
				<p className="intro"> จำนวน {status[label].count} เรื่อง</p>
			</div>
		);
	}

	return null;
};
const CustomTooltipPie = ({ active, index, payload }) => {
	console.log('payload', payload.length > 0 ? payload[0].name : '');
	if (active) {
		return (
			<div className="custom-tooltip">
				<p className="label">{payload.length > 0 ? payload[0].name : ''}</p>
				<p className="intro"> จำนวน {payload.length > 0 ? payload[0].value : ''} เรื่อง</p>
			</div>
		);
	}

	return null;
};
const renderCustomBarLabel = ({ x, y, width, value }) => {
	// console.log('value', value);
	return (
		<text x={x + width / 2} y={y} fill="#666" textAnchor="middle" dy={-6}>
			{value}
		</text>
	);
};

const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, payload, percent, index }) => {

	const RADIAN = Math.PI / 180;

	const radius = innerRadius + (outerRadius - innerRadius) * 0.5;

	const sin = Math.sin(-RADIAN * midAngle);
	const cos = Math.cos(-RADIAN * midAngle);
	const sx = cx + (outerRadius + 10) * cos;
	const sy = cy + (outerRadius + 10) * sin;
	const mx = cx + (outerRadius + 30) * cos;
	const my = cy + (outerRadius + 30) * sin;
	const ex = mx + (cos >= 0 ? 1 : -1) * 22;
	const ey = my;
	const textAnchor = cos >= 0 ? 'start' : 'end';
	return (
		<g>
			<text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`ร้อยละ ${(percent * 100).toFixed(0)}`}</text>
			<path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={'#ff3300'} fill="none" />
		</g>
	);
};
export default class AdminReportSummary extends Component {
	constructor(props) {
		super(props);
		this.state = {
			yearsAdjust: [],
			select_year: '',
			timemin: '',
			timemax: '',
			type: '',
			sub_type: '',
			values: '',
			// ---search-------
			items: [],
			itemsOld: [],
			graphs: [],
			search_status: false,
			central: null,
			comCount: {},
			// ----------
			clientWidth: document.body.clientWidth,
			// ----------
			tab: 1,
			// ----------
			loadingSave: false
		};
	}
	componentDidMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.yearsAdjust();
		this.fetchItem();
		this.fetchCentral();
	}

	async fetchItem(e) {
		let { category_id, district, sub_type, type, timemin, timemax, values } = this.state;
		let sum = 0
		const com = await get('/complaint/get_complaint_count');
		// console.log('com', com);
		this.setState({ comCount: com });
		if (e) {
			this.setState({ loadingSave: true });
			const item = await post('/complaint/search_graph', {
				values,
				type:
					type === '' || type === null
						? ''
						: headComplaint[1].data.filter((e) => e.value === Number(type))[0].label,
				sub_type,
				timemin,
				timemax,
				district: !district ? '' : district,
				category_id: !category_id ? '' : category_id
			});
			// console.log('item', item);
			this.setState({ graphs: item });
			this.setState({ loadingSave: false });
			// console.log('item search>>>', graphs)
		} else {
			const item = await post('/complaint/search_graph');
			this.setState({ graphs: item });
			this.setState({ loadingSave: false });
		}

		this.state.graphs.map((e)=> sum += e.count)
		this.setState({sum})


	}

	async fetchCentral() {
		const central = await get('/central/');
		// console.log('central', central);
		this.setState({ central });
	}

	yearsAdjust() {
		const { yearsAdjust } = this.state;
		years().map((e) => {
			yearsAdjust.push({
				label: Number(e.id + 543),
				value: Number(e.naem + 543)
			});
			this.setState({ yearsAdjust });
		});
	}
	fullname = (cell, row) => {
		return (
			<p>
				{row.fname} {row.lname}
			</p>
		);
	};

	// -----------------------------------------------------------------------------------------------------------------------
	render() {
		const colors = scaleOrdinal(schemeCategory10).range();
		const { graphs, timemin, timemax, central, clientWidth, tab, items, itemsOld, comCount } = this.state;
		const optionsTable = {
			paginationSize: 3,
			sizePerPageList: [10],
			paginationShowsTotal: this.renderPaginationShowsTotal,
			onRowClick: (cell) => {
				this.props.history.push({ pathname: '/admindepartcompaint', state: { id: cell.id } });
			}
		};
		return (
			<PanelRight>
				<Bgblue />
				{tab === 1 ? (
					<PanelContent>
						<PanelHead>
							<HeadLeft>
								<div className="report-header">
									<span>จำนวนรับเรื่องโดยศูย์ดำรงธรรม</span>
									<strong>{Number(comCount.complaitCountByCentral).toLocaleString()}</strong>
								</div>
								<div className="report-header">
									<span>จำนวนรับเรื่องโดยหน่วยงาน</span>
									<strong>{Number(comCount.complaitCount).toLocaleString()}</strong>
								</div>
							</HeadLeft>
						</PanelHead>
						<PanelWhi>
							<TextHead title="สรุปผลการร้องเรื่องในแต่ละอำเภอ" />
							{/******************************************************************************* */}
							<div className="d-grid mt-4">
								<div className="d-flex align-items-end flex-wrap">
									<Search
										title="ค้นหาเรื่องร้องเรียน"
										classNameDiv="mb-3 col-10"
										raduis="5px"
										w="100%"
										placeholder={
											'ค้นหาชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ'
										}
										// hint="ชื่อ-นามสกุล, เลขบัตรประชาชน, รหัสเรื่องจากระบบ, รหัส MOI, ชื่อเรื่อง,ที่อยู่, หน่วยงานที่รับผิดชอบ, ชื่อเจ้าหน้าที่ศุนย์ดำรงธรรมที่รับผิดชอบ"
										onChange={(e) => this.setState({ values: e.target.value })}
									/>
									<SelectCagetory
										title="หน่วยงาน"
										className="col-4"
										allItem={
											central ? (
												central.map((e) => {
													return { label: e.central_name, value: e.central_id };
												})
											) : null
										}
										onChange={(e) => this.setState({ category_id: e.target.value })}
									/>
									<div className="col-6">
										<div className="form-group select-arrow">
											<label>ค้นหาตามช่วงเวลา</label>
											<div className="d-flex justify-content-between">
												<DatePicker
													placeholderText="เลือกวันเริ่มต้น"
													locale={th}
													dateFormat={
														'dd MMMM ' + moment(timemin).add(543, 'year').format('YYYY')
													}
													renderCustomHeader={({ date, changeYear, changeMonth }) => (
														<div
															style={{
																display: 'flex',
																justifyContent: 'between'
															}}
														>
															<select
																className="form-control mx-1"
																value={
																	timemin === '' ||
																		timemin === 'null' ||
																		timemin === null ? (
																			moment(new Date()).month()
																		) : (
																			moment(timemin).month()
																		)
																}
																onClick={() => this.setState({ timemin: date })}
																onChange={({ target: { value } }) => {
																	changeMonth(value);
																}}
															>
																{month.map((e) => (
																	<option value={Number(e.value) - 1}>
																		{e.label}
																	</option>
																))}
															</select>
															<select
																className="form-control mx-1"
																value={
																	timemin === '' ||
																		timemin === 'null' ||
																		timemin === null ? (
																			moment(new Date()).year()
																		) : (
																			moment(timemin).year()
																		)
																}
																onClick={() => this.setState({ timemin: date })}
																onChange={({ target: { value } }) => {
																	changeYear(value);
																}}
															>
																{years().map((e) => (
																	<option value={e.id}>{Number(e.name + 543)}</option>
																))}
															</select>
														</div>
													)}
													selected={timemin}
													onChange={(date) => this.setState({ timemin: date })}
												/>
												<DatePicker
													placeholderText="เลือกวันสิ้นสุด"
													locale={th}
													dateFormat={
														'dd MMMM ' + moment(timemax).add(543, 'year').format('YYYY')
													}
													renderCustomHeader={({ date, changeYear, changeMonth }) => (
														<div
															style={{
																display: 'flex',
																justifyContent: 'center'
															}}
														>
															<select
																className="form-control mx-1"
																value={
																	timemax === '' ||
																		timemax === 'null' ||
																		timemax === null ? (
																			moment(new Date()).month()
																		) : (
																			moment(timemax).month()
																		)
																}
																onClick={() => this.setState({ timemax: date })}
																onChange={({ target: { value } }) => {
																	changeMonth(value);
																}}
															>
																{month.map((e) => (
																	<option value={Number(e.value) - 1}>
																		{e.label}
																	</option>
																))}
															</select>
															<select
																className="form-control mx-1"
																value={
																	timemax === '' ||
																		timemax === 'null' ||
																		timemax === null ? (
																			moment(new Date()).year()
																		) : (
																			moment(timemax).year()
																		)
																}
																onClick={() => this.setState({ timemax: date })}
																onChange={({ target: { value } }) => {
																	changeYear(value);
																}}
															>
																{years().map((e) => (
																	<option value={e.id}>{Number(e.name + 543)}</option>
																))}
															</select>
														</div>
													)}
													selected={timemax}
													onChange={(date) => this.setState({ timemax: date })}
												/>
											</div>
										</div>
									</div>
								</div>
								<div className="d-flex align-items-end">
									<SelectTypeSerach
										value={this.state.type}
										item={headComplaint[1]}
										onChangeSelect={(e) => {
											this.setState({ type: e.target.value });
											// console.log('name', e.target.name);
										}}
										className="col-md-4"
									/>
									<SelectSubtype
										disabled={this.state.type === 0 || this.state.type === ''}
										value={this.state.sub_type}
										item={sub_complaint[0]}
										head={this.state.type}
										onChangeSelect={(e) => this.setState({ sub_type: e.target.value })}
										className="col-md-4"
									/>
									<SelectFormGroup
										title="อำเภอ"
										allItem={disTrict}
										className="col-md-2"
										onChange={(e) => this.setState({ district: e.target.value })}
									/>
									<Button my="1rem" mx="auto" onClick={() => this.fetchItem(true)}>
										ผลลัพธ์
									</Button>
								</div>
							</div>
							{/******************************************************************************* */}
							<div className="report-chart">

								<div className="chart-note">
									{changeStatusRadio[0].data.map((el, index) => {
										return (
											<div>
												<i style={{ backgroundColor: colors[index % 20] }} />
												<span
													onClick={() =>
														graphs[index].count > 0 &&
														this.setState({
															items: graphs[index].data,
															itemsOld: graphs[index].data,
															tab: 2
														})}
												>
													{el.label}
												</span>
											</div>
										);
									})}
								</div>
								<div className="w-100 my-3 pl-5"><h5>จำนวนรวมทั้งหมด {this.state.sum} เรื่อง </h5></div>
								{/******************************************************************************* */}
								<div className="py-3 w-100 justify-content-around">
									<div className="chart">
										{this.state.loadingSave ? (
											<LoaderSaving />
										) : graphs.length < 1 ? (
											<LoaderSaving />
										) : (
													''
												)}
										{graphs && (
											<BarChart
												width={clientWidth < 1600 ? 500 : 700}
												height={clientWidth < 1600 ? 300 : 400}
												data={graphs}
											>
												<YAxis />
												{/* <XAxis label={{ value: 'สถานะเรื่อง', position: 'outside', offset: 0, className:'mb-5'}} /> */}
												
												<Tooltip content={<CustomTooltip status={graphs} label />} />
												<Bar
													// type="monotone"
													dataKey="count"
													barSize={100}
													style={{ cursor: 'pointer' }}
													onClick={(data) =>
														this.setState({ items: data.data, itemsOld: data.data, tab: 2 })}
													label={renderCustomBarLabel}
												>
													{graphs.map((entry, index) => (
														<Cell key={`cell-${index}`} fill={colors[index % 20]} />
													))}
												</Bar>
											</BarChart>
										)}
										<span className="chart-x">สถานะเรื่องร้องเรียน</span>
										<span className="chart-y">จำนวนเรื่องร้องเรียน</span> 
										<line />
									</div>


									{/******************************************************************************* */}
									{graphs && (
										<div style={{ borderLeft: '1px solid #ccc' }}>
											<PieChart width={clientWidth < 1600 ? 450 : 700} height={clientWidth < 1600 ? 300 : 400} onMouseEnter={this.onPieEnter}>
												<Pie
													data={graphs.map((e) => ({ name: e.status_name, value: e.count }))}
													cx={clientWidth < 1600 ? 220 : 340}
													cy={clientWidth < 1600 ? 160 : 200}
													isAnimationActive={false}
													labelLine={false}
													label={renderCustomizedLabel}
													outerRadius={clientWidth < 1600 ? 100 : 150}
													fill="#8884d8"
													index
												>
													{
														graphs.map((entry, index) => <Cell fill={colors[index % 20]} />)
													}
												</Pie>
												<Tooltip content={<CustomTooltipPie status={graphs} index />} />
											</PieChart>
										</div>
									)}
								</div>
							</div>
							{/******************************************************************************* */}
						</PanelWhi>
					</PanelContent>
				) : (
						<PanelContent>
							<PanelHead>
								<HeadLeft>
									<Button bg="#5DC0EC" onClick={() => this.setState({ tab: 1 })}>
										กลับ
								</Button>
								</HeadLeft>
							</PanelHead>
							<PanelWhi>
								<div className="d-flex justify-content-between align-items-center mb-4">
									<div className="report-header-detail">
										{itemsOld[0].status_name} <strong> {itemsOld.length} เรื่อง</strong>
									</div>
									<div>
										<Search title="ค้นหาเรื่องร้องเรียน" onChange={this.searchItem} />
									</div>
								</div>
								{console.log('items', items)}
								<div id="my-tableHeader-class-text-left">
									<BootstrapTable
										data={items.map((e) => ({
											...e,
											date: moment(e.timestamp).add(543, 'years').format('LLL')
										}))}
										pagination={true}
										options={optionsTable}
										containerClass="my-container-class tb-setSearch"
										tableContainerClass="my-tableContainer-class"
										tableHeaderClass="my-tableHeader-class colorBlue"
										tableBodyClass="my-tableBody-pointer-class"
									>
										<TableHeaderColumn dataField="form_id" isKey>
											เลขที่เรื่องร้องเรียน
									</TableHeaderColumn>
										<TableHeaderColumn dataField="date">วันที่แจ้ง</TableHeaderColumn>
										<TableHeaderColumn dataField="subject">เรื่องร้องเรียน</TableHeaderColumn>
										<TableHeaderColumn dataField="name" dataFormat={this.fullname}>
											ผู้ร้องเรียน
									</TableHeaderColumn>
									</BootstrapTable>
								</div>
							</PanelWhi>
						</PanelContent>
					)}
			</PanelRight>
		);
	}
	searchItem = (e) => {
		let { itemsOld } = this.state;
		let texts = e.target.value.toLowerCase();
		let res = itemsOld
			.map((e) => ({
				...e,
				date: moment(e.timestamp).add(543, 'years').format('LLL'),
				name: e.fname + ' ' + e.lname
			}))
			.filter(
				(e, i) =>
					e.date.toLowerCase().indexOf(texts) > -1 ||
					String(e.form_id).toLowerCase().indexOf(texts) > -1 ||
					e.subject.toLowerCase().indexOf(texts) > -1 ||
					e.name.toLowerCase().indexOf(texts) > -1
			);
		this.setState({ items: res });
	};

	BarChart() { }
}
