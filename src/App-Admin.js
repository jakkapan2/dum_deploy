import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './Asset/css/style.css';
import './Asset/css/adminstyle.css';
import './Asset/css/reportstyle.css';
import './Asset/css/mainstyle.css';
import './Asset/css/tablestyle.css';
import './Asset/css/radiorate.css';
import './Asset/css/btnStyle.css';
import './Asset/scss/accordionStyle.css';
import './Asset/css/componentStyle.css';
import { observer } from 'mobx-react';
import User from './mobx/user';
import _NavbarHeadAdmin from './Components/Navbar/NavbarHeadAdmin';
import NavbarHeadAdmin from './Components/Navbar/NavbarHeadAdmin';
import NavbarLeftAdmin from './Components/Navbar/NavbarLeftAdmin';
import Loader from './Components/Load/Loader';
import { get, post } from './api';
import Role from './mobx/user';
import moment from 'moment';

@observer
class AppAdmin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			role: false,
			menuOpen: true,
			loading: true,
			openDrop: false,
			notiDetail: [],
			amount_noti: [],
			check_noti: true
		};
	}
	// ------------------------------------------------------------------------------------------------------------------
	componentDidMount = async () => {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		if (!User.role) {
			this.props.history.push('/login');
		} else {
			// navigator.serviceWorker.addEventListener('message', (message) => console.log('message', message));
			// this.interval = setInterval(async () => {
			if (Role.role === 'ADMIN') {
				let res = await post('/form/get_form_noti', { role: Role.role });
				console.log('ADMIN', res);
				this.setState({
					notiDetail: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.sort((a, b) => moment(b.insert_at) - moment(a.insert_at))
						.map((e) => ({
							...e,
							date: moment(e.insert_at).add(543, 'year').format('LLL')
						})),
					amount_noti: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.filter((e) => e.read === 0),
					check_noti: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.some((e) => e.read === 0)
				});
			} else {
				let res = await post('/form/get_form_noti', { role: Role.role, category_id: Role.central_id });
				console.log('SUB', res);
				this.setState({
					notiDetail: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.sort((a, b) => moment(b.insert_at) - moment(a.insert_at))
						.map((e) => ({
							...e,
							date: moment(e.insert_at).add(543, 'year').format('LLL')
						})),
					amount_noti: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.filter((e) => e.read === 0),
					check_noti: res.change
						.concat(res.tranfer)
						.concat(res.noti)
						.concat(res.tranferback)
						.some((e) => e.read === 0)
				});
			}
			// }, 2000);
		}
		// setTimeout(() => {
		this.setState({ loading: false });
		// }, 500)
	};
	componentWillUnmount() {
		// clearInterval(this.interval);
	}
	// ------------------------------------------------------------------------------------------------------------------
	dropNoti = () => {
		this.setState({ openDrop: !this.state.openDrop });
	};

	// ------------------------------------------------------------------------------------------------------------------
	render() {
		const { openDrop, notiDetail, amount_noti, check_noti } = this.state;
		return this.state.loading ? (
			<Loader />
		) : (
			<div className="App">
				<div className="setCenter -admin">
					<NavbarLeftAdmin />
					<div className="panel-right">
						<NavbarHeadAdmin
							userRole={this.state.role}
							dropNoti={this.dropNoti}
							openDrop={openDrop}
							notiDetail={notiDetail}
							amount_noti={amount_noti}
							check_noti={check_noti}
						/>
						{/* <div className='panel-content' > */}
						{this.props.children}
						{/* </div> */}
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(AppAdmin);
